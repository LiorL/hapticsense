set SolutionDir=%cd%\..\Penlink
set SolutionFileName=penlink.sln
set OutputDirectory=%SolutionDir%\Penlink\bin\Release
set OutputFileName=PenLink.exe
set PublishDir=%cd%\..\Publish
del /S /Q %OutputDirectory%\*
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSbuild %SolutionDir%\%SolutionFileName% /p:Configuration=Release /t:Clean;Rebuild
for /f "delims=" %%a in ('VersionInfo.exe %OutputDirectory%\%OutputFileName% format') do @set myvar=%%a
set TargetDir="%PublishDir%\%myvar%"
echo Target dir is: %TargetDir%
RD /S /Q %TargetDir%
Xcopy %OutputDirectory%\* %TargetDir% /Y /D /E /I
del /S %TargetDir%\*.xml %TargetDir%\*.pdb
7z a -t7z -m0=lzma2 -mx9 %TargetDir%.7z %TargetDir%\*
