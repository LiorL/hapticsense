﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace PenLink
{
    public partial class InfoWindow : UserControl
    {
        const int LineSize = 25;
        const int ColumnSize = 150;
        const double KeyValueDist = 0.7;
        const int VericalMargins = 20;
        const int HorizontalMargins = 10;
        [DllImport("user32.dll")]
        public static extern bool EnableWindow(IntPtr HWnd, bool Enabled);

        class InfoElement
        {
            Label lblDesc;
            Label lblValue;

            public InfoElement(Control aParent,string aDesc,Point aPoint)
            {
                lblDesc = new Label();

                lblDesc.Text = aDesc;
                lblValue = new Label();

                lblDesc.Text = aDesc;
                lblDesc.ForeColor = Color.Green;
                lblDesc.Parent = aParent;
                lblDesc.BackColor = Color.Transparent;
                lblDesc.Show();
                lblDesc.AutoSize = true;
                lblDesc.Left = aPoint.X;
                lblDesc.Top = aPoint.Y;
                //lblDesc.Left = 20;
                //lblDesc.Top = 20;

                lblValue.Text = "N/A";
                lblValue.ForeColor = Color.Green;
                lblValue.Parent = aParent;
                lblValue.AutoSize = true;
                lblValue.Show();
                lblValue.Left = aPoint.X + (int)(ColumnSize * KeyValueDist);
                lblValue.Top = aPoint.Y;
                //lblValue.Left = 100;
                //lblValue.Top = 100;

            }

            public void SetValue (string aVal)
            {
            lblValue.Text = aVal;
            }
            public void SetDesc(string aDesc)
            {
            lblDesc.Text = aDesc;
            }


        }
     
        int CurrentElement;
        Dictionary <string,InfoElement> dicInfoelements;
        public InfoWindow() 
        {
            InitializeComponent();
            
            dicInfoelements = new Dictionary<string,InfoElement>();
            //lblHeader = new Label();
            //lblHeader.Text = "Info Debug Window";
            //lblHeader.ForeColor = Color.Green;
            //lblHeader.Parent = this;
            //lblHeader.BackColor = Color.Transparent;
            //lblHeader.Dock = DockStyle.Top;
            //lblHeader.Show();
            EnableWindow(this.Handle, false);
            
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        public void SetValue(string aDesc,string aVal)
        {
            if (!dicInfoelements.ContainsKey(aDesc))
                AddElement(aDesc);
            
            dicInfoelements[aDesc].SetValue(aVal);
        }

        public void AddElement(string aDesc)
        {
            int MaxColumns = this.Width / (ColumnSize + VericalMargins);
            int MaxLines = (this.Height - (HorizontalMargins * 2)) / LineSize;
            int CurrentLine =  CurrentElement % MaxLines;
            int CurrentColumn = CurrentElement / MaxLines;
            Point pnt = new Point(CurrentColumn * ColumnSize + HorizontalMargins, CurrentLine * LineSize + VericalMargins );
            dicInfoelements.Add(aDesc, new InfoElement(this, aDesc, pnt));
            CurrentElement++;
            
        }

    }
}
