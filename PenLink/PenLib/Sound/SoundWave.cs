﻿using SharpDX.DirectSound;
using SharpDX.Multimedia;
using System;

namespace PenLink.Sound
{
    public class SoundWave
    {
        public enum Note { A, Ads, B, C, Cds, D, Dds, E, F, Fds, G, Gds };
        private static readonly double NoteFactor = Math.Pow(2, 1.0 / 12);
        public const int BaseOctave = 4;
        public const float A0 = 27.5f;

        private PrimarySoundBuffer fBuffer = null;
        private float fBaseTone;
        private int fSampleRate;

        public SoundWave(DirectSound device, Note aNote, short aOctave, int aSampleRate, int aBitsPerSample, int aTime, int aSilence, int aChannels, bool aSquareWave)
        {
            CreateSoundWave(device, GetFrequency(aNote, aOctave), aSampleRate, aBitsPerSample, aTime, aSilence, aChannels, aSquareWave);
        }

        private void CreateSoundWave(DirectSound device, float aBaseTone, int aSampleRate, int aBitsPerSample, int aTime, int aSilence, int aChannels, bool aSquareWave)
        {
            fBaseTone = aBaseTone;
            fSampleRate = aSampleRate;

            SoundBufferDescription buffds = new SoundBufferDescription();
            
            int Time;

            if (aTime < 0)
                Time = (int)(aSampleRate / 100.0);
            else
                Time = aTime;

            int BytesPerSample = aBitsPerSample / 8;
            int Samples = (int)Math.Round((aSampleRate * (Time / 1000.0)));
            int BufferSize = Samples * aChannels * BytesPerSample;

            buffds.BufferBytes = Samples * aChannels * BytesPerSample + (int)(aSilence / 1000.0 * aSampleRate * aChannels * BytesPerSample); // in bytes

            buffds.Format = WaveFormat.CreateCustomFormat(WaveFormatEncoding.Pcm, aSampleRate, aChannels, aSampleRate * aChannels * BytesPerSample, aChannels * BytesPerSample, aBitsPerSample);
            buffds.Flags = BufferFlags.ControlVolume | BufferFlags.ControlPositionNotify | BufferFlags.ControlFrequency | BufferFlags.Software;
            if (device != null)
                fBuffer = new PrimarySoundBuffer(device, buffds);
            fBuffer.Write<byte>(SoundManager.Instance.GenerateTone(aBaseTone, aSampleRate, aTime, aChannels, aSquareWave, aBitsPerSample), 0, LockFlags.None);

            if (fBuffer == null)
                throw new NullReferenceException();
        }

        public SoundWave(DirectSound device, float aBaseTone, int aSampleRate, int aBitsPerSample, int aTime, int aSilence, int aChannels, bool aSquareWave)
        {
            CreateSoundWave(device, aBaseTone, aSampleRate, aBitsPerSample, aTime, aSilence, aChannels, aSquareWave);
        }
        public bool Playing
        {

            get { return ( (BufferStatus)fBuffer.Status & (BufferStatus.Looping | BufferStatus.Playing)) != BufferStatus.None; }
        }
        public void PlayLooped()
        {
            fBuffer.Play(0, PlayFlags.Looping);
        }

        public void Play()
        {
            fBuffer.Play(0, PlayFlags.None);
        }


        public void Stop()
        {
            fBuffer.Stop();
        }

        public void SetFrequency(double Frequency)
        {
            double factor = (double)Frequency / fBaseTone;
            if (factor <= 0)
                factor = 0.01;
            fBuffer.Frequency = (int)(fSampleRate * factor);

        }

        public static float GetFrequency(Note note, int Octave)
        {
            int HalfTones = (int)note;
            HalfTones += (Octave - 1) * 12;
            return (float)(A0 * Math.Pow(NoteFactor, HalfTones));
        }

        private int GetNoteFreqRelativeToBase(Note note, int Octave)
        {
            int HalfTones = (int)(note - Note.A);
            int Octaves = Octave - BaseOctave;
            HalfTones += Octaves * 12;
            int Frequecy = (int)Math.Round((fBaseTone * Math.Pow(NoteFactor, HalfTones)));
            return Frequecy;
        }

        public void SetNote(Note note, int Octave)
        {
            SetFrequency(GetNoteFreqRelativeToBase(note, Octave));
        }


    }
}