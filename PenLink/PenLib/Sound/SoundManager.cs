﻿using SharpDX.DirectSound;
using System;
using System.Collections.Generic;

namespace PenLink.Sound
{
    public class SoundManager : Common.Singleton<SoundManager>
    {
        const double TwoMathPI = 2.0 * Math.PI;
        //private Dictionary<IntPtr, DirectSound> fDicDevices;
        private DirectSound fDevice;
        private IntPtr fHandle;

        protected SoundManager()
        {
            fHandle = IntPtr.Zero;
            List<DeviceInformation> devices = DirectSound.GetDevices();
            
            bool validDriverFound = false;
            foreach (DeviceInformation info in devices)
            {
                //Find a valid Audio device driver
                if (info.DriverGuid != Guid.Empty)
                    validDriverFound = true;

            }
            if (validDriverFound) 
                fDevice = new DirectSound();
            
        }

        public IntPtr Handle 
        {
            get { return fHandle;}
            set
            { 
                fHandle = value;
                if (fDevice != null)
                    fDevice.SetCooperativeLevel(fHandle, CooperativeLevel.Priority);
            }

        }


        public DirectSound Device
        {
            get {

                if (fHandle != IntPtr.Zero)
                    return fDevice;
                else
                    throw new Exception("Sound Manager handle has not been set");
            }
        }

        //public DirectSound GetDevice(IntPtr aHandle)
        //{
        //    DirectSound device = null;

        //    if (!fDicDevices.TryGetValue(aHandle, out device))
        //    {
        //        try
        //        {
        //            device = new DirectSound();
        //        }
        //        catch
        //        {
        //            return null;
        //        }
        //    }
        //    device.SetCooperativeLevel(aHandle, CooperativeLevel.Priority);
        //    return device;
        //}


        private  double GetSquareWaveAtPosition(double pitf, int Squariness)
        {
            double sigma = 0;
            int n;
            for (int k = 1; k < Squariness; k++)
            {
                n = (2 * k) - 1;
                sigma += Math.Sin(n * pitf) / n;
            }

            sigma *= 4;
            sigma /= Math.PI;

            if (sigma > 1)
                sigma = 1;
            if (sigma < -1)
                sigma = -1;



            return sigma;
        }


        public  byte[] GenerateTone(float aBaseTone, int aSampleRate, int aTime, int aChannels, bool aSquareWave, int BitsPerSample)
        {
            System.Int16 outword;
            int BytePerSample = BitsPerSample / 8;
            int Samples = (int)Math.Round((aSampleRate * (aTime / 1000.0)));
            int BuffSz = Samples * aChannels * BytePerSample;
            byte[] PlaybackData = new byte[BuffSz];
            short Normalizer = (short)(1 << (BitsPerSample - 1) - 1);

            for (int t = 0; t < PlaybackData.Length / 2; t++)// in bytes
            {
                double pit = (TwoMathPI * t) / aSampleRate;
                double result;
                result = aSquareWave == true ? GetSquareWaveAtPosition(pit * aBaseTone, 10) : Math.Sin(aBaseTone * pit);
                result *= Normalizer;
                outword = (Int16)result;

                //if (outword > 0)
                //    outword = 0x7fff;
                //else
                //    outword = -1;
                //stereo 16-bit pcm format
                PlaybackData[t * 2] = (System.Byte)(outword & 0xff);//lsb
                PlaybackData[t * 2 + 1] = (System.Byte)((outword >> 8) & 0xff);//msb
                //PlaybackData[t++] = (System.Byte)(outword & 0xff);//lsb
                //PlaybackData[t++] = (System.Byte)((outword >> 8) & 0xff);//msb
            }
            return PlaybackData;

        }

    }
}
