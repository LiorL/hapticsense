﻿using PenLink.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PenLink
{
    public class FormEx : Form
    {

        public FormEx()
        {
            this.Activated +=FormEx_Activated;
            this.Deactivate += FormEx_Deactivate;
        }

        void FormEx_Deactivate(object sender, EventArgs e)
        {
            fIsActivated = false;
        }

        void FormEx_Activated(object sender, EventArgs e)
        {
            fIsActivated = true;
        }

        private bool fMouseCursorVisible = true;
        bool fIsActivated;
        public bool IsActivated { get { return fIsActivated; } }

        public bool AllowClipCursor { get; set; }

        public bool MouseCursorVisible
        {
            get
            {
                return fMouseCursorVisible;
            }
            set
            {
                if (fMouseCursorVisible != value)
                {
                    fMouseCursorVisible = value;

                    switch (fMouseCursorVisible)
                    {

                        case true:
                            System.Windows.Forms.Cursor.Show();
                            if (AllowClipCursor)
                                SystemHelper.ClipCursorClear();
                            break;
                        case false:
                            if (AllowClipCursor)
                                SystemHelper.ClipCursor(this.Bounds);
                            System.Windows.Forms.Cursor.Hide();
                            break;
                        default:
                            break;
                    }

                }
            }

        }


     
    }
}
