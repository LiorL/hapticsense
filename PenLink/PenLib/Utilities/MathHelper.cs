﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Utilities
{
    public class MathHelper
    {
        public static float Clamp(float num, float min, float max)
        {
            return Math.Max(min, Math.Min(num, max));
        }

        public static double Clamp(double num, double min, double max)
        {
            return Math.Max(min, Math.Min(num, max));
        }
    }
}
