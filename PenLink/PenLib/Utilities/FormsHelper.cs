﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PenLink.Utilities
{
    public class FormsHelper
    {
        private static void SetSpacesToAllMenus(ToolStripMenuItem aRoot)
        {
            string a = aRoot.Text;
            int i = a.IndexOf('[');
            int j = a.IndexOf(']');

            if (i != -1 && j != -1 && i < j)
            {
                int lengthOfShortcut = j - i - 1;
                //int lengthOfText = i -1;
                string text = a.Substring(0, i);
                text = text.PadRight(12, (char)32);
                string shortcut = a.Substring(i + 1, lengthOfShortcut);
                text += shortcut;

                aRoot.Text = text;
            }


            foreach (ToolStripMenuItem item in aRoot.DropDownItems)
                SetSpacesToAllMenus(item);

        }

        private static void SetSpacesToAllMenus(MenuStrip aMenuStrip)
        {
            foreach (ToolStripMenuItem item in aMenuStrip.Items)
                SetSpacesToAllMenus(item);
        }

        public static void SetSpacesToAllMenus(Control RootControl)
        {
            if (RootControl is MenuStrip)
            {
                SetSpacesToAllMenus(RootControl as MenuStrip);
            }

            foreach (Control control in RootControl.Controls)
            {
                SetSpacesToAllMenus(control);
            }
        }

        public static void UnCheckChilds(ToolStripMenuItem aItem, bool aRecursive)
        {
            aItem.Checked = false;
            if (aItem.DropDownItems != null)
                foreach (ToolStripMenuItem item in aItem.DropDownItems)
                    UnCheckChilds(item, aRecursive);
        }

        public static void UnCheckMenuTreeByItem(ToolStripMenuItem aItem)
        {
            ToolStripMenuItem TopItem = aItem;
            while (TopItem.OwnerItem is ToolStripMenuItem) TopItem = TopItem.OwnerItem as ToolStripMenuItem;
            UnCheckChilds(TopItem, true);

        }

        public static void RecursiveCheck(ToolStripMenuItem aItem)
        {
            ToolStripMenuItem TopItem = aItem;
            while (TopItem.OwnerItem is ToolStripMenuItem)
            {
                TopItem.Checked = true;
                TopItem = TopItem.OwnerItem as ToolStripMenuItem;
            }
        }
        public static void UnCheckSiblings(ToolStripMenuItem aItem)
        {
            foreach (ToolStripMenuItem item in (aItem.OwnerItem as ToolStripMenuItem).DropDownItems)
                item.Checked = false;
        }


        public static void CheckSoleBrother(ToolStripMenuItem aItem)
        {
            UnCheckSiblings(aItem);
            aItem.Checked = true;
        }
        public static bool IsInDesignMode()
        {
            bool returnFlag = false;
#if DEBUG

            if (System.ComponentModel.LicenseManager.UsageMode == System.ComponentModel.LicenseUsageMode.Designtime)
            {
                returnFlag = true;
            }

            else if (Process.GetCurrentProcess().ProcessName.ToUpper().Equals("DEVENV"))
            {
                returnFlag = true;
            }
#endif
            return returnFlag;

        }
    }
}
