﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Utilities
{
    public class ReflectionHelper
    {
        public static Dictionary<string, object> ExtractFieldsFromInstance(object aInstance)
        {
            Type type = aInstance.GetType();
            FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            Dictionary<string, object> values = new Dictionary<string, object>();
            foreach (var field in fields)
            {
                string name = field.Name;
                object value = field.GetValue(aInstance);
                values.Add(name, value);
            }
            return values;
        }
    }
}
