﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Utilities
{
    class IOHelper
    {
        public static bool IsSerialPortExist(string aPortName)
        {
            string[] portNames = System.IO.Ports.SerialPort.GetPortNames();
            for (int i = 0; i < portNames.Length; i++)
                if (portNames[i] == aPortName)
                    return true;

            return false;
        }
    }
}
