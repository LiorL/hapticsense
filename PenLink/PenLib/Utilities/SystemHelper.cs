﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PenLink.Utilities
{
    public class SystemHelper
    {

        [DllImport("user32.dll")]
        public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int RegisterWindowMessage(string message);

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool SetProp(IntPtr hWnd, string lpString, IntPtr hData);

        [DllImport("user32.dll")]
        public static extern IntPtr GetProp(IntPtr hWnd, string lpString);

        public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam); 

        [DllImport("user32.dll", CharSet = CharSet.Unicode)] 
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder strText, int maxCount); 
        
        [DllImport("user32.dll", CharSet = CharSet.Unicode)] 
        public static extern int GetWindowTextLength(IntPtr hWnd); 
        
        [DllImport("user32.dll")] 
        public static extern bool EnumWindows(EnumWindowsProc enumProc, IntPtr lParam); 
        
        [DllImport("user32.dll")] 
        public static extern bool IsWindowVisible(IntPtr hWnd); 
        
                
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetDllDirectory(string lpPathName);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern int GetDllDirectory(int bufsize, StringBuilder buf);

        [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr LoadLibrary(string librayName);

        [DllImport("mylibrary")]
        public static extern void InitMyLibrary();

        [DllImport("user32.dll", EntryPoint = "ClipCursor", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern bool _ClipCursor(ref Rect rect);

        [StructLayout(LayoutKind.Explicit)]
        public struct Rect
        {
            public static Rect FromSystemRect(System.Drawing.Rectangle aRect)
            {
                Rect result = new Rect();
                result.left = aRect.Left;
                result.top = aRect.Top;
                result.right = aRect.Right;
                result.bottom = aRect.Bottom;
                return result;
            }

            [FieldOffset(0)]
            public int left;
            [FieldOffset(4)]
            public int top;
            [FieldOffset(8)]
            public int right;
            [FieldOffset(12)]
            public int bottom;

            static public readonly Rect Null;
            static public readonly Rect Infinite;
            static Rect()
            {
                Null = new Rect();
                Infinite = new Rect();
                Infinite.left = Infinite.top = int.MinValue;
                Infinite.bottom = Infinite.right = int.MaxValue;
            }
        }

        public static void ClipCursor(System.Drawing.Rectangle aRect)
        {
            Rect r = Rect.FromSystemRect(aRect);
            _ClipCursor(ref r);
        }

        public static void ClipCursorClear()
        {
            Rect r = Rect.Infinite;
            _ClipCursor(ref r);
        }

        public static bool IsWindows8OrGreater()
        {
            //windows 8 is verision 6.2
            Version v = Environment.OSVersion.Version;
            return v.Major > 6 || (v.Major == 6 && v.Minor >= 2);
        }
        public static Screen GetSecondaryScreen()
        {
            Screen result = null;
            foreach (Screen screen in Screen.AllScreens)
            {
                if (screen != Screen.PrimaryScreen)
                {
                    result = screen;
                    break;
                }
            }

            return result;
        }

        internal static Screen GetWorkingScreen()
        {
            return Screen.AllScreens.Length > 1 ? SystemHelper.GetSecondaryScreen() : Screen.PrimaryScreen;
        }

        public delegate bool SystemWindowDelegate(IntPtr aWindowHandle,object obj);

        [StructLayout( LayoutKind.Sequential, Pack = 1)]
        struct SystemWindowWrapper
        {
            //public IntPtr userObject;

            
            [MarshalAs(UnmanagedType.FunctionPtr)]
            public SystemWindowDelegate callback;
            [MarshalAs(UnmanagedType.LPStr)]
            public string assemblyName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string typeName;
            public IntPtr userObject;
        }
     

        public static void ForEachSystemWindow(SystemWindowDelegate aEnumWindowDelegate, object obj)
        {
            SystemWindowWrapper wrapper = new SystemWindowWrapper();
            wrapper.callback = aEnumWindowDelegate;
            IntPtr userObjectPointer = IntPtr.Zero;
            IntPtr wrapperPointer = IntPtr.Zero; 

            try
            {
                if (obj != null)
                {
                    userObjectPointer = Marshal.AllocHGlobal(Marshal.SizeOf(obj));
                    Marshal.StructureToPtr(obj, userObjectPointer, false);
                    wrapper.typeName = obj.GetType().FullName;
                    wrapper.assemblyName = obj.GetType().Assembly.FullName;
                    wrapper.userObject = userObjectPointer;

                }
                
                wrapperPointer = Marshal.AllocHGlobal(Marshal.SizeOf(wrapper));
                Marshal.StructureToPtr(wrapper, wrapperPointer, false);
                Utilities.SystemHelper.EnumWindows(new PenLink.Utilities.SystemHelper.EnumWindowsProc(GeneralWindowEnumCallbak), wrapperPointer);

            }

            finally
            {
                if (userObjectPointer != IntPtr.Zero)
                    Marshal.FreeHGlobal(userObjectPointer);

                if (wrapperPointer != IntPtr.Zero)
                    Marshal.FreeHGlobal(wrapperPointer);
                
            }


        }

        private static bool GeneralWindowEnumCallbak(IntPtr hWnd, IntPtr lParam)
        {
            
            SystemWindowWrapper wrapper = (SystemWindowWrapper)Marshal.PtrToStructure(lParam, typeof(SystemWindowWrapper));
            object obj = null;
            if (wrapper.userObject != IntPtr.Zero)
            {
                Type type = Activator.CreateInstance(wrapper.assemblyName, wrapper.typeName).Unwrap().GetType();
                obj = Marshal.PtrToStructure(wrapper.userObject, type);
            }
            return wrapper.callback(hWnd, obj);
            
            
        }

        private  const string DllsPathGuid = "{73047A57-7D8E-4C63-9DD8-1D240BEE2C8B}";
        /// <summary>
        ///  Set a direectory to look for managed dlls
        /// </summary>
        /// <param name="aPath">The path of the directory</param>
        /// <param name="aDomain">[optional] The Application domain to  to be applied.  defaults to the current domain </param>
        public static void SetManagedDllDirectory(string aPath, AppDomain aDomain = null)
        {
            if (aDomain == null)
                aDomain = AppDomain.CurrentDomain;

            aDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            aDomain.SetData(DllsPathGuid,aPath);
        }

        private static System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            string managedDllPath = (sender as AppDomain).GetData(DllsPathGuid) as string;
            string assemblyPath = System.IO.Path.Combine(managedDllPath, string.Format("{0}.dll", new System.Reflection.AssemblyName(args.Name).Name));
            if (!System.IO.File.Exists(assemblyPath))
            {
                assemblyPath = System.IO.Path.Combine(managedDllPath, string.Format("{0}.exe", new System.Reflection.AssemblyName(args.Name).Name));
                if (!System.IO.File.Exists(assemblyPath))
                {
                    return null;
                }
            }
            return System.Reflection.Assembly.LoadFrom(assemblyPath);
        }
    }
}
