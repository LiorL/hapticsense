﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Utilities
{
    public class FormattingHelper
    {
        public static string numToStringFixed2<T>(T aNum)
        {
            return string.Format("{0:0.##}", aNum);
        }
    }
}
