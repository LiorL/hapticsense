﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Utilities
{
    public class DataHelper
    {
        public static byte[] AnyToByteArray<T>(T obj, int size)
        {

            byte[] dataArray = new byte[size];

            //Buffer.BlockCopy(obj, 0, dataArray, 0, size);

            GCHandle srcHandle = GCHandle.Alloc(obj, GCHandleType.Pinned);
            IntPtr source = srcHandle.AddrOfPinnedObject();
            Marshal.Copy(source, dataArray, 0, size);
            srcHandle.Free();
            return dataArray;
        }
    }
}
