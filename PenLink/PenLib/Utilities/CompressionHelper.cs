﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Utilities
{
    public class CompressionHelper
    {
        public static byte[] Decompress(byte[] gzip)
        {
            return Decompress(new MemoryStream(gzip)).ToArray();
        }

        public static MemoryStream Decompress(MemoryStream gzip)
        {
            // Create a GZIP stream with decompression mode.
            // ... Then create a buffer and write into while reading from the GZIP stream.
            gzip.Position = 0;
            using (System.IO.Compression.GZipStream stream = new System.IO.Compression.GZipStream(gzip, System.IO.Compression.CompressionMode.Decompress))
            {

                const int size = 1024 * 1024 * 4;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory;
                }
            }
        }


        //public static MemoryStream Compress(MemoryStream aStream)
        //{
        //    return Compress(aStream.ToArray());
        //}

        public static byte[] Compress(byte[] raw)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }
                return memory.ToArray();
            }
        }
    }
}
