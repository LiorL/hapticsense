using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoundLineCode;
using PenLink.Graphics;
using PenLink;
using System.IO;
using SharpDX;
using SharpDX.D3DCompiler;
using ResourceType = PenLink.Graphics.ResourceType;

using SharpDX.Direct3D11;
using SharpDX.Direct3D;
using System.Runtime.InteropServices;
using PenLink.Utilities;

namespace PenLink.Graphics
{
    /// <summary>
    /// Class to handle drawing a list of RoundLines.
    /// </summary>
    class RoundLineManager
    {
        const double PiOver2 = Math.PI / 2.0;
        const int NumberOfFloatsPerInstance = 4;
        const int SizeOfGPUFloat = sizeof(float);

        private GraphicRenderer renderer;
        private Device device;
        private Graphics.Effect fEffect;
        SharpDX.Direct3D11.EffectVariable viewProjMatrixParameter;
        SharpDX.Direct3D11.EffectVariable instanceDataParameter;
        SharpDX.Direct3D11.EffectVariable colorDataParameter;
        SharpDX.Direct3D11.EffectVariable timeParameter;
        SharpDX.Direct3D11.EffectVariable lineRadiusParameter;
        SharpDX.Direct3D11.EffectVariable lineColorParameter;
        SharpDX.Direct3D11.EffectVariable blurThresholdParameter;
        PenLink.Graphics.Resources.VertexBuffer fVB;
        PenLink.Graphics.Resources.IndexBuffer fIB;
        SharpDX.Direct3D11.InputLayout vdecl;
        
        private int numInstances;
        private int numVertices;
        private int numIndices;
        private int numPrimitivesPerInstance;
        private int numPrimitives;
        private int bytesPerVertex;
        float[] translationData;
        float[] colorData;

        public int NumLinesDrawn;
        public float BlurThreshold = 0.65f;


        public void Init(GraphicRenderer aRenderer)
        {
            this.renderer = aRenderer;
            this.device = aRenderer.GraphicsDevice;
            
            fEffect = ResourceLoader.Instance.GetResource(ResourceType.ID_EFFECTS, "RoundLine", aRenderer) as Graphics.Effect;
            viewProjMatrixParameter = fEffect.DeviceEffect.GetVariableByName("viewProj");
            instanceDataParameter = fEffect.DeviceEffect.GetVariableByName("instanceData");
            colorDataParameter = fEffect.DeviceEffect.GetVariableByName("colorData");
            timeParameter = fEffect.DeviceEffect.GetVariableByName("time");
            lineRadiusParameter = fEffect.DeviceEffect.GetVariableByName("lineRadius");
            lineColorParameter = fEffect.DeviceEffect.GetVariableByName("lineColor");
            blurThresholdParameter = fEffect.DeviceEffect.GetVariableByName("blurThreshold");

            CreateRoundLineMesh();
        }

        public string[] TechniqueNames
        {
            get
            {
                int numTechniques = fEffect.DeviceEffect.Description.TechniqueCount;
                string[] names = new string[numTechniques];
                for (int i = 0; i < numTechniques; i++)
                    names[i] = fEffect.DeviceEffect.GetTechniqueByIndex(i).Description.Name;
                return names;
            }
        }

        private void CreateRoundLineMesh()
        {
            const int primsPerCap = 6; // A higher primsPerCap produces rounder endcaps at the cost of more vertices
            const int verticesPerCap = primsPerCap * 2 + 2;
            const int primsPerCore = 4;
            const int verticesPerCore = 8;

            numInstances = 100;
            numVertices = (verticesPerCore + verticesPerCap + verticesPerCap) * numInstances;
            numPrimitivesPerInstance = primsPerCore + primsPerCap + primsPerCap;
            numPrimitives = numPrimitivesPerInstance * numInstances;
            numIndices = 3 * numPrimitives;
            short[] indices = new short[numIndices];
            bytesPerVertex = RoundLineVertex.SizeInBytes;
            RoundLineVertex[] tri = new RoundLineVertex[numVertices];
            translationData = new float[NumberOfFloatsPerInstance * numInstances]; // Used in Draw()
            colorData = new float[NumberOfFloatsPerInstance * numInstances]; // Used in Draw()

            int iv = 0;
            int ii = 0;
            int iVertex;
            int iIndex;
            for (int instance = 0; instance < numInstances; instance++)
            {
                // core vertices
                const float pi2 = (float)PiOver2;
                const float threePi2 = 3.0f * pi2;
                iVertex = iv;
                tri[iv++] = new RoundLineVertex(new Vector3(0.0f, -1.0f, 0), new Vector2(1, threePi2), new Vector2(0, 0), instance);
                tri[iv++] = new RoundLineVertex(new Vector3(0.0f, -1.0f, 0), new Vector2(1, threePi2), new Vector2(0, 1), instance);
                tri[iv++] = new RoundLineVertex(new Vector3(0.0f, 0.0f, 0), new Vector2(0, threePi2), new Vector2(0, 1), instance);
                tri[iv++] = new RoundLineVertex(new Vector3(0.0f, 0.0f, 0), new Vector2(0, threePi2), new Vector2(0, 0), instance);
                tri[iv++] = new RoundLineVertex(new Vector3(0.0f, 0.0f, 0), new Vector2(0, pi2), new Vector2(0, 1), instance);
                tri[iv++] = new RoundLineVertex(new Vector3(0.0f, 0.0f, 0), new Vector2(0, pi2), new Vector2(0, 0), instance);
                tri[iv++] = new RoundLineVertex(new Vector3(0.0f, 1.0f, 0), new Vector2(1, pi2), new Vector2(0, 1), instance);
                tri[iv++] = new RoundLineVertex(new Vector3(0.0f, 1.0f, 0), new Vector2(1, pi2), new Vector2(0, 0), instance);

                // core indices
                indices[ii++] = (short)(iVertex + 0);
                indices[ii++] = (short)(iVertex + 1);
                indices[ii++] = (short)(iVertex + 2);
                indices[ii++] = (short)(iVertex + 2);
                indices[ii++] = (short)(iVertex + 3);
                indices[ii++] = (short)(iVertex + 0);

                indices[ii++] = (short)(iVertex + 4);
                indices[ii++] = (short)(iVertex + 6);
                indices[ii++] = (short)(iVertex + 5);
                indices[ii++] = (short)(iVertex + 6);
                indices[ii++] = (short)(iVertex + 7);
                indices[ii++] = (short)(iVertex + 5);

                // left halfdisc
                iVertex = iv;
                iIndex = ii;
                for (int i = 0; i < primsPerCap + 1; i++)
                {
                    float deltaTheta = (float)Math.PI / primsPerCap;
                    float theta0 = (float)PiOver2 + i * deltaTheta;
                    float theta1 = theta0 + deltaTheta / 2;
                    // even-numbered indices are at the center of the halfdisc
                    tri[iVertex + 0] = new RoundLineVertex(new Vector3(0, 0, 0), new Vector2(0, theta1), new Vector2(0, 0), instance);

                    // odd-numbered indices are at the perimeter of the halfdisc
                    float x = (float)Math.Cos(theta0);
                    float y = (float)Math.Sin(theta0);
                    tri[iVertex + 1] = new RoundLineVertex(new Vector3(x, y, 0), new Vector2(1, theta0), new Vector2(1, 0), instance);

                    if (i < primsPerCap)
                    {
                        // indices follow this pattern: (0, 1, 3), (2, 3, 5), (4, 5, 7), ...
                        indices[iIndex + 0] = (short)(iVertex + 0);
                        indices[iIndex + 1] = (short)(iVertex + 1);
                        indices[iIndex + 2] = (short)(iVertex + 3);
                        iIndex += 3;
                        ii += 3;
                    }
                    iVertex += 2;
                    iv += 2;
                }

                // right halfdisc
                for (int i = 0; i < primsPerCap + 1; i++)
                {
                    float deltaTheta = (float)Math.PI / primsPerCap;
                    float theta0 = 3 * (float)PiOver2 + i * deltaTheta;
                    float theta1 = theta0 + deltaTheta / 2;
                    float theta2 = theta0 + deltaTheta;
                    // even-numbered indices are at the center of the halfdisc
                    tri[iVertex + 0] = new RoundLineVertex(new Vector3(0, 0, 0), new Vector2(0, theta1), new Vector2(0, 1), instance);

                    // odd-numbered indices are at the perimeter of the halfdisc
                    float x = (float)Math.Cos(theta0);
                    float y = (float)Math.Sin(theta0);
                    tri[iVertex + 1] = new RoundLineVertex(new Vector3(x, y, 0), new Vector2(1, theta0), new Vector2(1, 1), instance);

                    if (i < primsPerCap)
                    {
                        // indices follow this pattern: (0, 1, 3), (2, 3, 5), (4, 5, 7), ...
                        indices[iIndex + 0] = (short)(iVertex + 0);
                        indices[iIndex + 1] = (short)(iVertex + 1);
                        indices[iIndex + 2] = (short)(iVertex + 3);
                        iIndex += 3;
                        ii += 3;
                    }
                    iVertex += 2;
                    iv += 2;
                }
            }

            //set input layout
            vdecl = new SharpDX.Direct3D11.InputLayout(renderer.GraphicsDevice,fEffect.Signature, RoundLineVertex.VertexElements);

            //write vertex buffer
            fVB = new PenLink.Graphics.Resources.VertexBuffer(renderer);
            fVB.SetData(DataHelper.AnyToByteArray(tri, numVertices * bytesPerVertex));

            //Write index buffer
            fIB = new Resources.IndexBuffer(renderer);
            fIB.SetData(DataHelper.AnyToByteArray(indices, numIndices * 2));
        }


        /// <summary>
        /// Compute a reasonable "BlurThreshold" value to use when drawing RoundLines.
        /// See how wide lines of the specified radius will be (in pixels) when drawn
        /// to the back buffer.  Then apply an empirically-determined mapping to get
        /// a good BlurThreshold for such lines.
        /// </summary>
        public float ComputeBlurThreshold(float lineRadius, Matrix viewProjMatrix, float viewportWidth)
        {
            Vector4 lineRadiusTestBase = new Vector4(0, 0, 0, 1);
            Vector4 lineRadiusTest = new Vector4(lineRadius, 0, 0, 1);
            Vector4 delta = lineRadiusTest - lineRadiusTestBase;
            Vector4 output = Vector4.Transform(delta, viewProjMatrix);
            output.X *= viewportWidth;

            double newBlur = 0.125 * Math.Log(output.X) + 0.4;
            return MathHelper.Clamp((float)newBlur, 0.5f, 0.99f);
        }


        /// <summary>
        /// Draw a single RoundLine.  Usually you want to draw a list of RoundLines
        /// at a time instead for better performance.
        /// </summary>
        //public void Draw(RoundLine roundLine, float lineRadius, Color lineColor, Matrix viewProjMatrix,
        //    float time, string techniqueName)
        //{
        //    return;
        //    device.SetVertexBuffer(vb);
        //    //device.VertexDeclaration = vdecl;
        //    //device.Vertices[0].SetSource(vb, 0, bytesPerVertex);
        //    device.Indices = ib;


        //    viewProjMatrixParameter.SetValue(viewProjMatrix);
        //    timeParameter.SetValue(time);
        //    lineColorParameter.SetValue(lineColor.ToVector4());
        //    lineRadiusParameter.SetValue(lineRadius);
        //    blurThresholdParameter.SetValue(BlurThreshold);

        //    int iData = 0;
        //    int iColor = 0;

        //    translationData[iData++] = roundLine.P0.X * viewProjMatrix.M11;
        //    translationData[iData++] = roundLine.P0.Y;
        //    translationData[iData++] = roundLine.Rho;
        //    translationData[iData++] = roundLine.Theta;

        //    colorData[iColor++] = roundLine.LineColor.R / 255.0f;
        //    colorData[iColor++] = roundLine.LineColor.G / 255.0f;
        //    colorData[iColor++] = roundLine.LineColor.B / 255.0f;
        //    colorData[iColor++] = roundLine.LineColor.A / 255.0f;

        //    instanceDataParameter.SetValue(translationData);
        //    colorDataParameter.SetValue(colorData);

        //    if (techniqueName == null)
        //        effect.CurrentTechnique = effect.Techniques[0];
        //    else
        //        effect.CurrentTechnique = effect.Techniques[techniqueName];
        //    effect.CurrentTechnique.Passes[0].Apply();

        //    int numInstancesThisDraw = 1;
        //    device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, numVertices, 0, numPrimitivesPerInstance * numInstancesThisDraw);
        //    NumLinesDrawn += numInstancesThisDraw;
        //}



        private void FlushLines(EffectPass aPass, int numInstancesThisDraw)
        {
            GCHandle handle = new GCHandle();
            GCHandle handle1 = new GCHandle();
            try
            {
                int bytesThisDraw = numInstancesThisDraw * NumberOfFloatsPerInstance * SizeOfGPUFloat;
                handle = GCHandle.Alloc(translationData, GCHandleType.Pinned);
                instanceDataParameter.SetRawValue(handle.AddrOfPinnedObject(), 0, bytesThisDraw);
                handle1 = GCHandle.Alloc(colorData, GCHandleType.Pinned);
                colorDataParameter.SetRawValue(handle1.AddrOfPinnedObject(), 0, bytesThisDraw);
            }
            finally
            {
                if (handle.IsAllocated)
                    handle.Free();
                if (handle1.IsAllocated)
                    handle1.Free();
            }

            aPass.Apply(device.ImmediateContext);
            device.ImmediateContext.DrawIndexed(numPrimitivesPerInstance * numInstancesThisDraw * 3, 0, 0);
            NumLinesDrawn += numInstancesThisDraw;
        }

        /// <summary>
        /// Draw a list of Lines.
        /// </summary>
        public void Draw(List<RoundLine> roundLines, float lineRadius, Matrix viewProjMatrix,
            float time, string techniqueName)
        {
            device.ImmediateContext.InputAssembler.InputLayout = vdecl;
            device.ImmediateContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            device.ImmediateContext.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(fVB.GetBuffer(), RoundLineVertex.SizeInBytes, 0));
            device.ImmediateContext.InputAssembler.SetIndexBuffer(fIB.GetBuffer(),SharpDX.DXGI.Format.R16_UInt,0);

            viewProjMatrixParameter.AsMatrix().SetMatrix(viewProjMatrix);
            timeParameter.AsScalar().Set(time);
            lineColorParameter.AsVector().Set(Vector4.UnitW);
            lineRadiusParameter.AsScalar().Set(lineRadius);
            blurThresholdParameter.AsScalar().Set(BlurThreshold);

            SharpDX.Direct3D11.Effect effect = fEffect.DeviceEffect;

            EffectTechnique technique =  techniqueName == null ? effect.GetTechniqueByIndex(0) : effect.GetTechniqueByName(techniqueName);
            EffectPass pass = technique.GetPassByIndex(0);
            int iData = 0;
            int numInstancesThisDraw = 0;
            int iColor = 0;

            foreach (RoundLine roundLine in roundLines)
            {
                translationData[iData++] = roundLine.P0.X; 
                translationData[iData++] = roundLine.P0.Y;
                translationData[iData++] = roundLine.Rho;
                translationData[iData++] = roundLine.Theta;

                colorData[iColor++] = roundLine.LineColor.Red;
                colorData[iColor++] = roundLine.LineColor.Green;
                colorData[iColor++] = roundLine.LineColor.Blue;
                colorData[iColor++] = roundLine.LineColor.Alpha;

                numInstancesThisDraw++;

                if (numInstancesThisDraw == numInstances)
                {
                    FlushLines(pass, numInstancesThisDraw);
                    numInstancesThisDraw = 0;
                    iData = 0;
                    iColor = 0;
                }
            }

            if (numInstancesThisDraw > 0)
                FlushLines(pass,numInstancesThisDraw);
        }
    }
}