﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink
{

    public class FrameMonitor
    {

        private double fSampleInterval = 0.5;
        private int fTotalFrames = 0;
        private double fTotalTime = 1;
        private int fFrames = 0;
        private StopWatch fTiming = new StopWatch();


        public FrameMonitor()
        {
            this.fTiming.Reset();
            this.fTiming.Start();

        }

        public void Tick()
        {
            this.fFrames++;
            if (this.fTiming.ElapsedSeconds >= this.fSampleInterval)
            {
                this.fTotalTime = this.fTiming.ElapsedSeconds;
                this.fTotalFrames = this.fFrames;
                this.fFrames = 0;
                this.fTiming.Start();
            }
        }

        public double FPS { get { return  this.fTotalFrames / this.fTotalTime;}}
    }
}
