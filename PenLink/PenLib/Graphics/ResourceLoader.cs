using System;
using System.Collections.Generic;
using System.IO;

using SharpDX.Direct3D11;
using Effect = SharpDX.Direct3D11.Effect;
using Filter = SharpDX.Direct3D11.Filter;
using Texture2D = SharpDX.Direct3D11.Texture2D;
using SharpDX.D3DCompiler;
using SpriteTextRenderer.SharpDX;
//using Font = SharpDX.DirectWrite.Font;
//using Effect = SharpDX.Direct3D11.Effect;

namespace PenLink.Graphics
{
    public class ResourceLoader : Common.Singleton<ResourceLoader>
    {
        protected ResourceLoader()
        {
            fDicResources = new Dictionary<string, object>();
            fDicResourcesPaths = new Dictionary<string, object>();
        }


        Dictionary<string, object> fDicResources;
        Dictionary<string, object> fDicResourcesPaths;

        string fResourcesPath;
        string fTexturesPath;
        string fEffectsPath;
        string fFontsPath;

        public string SetResourcesFolder
        {
            set
            {
                if (fResourcesPath != value && !String.IsNullOrWhiteSpace(value))
                {
                    fResourcesPath = value;
                    fTexturesPath = Path.Combine(fResourcesPath, ResourceType.ID_TEXTURES);
                    fEffectsPath = Path.Combine(fResourcesPath, ResourceType.ID_EFFECTS);
                    fFontsPath = Path.Combine(fResourcesPath, ResourceType.ID_FONTS);
                }
            }
            get
            {
                return fResourcesPath;
            }
        }


        private string TryGetFullPathOfLibraryResource(string aType, string aResourceName)
        {

            string[] files = Directory.GetFiles(Path.Combine(fResourcesPath, aType), aResourceName + ".*");
            if (files.Length > 1)
                throw new Exception("having two file with same name is illegal");
            return files.Length > 0 ? files[0] : null;
        }


        private object LoadResource(string aType, string aFullPath, GraphicRenderer aDevice)
        {
            switch (aType)
            {
                case ResourceType.ID_EFFECTS:
                    return LoadEffectFromFile(aFullPath, aFullPath, aDevice);
                case ResourceType.ID_FONTS:
                    return LoadFontDescriptor(aDevice, aFullPath);
                case ResourceType.ID_TEXTURES:
                    return LoadTexture(aFullPath, aDevice);
                default:
                    return null;
            }
        }

        public object GetResource(string aType, string aResourceName, GraphicRenderer aDevice)
        {
            object resource = null;

            string LoadPendingfullPath = null;
            string FullResourceName = null;
            if (Path.IsPathRooted(aResourceName))
            {
                if (!fDicResourcesPaths.TryGetValue(aResourceName, out resource))
                    LoadPendingfullPath = aResourceName;
            }
            else
            {
                FullResourceName = aType + '/' + aResourceName;
                if (!fDicResources.TryGetValue(FullResourceName, out resource))
                    LoadPendingfullPath = TryGetFullPathOfLibraryResource(aType, aResourceName);
            }

            if (resource == null)
            {
                resource = LoadResource(aType, LoadPendingfullPath, aDevice);

                if (resource != null)
                {
                    if (LoadPendingfullPath != null)
                    {
                        fDicResourcesPaths.Add(LoadPendingfullPath, resource);
                        if (FullResourceName != null)
                            fDicResources.Add(FullResourceName, resource);
                    }
                }
            }

            return resource;
        }


        private Resources.Texture LoadTexture(string aFullPath, GraphicRenderer device)
        {
            
            return Resources.Texture.FromFile(device, aFullPath);
        }

        private SpriteTextRenderer.TextBlockRenderer LoadFontDescriptor(GraphicRenderer device, string aFullPath)
        {
            return new TextBlockRenderer(device.SpriteBatch, "Arial",  
                SharpDX.DirectWrite.FontWeight.Normal, SharpDX.DirectWrite.FontStyle.Normal, SharpDX.DirectWrite.FontStretch.Normal, 30);
        }

        private Graphics.Effect LoadEffectFromString(string aEffectID, string aEffectContent, GraphicRenderer aDevice)
        {
            try
            {
                return new Graphics.Effect(aEffectContent,aDevice);
            }

            catch (Exception e)
            {
                throw new Exception("Unable to compile effect " + aEffectID, e);
            }

        }

        private Effect LoadEffectFromFile(string aEffectID, string aFullPath, GraphicRenderer aDevice)
        {
            string effectcontent = File.ReadAllText(aFullPath);
            if (!String.IsNullOrWhiteSpace(effectcontent))
            {
                return LoadEffectFromString(aEffectID, effectcontent, aDevice);
            }

            return null;
        }
    }
}