﻿using System.Runtime.InteropServices;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace PenLink.Graphics
{
    // A vertex type for drawing RoundLines, including an instance index
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct RoundLineVertex
    {
        public Vector3 pos;
        public Vector2 rhoTheta;
        public Vector2 scaleTrans;
        public float index;


        public RoundLineVertex(Vector3 pos, Vector2 norm, Vector2 tex, float index)
        {
            this.pos = pos;
            this.rhoTheta = norm;
            this.scaleTrans = tex;
            this.index = index;
        }

        public static int SizeInBytes = 8 * sizeof(float);

        public static InputElement[] VertexElements = new InputElement[]
                                                     {
                                                         new InputElement("POSITION",   0, Format.R32G32B32_Float, 0, 0),
                                                         new InputElement("NORMAL",     0, Format.R32G32_Float, 12, 0),
                                                         new InputElement("TEXCOORD",   0, Format.R32G32_Float, 20, 0),
                                                         new InputElement("TEXCOORD",   1, Format.R32_Float, 28, 0),
                                                     };
    }
}
