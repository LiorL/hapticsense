using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Direct3D11;
using SharpDX.Direct3D;
using SharpDX;

namespace PenLink.Graphics
{
    public class Quad
    {
        GraphicRenderer fRenderer;
        InputLayout vd;
        Graphics.Resources.VertexBuffer vb;
        PenLink.Graphics.Effect fEffect;
        SharpDX.Direct3D11.EffectVariable fLineWidth;
        SharpDX.Direct3D11.EffectVariable fOuterRadius;
        SharpDX.Direct3D11.EffectVariable fBlendColor;
        SharpDX.Direct3D11.EffectVariable fRatio;
        SharpDX.Direct3D11.EffectVariable fBlur;
        int vertexSize = (3 + 4 + 2) * 4;

        public static InputElement[] VertexElements = new InputElement[]
                                                          {
                                                              new InputElement("POSITION",      0, SharpDX.DXGI.Format.R32G32B32_Float, 0, 0,InputClassification.PerVertexData,0),
                                                              new InputElement("COLOR",         0, SharpDX.DXGI.Format.R32G32B32A32_Float, 12, 0,InputClassification.PerVertexData,0),
                                                              new InputElement("TEXTCOORD",     0, SharpDX.DXGI.Format.R32G32_Float, 28, 0,InputClassification.PerVertexData,0),
                                                          };

        
        
        public Quad(GraphicRenderer aRenderer)
        {
             fRenderer = aRenderer;
        }

        public void SetOuterRadius(float aOuterRadius)
        {
            fOuterRadius.AsScalar().Set(aOuterRadius);
        }

        public void SetBlendColor(SharpDX.Color4 aColor)
        {
            fBlendColor.AsVector().Set(aColor);
        }

        public void Create()
        {
             vb = new Graphics.Resources.VertexBuffer(fRenderer);
             float size = 1.0f;
             float z = 0.0f;

             float[] data =  {             //position            //color                 //texture coordinates
                                           -size, size,z,       1.0f, 1.0f, 1.0f, 1.0f,       0f,0f,
                                           -size,-size,z,       1.0f, 1.0f, 1.0f, 1.0f,       0f,1.0f,
                                           size, size, z,      1.0f, 1.0f, 1.0f, 1.0f,        1.0f,0f,
                                           size, -size,z,      1.0f, 1.0f, 1.0f, 1.0f,        1.0f,1.0f
                             };

             byte[] dataByte = new byte[data.Length * 4];
             System.Buffer.BlockCopy(data, 0, dataByte, 0, data.Length * 4);
             vb.SetData(dataByte);

             fEffect = ResourceLoader.Instance.GetResource(ResourceType.ID_EFFECTS, "Simple", fRenderer) as Graphics.Effect;

             fLineWidth = fEffect.DeviceEffect.GetVariableByName("width");
             fOuterRadius = fEffect.DeviceEffect.GetVariableByName("outerRadius");
             fBlendColor = fEffect.DeviceEffect.GetVariableByName("blendColor");
             fRatio = fEffect.DeviceEffect.GetVariableByName("ratio");
             fBlur = fEffect.DeviceEffect.GetVariableByName("blur");

             fLineWidth.AsScalar().Set(0.005f);
             fRatio.AsScalar().Set(1.0f / fRenderer.BackBufferRatio);
             fBlur.AsScalar().Set(0.5f);
             

             vd = new SharpDX.Direct3D11.InputLayout(fRenderer.GraphicsDevice, fEffect.Signature, VertexElements);


        }


        public void Draw(DeviceContext aContext)
        {
            aContext.InputAssembler.InputLayout = vd;
            aContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleStrip;
            aContext.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(vb.GetBuffer(), vertexSize, 0));
            fEffect.DeviceEffect.GetTechniqueByIndex(0).GetPassByIndex(0).Apply(aContext);
            aContext.Draw(4, 0);    
        }
    }    
}
