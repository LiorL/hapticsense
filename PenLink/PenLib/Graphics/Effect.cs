﻿using SharpDX.D3DCompiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Graphics
{
    public class Effect
    {
        private SharpDX.Direct3D11.Effect fEffect;
        private ShaderBytecode fByteCode;
        ShaderBytecode fSignature;
        public Effect(string aEffectContent,GraphicRenderer aDevice)
        {
            ShaderFlags flags = ShaderFlags.None;
            
            #if DEBUG
                flags |= ShaderFlags.Debug;
            #else
                flags |= ShaderFlags.OptimizationLevel3;
            #endif


            fByteCode = SharpDX.D3DCompiler.ShaderBytecode.Compile(aEffectContent, "fx_5_0",flags,EffectFlags.None);
            fEffect = new SharpDX.Direct3D11.Effect(aDevice.GraphicsDevice, fByteCode);
            fSignature = fEffect.GetTechniqueByIndex(0).GetPassByIndex(0).Description.Signature;

        }

        public ShaderBytecode Signature { get { return fSignature; } }

        public ShaderBytecode ByteCode { get { return fByteCode; }}

        public SharpDX.Direct3D11.Effect DeviceEffect { get { return fEffect; } } 
    }
}
