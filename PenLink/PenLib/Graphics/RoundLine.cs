// RoundLine.cs
// By Michael D. Anderson
// Version 3.00, Mar 12 2009
//
// A class to efficiently draw thick lines with rounded ends.

#region Using Statements
using SharpDX;
using System;

#endregion


namespace RoundLineCode
{
    /// <summary>
    /// Represents a single line segment.  Drawing is handled by the RoundLineManager class.
    /// </summary>
    public partial class RoundLine
    {
        private Vector2 p0; // Begin point of the line
        private Vector2 p1; // End point of the line
        private float rho; // Length of the line
        private float theta; // Angle of the line
        private Color4 fColor;
        private float fScreenRatio;

        public Color4 LineColor { get { return fColor; } }

        public Vector2 P0 
        { 
            get 
            { 
                return p0; 
            }
            set
            {
                p0 = value;
                p0.X /= fScreenRatio;
                RecalcRhoTheta();
            }
        }
        public Vector2 P1 
        {
            get 
            { 
                return p1; 
            }
            set
            {
                p1 = value;
                p1.X /= fScreenRatio;
                RecalcRhoTheta();
            }
        }
        public float Rho { get { return rho; } }
        public float Theta { get { return theta; } }


        private void Init(Vector2 p0, Vector2 p1, Color4 color, float screenRatio)
        {
            fScreenRatio = screenRatio;
            this.P0 = p0;
            this.P1 = p1;
            SetColor(color);
            RecalcRhoTheta();
        }


        public RoundLine(Vector2 p0, Vector2 p1,Color4 color,float screenRatio)
        {
            Init(p0, p1, color, screenRatio);
        }


        public RoundLine(float x0, float y0, float x1, float y1, Color4 color,float screenRatio)
        {
            Init(new Vector2(x0, y0), new Vector2(x1, y1), color, screenRatio);
        }

        private void SetColor(Color4 color)
        {
            fColor = color;
        }


        protected void RecalcRhoTheta()
        {
            
            Vector2 delta = P1 - P0;
            rho = delta.Length();
            theta = (float)Math.Atan2(delta.Y, delta.X);
        }
    };
}
