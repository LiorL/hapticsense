﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PenLib.Graphics
{

    public interface IGraphicsHost
    {

        IntPtr Handle { get; }

        int Height { get; }

        int Width { get;}
        event EventHandler SizeChanged;

        event EventHandler Activated;

    }
}
