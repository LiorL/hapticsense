﻿using System;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SpriteTextRenderer.SharpDX;



namespace PenLink.Graphics
{
    
    public class Sprite
    {
        public enum CursorPlacement { None, Center, TopLeft, TopRight, BottomLeft, BottomRight };
        Vector2     fSize;
        Vector2     fPosition;
        Color4      fBlendColor;
        Matrix      fTransformation;
        Matrix      fMatOrigin;
        Matrix      fMatScale;
        Matrix      fMatTrans;
        bool        fIsVisible;
        int         fNumFrames;
        int         fCurrentFrame = -1;
        float       fFrameWidth;
        float       fFrameheight;
        Rectangle   fSourceRect;
        CursorPlacement fPlacement;

        public CursorPlacement Placement
        {
            get { return fPlacement; }
            set
            {
                if (fPlacement != value)
                {
                    fPlacement = value;
                    UpdateOriginScaleMatrix();
                }
            }
        }

        PenLink.Graphics.Resources.Texture fTexture;

        public Sprite(PenLink.Graphics.Resources.Texture aTexture, Vector2 aSize, Vector2 aPosition)
        {
            fMatScale = Matrix.Identity;
            fMatTrans = Matrix.Identity;
            fMatOrigin = Matrix.Identity;
            fTexture = aTexture;
            fCurrentFrame = 0;
            TotalFrames = 1;
            fSize = aSize;
            fPosition = aPosition;
            fBlendColor = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            IsVisible = true;
            RefreshSource();
            Placement = CursorPlacement.Center;
            UpdateTranslation();
        }

        public Sprite(PenLink.Graphics.Resources.Texture aTexture) : this(aTexture, Vector2.Zero, Vector2.Zero) { }

        public Sprite(){}

        public int CurrentFrame
        {
            get { return fCurrentFrame; }
            set
            {
                if (fCurrentFrame != value)
                {
                    fCurrentFrame = value;
                    UpdateSourceRect();
                }
            }
        }

        public int TotalFrames
        {
            get { return fNumFrames; }
            set
            {
                if (fNumFrames != value)
                {
                    fNumFrames = value;
                    RefreshSource();
                }
            }
        }

        private void RefreshSource()
        {
            if (fTexture != null)
            {
                Texture2DDescription desc = (fTexture.ResObject as SharpDX.Direct3D11.Texture2D).Description;
                fFrameWidth = ( desc.Width / fNumFrames);
                fFrameheight = desc.Height;
            }

            UpdateSourceRect();
            UpdateOriginScaleMatrix();
        }

        private void UpdateOriginScaleMatrix()
        {
            fMatScale[0,0] = fSize.X / fFrameWidth;
            fMatScale[1,1] = fSize.Y / fFrameheight;

            Vector2 origin = ComputePlacement();

            fMatOrigin[3, 0] = origin.X;
            fMatOrigin[3, 1] = origin.Y;
        
            
            UpdateTransformation();
        }
        

        public PenLink.Graphics.Resources.Texture Texture
        {
            get { return fTexture; }
            set
            {
                fTexture = value;
                RefreshSource();
            }
        }

        private void UpdateSourceRect()
        {
            if (fCurrentFrame != -1)
                fSourceRect = new Rectangle((int)(fFrameWidth * fCurrentFrame), 0, (int)fFrameWidth, (int)fFrameheight);
        }

        public Vector2 Size
        {
            get { return fSize; }
            set
            {
                fSize = value;
                UpdateOriginScaleMatrix();
            }
        }

        public bool IsVisible
        {
            get
            {
                return fIsVisible;
            }
            set
            {
                fIsVisible = value;
            }
        }

        public Vector2 Position
        {
            get 
            {
             return fPosition; 
             }
            set
            {
                fPosition = value;
                UpdateTranslation();

            }
        }
        
        private void UpdateTransformation()
        {
            fTransformation = Matrix.Multiply(fMatOrigin, Matrix.Multiply(fMatScale, fMatTrans));
        }

        private void UpdateTranslation()
        {
            fMatTrans[3, 0] = fPosition.X;
            fMatTrans[3, 1] = fPosition.Y;
            fMatTrans[3, 2] = 0.0f;
            UpdateTransformation();
        }
        public Color4 BlendColor
        {
            set { fBlendColor = value; }
        }


        private Vector2 ComputePlacement()
        {
            Vector2 baseOrigin = new Vector2(fFrameWidth, fFrameheight) / -2;
            Vector2 size = new Vector2(fFrameWidth,fFrameheight) / 2;
            switch (fPlacement)
            {
                case CursorPlacement.None:
                    break;
                case CursorPlacement.Center:
                    size = Vector2.Zero;
                    break;
                case CursorPlacement.TopLeft:
                    size.X *= -1;
                    size.Y *= -1;
                    break;
                case CursorPlacement.TopRight:
                    size.X *= 1;
                    size.Y *= -1;
                    break;
                case CursorPlacement.BottomLeft:
                    size.X *= -1;
                    size.Y *= 1;
                    break;
                case CursorPlacement.BottomRight:
                    size.X *= 1;
                    size.Y *= 1;
                    break;
                default:
                    break;
            }
            return size + baseOrigin;
        }

        public void Draw(SpriteRenderer spritebatch)
        {

            if ((fIsVisible) && (fTexture != null))
            {
                Vector3 scale;
                Vector3 position;
                Quaternion rotation;
                fTransformation.Decompose(out scale, out rotation, out position);
                
                Vector2 uvOffset = new Vector2(fCurrentFrame * (1.0f / TotalFrames), 0.0f);
                Vector2 uvSize =  new Vector2(1.0f / TotalFrames ,1.0f);

                spritebatch.Draw(fTexture.ResourceView,  new Vector2(position.X, position.Y), new Vector2(fSize.X, fSize.Y), uvOffset, uvSize, fBlendColor,SpriteTextRenderer.CoordinateType.Absolute);
            }      
        }
        /// <summary>
        /// Retunrs the distance between the center of two 2d objects
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public float Distance(Sprite o)
        {
            return (fPosition - o.fPosition).Length();
        }
        /// <summary>
        /// Retunrs the square distance between the center of two 2d objects
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public float DistanceSquared(Sprite o)
        {
            return (fPosition - o.fPosition).LengthSquared();
        }
        
        public bool IsContain(Sprite o)
        {
            return Distance(o) < (fSize.X / 2);
        }

    }
}


