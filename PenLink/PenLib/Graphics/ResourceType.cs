﻿namespace PenLink.Graphics
{
    public class ResourceType
    {
        public const string ID_TEXTURES = "Textures";
        public const string ID_EFFECTS = "Effects";
        public const string ID_FONTS = "Fonts";
    }
}