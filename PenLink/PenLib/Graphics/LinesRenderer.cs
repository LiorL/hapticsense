using System.Collections.Generic;
using RoundLineCode;
using SharpDX;
using SharpDX.Direct3D11;
using SpriteTextRenderer.SharpDX;

namespace PenLink.Graphics
{
    public class LinesRenderer
    {
        public PenLink.Graphics.Resources.Texture LinesSurface;
        private Matrix projMatrix;
        private float fWidth = 0;
        private float fHeight = 0;
        private RoundLineManager roundLineManager;
        private List<RoundLine> lines = new List<RoundLine>();
        private List<RoundLine> Alllines = new List<RoundLine>();
        private GraphicRenderer fRenderer;

        public LinesRenderer(float aWidth, float aheight, GraphicRenderer renderer)
        {
            roundLineManager = new RoundLineManager();
            roundLineManager.Init(renderer);
            fRenderer = renderer;
            LinesSurface = new Resources.Texture(fRenderer);
            LinesSurface.BackGroundColor = Color.Zero;
            Resize(aWidth, aheight);
        }

        private void UpdateSurface()
        {
            LinesSurface.Width = (int)fWidth;
            LinesSurface.Height = (int)fHeight;

            LinesSurface.Destory();
            LinesSurface.Create();
            Clear(fRenderer);
            Create2DProjectionMatrix();
        }

        public void Resize(float aWidth, float aHeight)
        {
            fWidth = aWidth;
            fHeight = aHeight;
            UpdateSurface();
        }


        private void Create2DProjectionMatrix()
        {
            float vpr = (float)fWidth / fHeight;
            projMatrix = Matrix.OrthoOffCenterRH(-1 * vpr, 1.0f * vpr, -1.0f, 1.0f, -10.0f, 10.0f);
        }

        public void Draw(GraphicRenderer renderer)
        {
            if (lines.Count > 0)
            {
                const float desiredRadius = 0.0045f;
                float fixedRaduis = fHeight > fWidth ? desiredRadius / (fHeight / fWidth) : desiredRadius;
                roundLineManager.Draw(lines, fixedRaduis, projMatrix, 0, roundLineManager.TechniqueNames[0]);
                lines.Clear();
            }
        }

        public void Clear(GraphicRenderer renderer)
        {
            LinesSurface.Clear();
        }

        public void Add(RoundLine roundLine)
        {
            Alllines.Add(roundLine);
            lines.Add(roundLine);
        }

        public void DrawSurface(SpriteRenderer Sprite)
        {
            Sprite.Draw(LinesSurface.ResourceView, Vector2.Zero, new Vector2(fWidth, fHeight), SpriteTextRenderer.CoordinateType.Absolute);
        }

        public void DrawToSurface(GraphicRenderer fGraphicRenderer)
        {
            if (lines.Count > 0)
            {
                DeviceContext context = this.fRenderer.GraphicsDevice.ImmediateContext;
                RenderTargetView backBuffer = context.OutputMerger.GetRenderTargets(1)[0];
                context.OutputMerger.SetTargets(LinesSurface.RenderTarget);
                Draw(fGraphicRenderer);
                context.OutputMerger.SetTargets(backBuffer);
                backBuffer.Dispose();
            }

        }
    }
}
