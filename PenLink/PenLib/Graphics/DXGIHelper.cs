﻿using SharpDX.DXGI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Graphics
{
    public enum OutPutChooseMethod { None, Primary, Second, Last }
    public class DXGIHelper
    {
        public static Output FindOutPutByName(string aName,int aAdapterIdx)
        {
            Factory factory = new Factory();
            Adapter adapter = factory.GetAdapter(aAdapterIdx);
            Output choosenOutput = null;
            foreach (Output output in adapter.Outputs)
            {
                string outputName = output.Description.DeviceName;
                if (outputName.ToLower().Contains(aName.ToLower()))
                {
                    choosenOutput = output;
                    break;
                }
            }

            if (choosenOutput != null)
                return choosenOutput;

            return choosenOutput;
        }

        internal static Output FindOutPutByIndex(int aOutputIndex, int aAdapterIdx)
        {
            Factory factory = new Factory();
            Adapter adapter = factory.GetAdapter(aAdapterIdx);
            Output choosenOutput = null;

            if (aOutputIndex >= 0 && aOutputIndex < adapter.Outputs.Length)
                choosenOutput = adapter.Outputs[aOutputIndex];

            return choosenOutput;
        }
        

        public static Output ChoostOutput(OutPutChooseMethod aChooseMethod, int aAdapterIdx)
        {
            Factory factory = new Factory();
            Adapter adapter = factory.GetAdapter(aAdapterIdx);
            int outputCount = adapter.Outputs.Count();
            int outputIndex = 0;
            switch (aChooseMethod)
            {
                case OutPutChooseMethod.None:
                    break;
                case OutPutChooseMethod.Primary:
                    outputIndex = 0;
                    break;
                case OutPutChooseMethod.Second:
                    if (outputCount > 1)
                        outputIndex = 1;
                    break;
                case OutPutChooseMethod.Last:
                    outputIndex = outputCount - 1;
                    break;
                default:
                    outputIndex = 0;
                    break;
            }
            return adapter.Outputs[outputIndex];
        }


        
    }
}
