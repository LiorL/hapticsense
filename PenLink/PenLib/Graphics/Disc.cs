﻿using RoundLineCode;
using SharpDX;

namespace PenLink.Graphics
{
    // A "degenerate" RoundLine where both endpoints are equal
    public class Disc : RoundLine
    {
        public Disc(Vector2 p,Color aColor,float aScreenRatio) : base(p, p,aColor,aScreenRatio) { }
        public Disc(float x, float y, Color aColor, float aScreenRatio) : base(x, y, x, y, aColor, aScreenRatio) { }
        public Vector2 Pos
        {
            get
            {
                return P0;
            }
            set
            {
                P0 = value;
                P1 = value;
            }
        }
    };

}
