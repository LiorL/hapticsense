﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Direct3D11;
using SharpDX.Direct3D;
using SharpDX;
using PenLink.Utilities;

namespace PenLink.Graphics
{
    class Grid
    {
        GraphicRenderer fRenderer;
        InputLayout vd;
        Graphics.Resources.VertexBuffer vb;
        Graphics.Resources.IndexBuffer ib;
        PenLink.Graphics.Effect fEffect;
        int vertexSize = (3 + 4 + 2) * 4;
        SharpDX.Direct3D11.EffectVariable fMVP;
        

        public static InputElement[] VertexElements = new InputElement[]
                                                          {
                                                              new InputElement("POSITION",      0, SharpDX.DXGI.Format.R32G32B32_Float, 0, 0,InputClassification.PerVertexData,0),
                                                              new InputElement("COLOR",         0, SharpDX.DXGI.Format.R32G32B32A32_Float, 12, 0,InputClassification.PerVertexData,0),
                                                              new InputElement("TEXTCOORD",     0, SharpDX.DXGI.Format.R32G32_Float, 28, 0,InputClassification.PerVertexData,0),
                                                          };
        private int fTotalIndices;


        struct GridVertex
        {
            public Vector3 position;
            public Vector4 color;
            public Vector2 UV0;
        }

        public Grid(GraphicRenderer aRenderer)
        {
            fRenderer = aRenderer;
        }

        public void Create()
        {
            vb = new Graphics.Resources.VertexBuffer(fRenderer);
            float []color = {1.0f,1.0f,1.0f,1.0f};
            const int IndicesPerQuad = 6;
            float width = 2f;
            float height = 2f;
            int columns = 40;
            int rows =  40;
            int horizontalQuads = (rows + 1);
            int verticalQuads = (columns + 1);
            int horizontalVerticesCount = (rows + 2);
            int verticalVerticesCount = (columns + 2);
            int totalQuads = horizontalQuads * verticalQuads;
            

            int totalVertices = horizontalVerticesCount * verticalVerticesCount;
            int totalIndices = totalQuads * IndicesPerQuad;
            fTotalIndices = totalIndices;
            
            byte []verticesData = new byte[(int)(totalVertices * vertexSize)];
            System.IO.MemoryStream ms = new System.IO.MemoryStream(verticesData);
            
            float horizontalStep = width / (rows + 1);
            float verticalStep = height / (columns + 1);
            Random r = new Random();
            for (int i = 0; i < horizontalVerticesCount; i++)
            {
                for (int j = 0; j < verticalVerticesCount; j++)
                {
                    GridVertex vertex;
                    float x = -(width / 2) + horizontalStep * i;
                    float y = -(height / 2) +  verticalStep  * j ;
                    float z = (float)r.NextDouble() / 3f;
                    
                    vertex.position = new Vector3(x,y,z);
                    vertex.color = new Vector4(r.NextFloat(0, 1), r.NextFloat(0, 1), r.NextFloat(0, 1), 1.0f);
                    vertex.UV0 = new Vector2(0.0f, 0.0f);
                    byte[] currentBuffer = DataHelper.AnyToByteArray(vertex,vertexSize);
                    
                    ms.Write(currentBuffer,0,vertexSize);
                    
                }
            }
            vb.SetData(verticesData);

            //Build indices
            

            UInt16 []indices = new UInt16[(int)totalIndices];
            int IndicesSize = (int)totalIndices * sizeof(UInt16);

            int currentIndex = 0;
            for (int i = 0; i < horizontalQuads; i++)
            {
                for (int j = 0; j < verticalQuads; j++)
                {
                    int offset = j + i * verticalVerticesCount;
                    //int columnOffset = j * 4;
                    //int rowOffset = i * 4;
                    //First triangle of the quad
                    indices[currentIndex++] = (UInt16)(offset);
                    indices[currentIndex++] = (UInt16)(offset + 1 );
                    indices[currentIndex++] = (UInt16)(offset + verticalVerticesCount);
                    //Second triangle of the quad
                    indices[currentIndex++] = (UInt16)(offset + 1 );
                    indices[currentIndex++] = (UInt16)(offset + 1 + verticalVerticesCount );
                    indices[currentIndex++] = (UInt16)(offset + verticalVerticesCount);
             
                }
                
            }

            ib = new Resources.IndexBuffer(fRenderer);
            ib.SetData(DataHelper.AnyToByteArray(indices, IndicesSize));
            fEffect = ResourceLoader.Instance.GetResource(ResourceType.ID_EFFECTS, "Standard", fRenderer) as Graphics.Effect;

            fMVP = fEffect.DeviceEffect.GetVariableByName("mvp");

            Matrix m = Matrix.RotationYawPitchRoll(0, -70, 0);

            
            
            
            //Matrix m = Matrix.Identity;
            
            fMVP.AsMatrix().SetMatrix(m);
            vd = new SharpDX.Direct3D11.InputLayout(fRenderer.GraphicsDevice, fEffect.Signature, VertexElements);
        }

        float fRotation = 0;
        public void Draw(DeviceContext aContext)
        {
            Quaternion q = Quaternion.RotationYawPitchRoll(0, fRotation,0);
            Matrix m = Matrix.RotationQuaternion(q);
            fMVP.AsMatrix().SetMatrix(m);
            
            fRotation += 0.01f;
            aContext.InputAssembler.InputLayout = vd;
            aContext.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;
            aContext.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(vb.GetBuffer(), vertexSize, 0));
            aContext.InputAssembler.SetIndexBuffer(ib.GetBuffer(), SharpDX.DXGI.Format.R16_UInt, 0);
            fEffect.DeviceEffect.GetTechniqueByIndex(0).GetPassByIndex(0).Apply(aContext);
            aContext.DrawIndexed(fTotalIndices, 0, 0);
            
        }
    }
}
