﻿using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.WIC;
using System.Runtime.InteropServices;
namespace PenLink.Graphics.Resources
{
    
    
    public class Texture : Resource
    {
        private enum TextureSource { None, File, Runtime };

        Texture2D               fTexture;
        TextureSource           fTextureSOurce;
        Texture2DDescription    fTextureDesc;
        RenderTargetView        fRendertargetView;

        public RenderTargetView RenderTarget { get { return fRendertargetView;}}
         

        private Color fColor = Color.Transparent;

        private int fWidth;

        public int Width
        {
            get { return fWidth; }
            set { fTextureDesc.Width = fWidth = value;}
        }

        private int fHeight;

        public int Height
        {
            get { return fHeight; }
            set {fTextureDesc.Height = fHeight = value; }
        }


	public Color BackGroundColor
	{
		get { return fColor;}
		set { fColor = value;}
	}
        
        public Texture(GraphicRenderer aGraphicRendeder) : base(aGraphicRendeder) 
        { 
             fTextureSOurce = TextureSource.Runtime;
             fTextureDesc = new Texture2DDescription();
             fTextureDesc.CpuAccessFlags = CpuAccessFlags.None;
             fTextureDesc.Format = SharpDX.DXGI.Format.B8G8R8A8_UNorm;
             fTextureDesc.Height = fHeight;
             fTextureDesc.Width = fWidth;
             fTextureDesc.Usage = ResourceUsage.Default;
             fTextureDesc.MipLevels = 1;
             fTextureDesc.ArraySize = 1;
             fTextureDesc.OptionFlags = ResourceOptionFlags.None;
             fTextureDesc.BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource;
             fTextureDesc.SampleDescription = new SharpDX.DXGI.SampleDescription(1,0);

             
        }

        protected override SharpDX.Direct3D11.Resource DeviceObject
        {
            get
            {
                return fTexture;
            }
            set
            {
                fTexture = (Texture2D)value;
            }
        }

        

        protected override void PerformCreate()
        {
            switch (fTextureSOurce)
            {
                case TextureSource.Runtime:
                    fTexture = new Texture2D(this.Renderer.GraphicsDevice, fTextureDesc);
                    Reinit();
                    Clear();
                    break;
                case TextureSource.File:
                    //TODO: reload file
                    break;
            }
            
            
        }


        private void Reinit()
        {
             
            fShaderResourceView = new  ShaderResourceView(Renderer.GraphicsDevice,fTexture);
            fWidth = fTexture.Description.Width;
            fHeight = fTexture.Description.Height;
            if (fTextureSOurce == TextureSource.Runtime)
                fRendertargetView = new RenderTargetView(Renderer.GraphicsDevice, fTexture);
        }

        

        public static Texture FromFile(GraphicRenderer aDevice,string aFullPath)
        {
            ImagingFactory fac = new ImagingFactory();

            BitmapDecoder bc;
            try
            {
                 bc = new SharpDX.WIC.BitmapDecoder(fac, aFullPath, DecodeOptions.CacheOnLoad);
            }
            catch (SharpDXException exc)
            {
                return null;
            }

            BitmapFrameDecode bfc =  bc.GetFrame(0);
            FormatConverter fc = new FormatConverter(fac);
            System.Guid desiredFormat = PixelFormat.Format32bppBGRA;
            fc.Initialize(bfc, desiredFormat);
            Texture result = new Texture(aDevice);
            
            float[] buffer = new float[fc.Size.Width * fc.Size.Height];
               
            
            bool canConvert = fc.CanConvert(bfc.PixelFormat, desiredFormat);
            
            fc.CopyPixels<float>(buffer);
            GCHandle handle =  GCHandle.Alloc(buffer,GCHandleType.Pinned);
            float sizeOfPixel = PixelFormat.GetBitsPerPixel(desiredFormat) / 8;
            if (sizeOfPixel != 4.0f)
                throw new System.Exception("Unknown error");

            
            DataBox db = new DataBox(handle.AddrOfPinnedObject(), fc.Size.Width * (int)sizeOfPixel, fc.Size.Width * fc.Size.Height * (int)sizeOfPixel );
            result.fWidth =  result.fTextureDesc.Width =  fc.Size.Width;
            result.fHeight = result.fTextureDesc.Height = fc.Size.Height;

            result.fTexture = new Texture2D(aDevice.GraphicsDevice,result.fTextureDesc,new DataBox[] {db}); 
            handle.Free();
            result.fTextureSOurce = TextureSource.File;
            
            
            result.fSource = aFullPath;
            result.Reinit();
            
            return result;
        }
            

        
        public void Clear()
        {
            if (fTextureSOurce == TextureSource.Runtime)
                this.Renderer.GraphicsDevice.ImmediateContext.ClearRenderTargetView(fRendertargetView,new SharpDX.Color4( fColor.ToVector4()));
        }
    }
}