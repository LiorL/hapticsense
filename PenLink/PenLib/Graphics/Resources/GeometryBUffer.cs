﻿using SharpDX;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Graphics.Resources
{
    public enum BufferType { VertexBuffer, IndexBuffer }
    public class GeometryBuffer : Resource
    {
        SharpDX.Direct3D11.Buffer fGeometryBuffer;
        private byte[] fData;

        BufferType fBufferType;

        public GeometryBuffer(GraphicRenderer renderer, BufferType aBufferType)
            : base(renderer)
        {
            fBufferType = aBufferType;
        }

        protected override SharpDX.Direct3D11.Resource DeviceObject
        {
            get
            {
                return fGeometryBuffer;
            }
            set
            {
                fGeometryBuffer = (SharpDX.Direct3D11.Buffer)value;
            }
        }
        public void SetData(byte[] aData)
        {
            fData = aData;
            Destory();
            Create();

        }

        public SharpDX.Direct3D11.Buffer GetBuffer() { return fGeometryBuffer; }

        protected override void PerformCreate()
        {
            BufferDescription bufferDesc = new BufferDescription();
            bufferDesc.BindFlags = fBufferType == BufferType.IndexBuffer ? BindFlags.IndexBuffer : BindFlags.VertexBuffer;
            bufferDesc.CpuAccessFlags = CpuAccessFlags.Write;
            bufferDesc.SizeInBytes = fData.Length;
            bufferDesc.Usage = ResourceUsage.Dynamic;
            fGeometryBuffer = new SharpDX.Direct3D11.Buffer(Renderer.GraphicsDevice, bufferDesc);
            DataStream ds;
            SharpDX.DataBox data = Renderer.GraphicsDevice.ImmediateContext.MapSubresource(fGeometryBuffer, MapMode.WriteDiscard, MapFlags.None, out ds);
            ds.Write(fData, 0, fData.Length);
            ds.Dispose();
            Renderer.GraphicsDevice.ImmediateContext.UnmapSubresource(fGeometryBuffer, 0);

        }
    }
}

