﻿using SharpDX;
using SharpDX.Direct3D11;
namespace PenLink.Graphics.Resources
{
    public class VertexBuffer : GeometryBuffer
    {
        public VertexBuffer(GraphicRenderer renderer) : base(renderer, BufferType.VertexBuffer) { }
    }
}
