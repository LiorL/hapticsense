﻿namespace PenLink.Graphics.Resources
{
    public abstract class Resource
    {
        protected SharpDX.Direct3D11.ShaderResourceView fShaderResourceView;
        public string fSource;
        //Public interface
        public  GraphicRenderer Renderer{ get {return  fRenderer;}}
        public SharpDX.Direct3D11.Resource ResObject { get { return DeviceObject; } }

        public SharpDX.Direct3D11.ShaderResourceView ResourceView { get 
        {
            if (fShaderResourceView == null)
                throw new System.Exception("Shader resource view is not initialized");
            return fShaderResourceView; 
        } }
        
        protected abstract SharpDX.Direct3D11.Resource DeviceObject { get; set; }
        
        private readonly GraphicRenderer fRenderer;

        protected Resource(GraphicRenderer aRenderer)
        {
            fRenderer = aRenderer;
            fRenderer.Register(this);
        }

        public void Destory()
        {
            if (DeviceObject != null/* &&  DeviceObject.IsDefaultPool*/)
            {
                DeviceObject.Dispose();
                DeviceObject = null;
            }
        }

        public void Create()
        {
            if (DeviceObject == null)
                PerformCreate();
        }

        protected abstract void PerformCreate();

        ~Resource()
        {
            fRenderer.UnRegisterResource(this);
        }
    }
}