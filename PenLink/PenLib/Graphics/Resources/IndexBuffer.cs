﻿using SharpDX;
using SharpDX.Direct3D11;
namespace PenLink.Graphics.Resources
{
    public class IndexBuffer : GeometryBuffer
    {
        public IndexBuffer(GraphicRenderer renderer) : base(renderer, BufferType.IndexBuffer) { }
    }
}
