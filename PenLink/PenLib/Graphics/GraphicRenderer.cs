﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

using SharpDX.Direct3D;
using SpriteTextRenderer.SharpDX;
using PenLink.Graphics;
using PenLink.Utilities;
using PenLib.Graphics;

namespace PenLink
{
    public class GraphicRenderer
    {
        public enum VSyncMode {Disabled, Normal, Custom};
        
        public delegate void OnCustomInputDelegate();
        public delegate void FullScreenToggleDelegate(bool fullScreen);

        //public MethodInvoker fOnRender;
        public event MethodInvoker OnRender;
        public FullScreenToggleDelegate fOnFullScreenToggle;
        private SharpDX.Direct3D11.Device fgraphicsDevice;

        private bool fExclusiveModeBroken;
        private SwapChain fSwapChain;
        private FrameMonitor fFrameMonitor;
        private SwapChainDescription fParameters;
        private bool fUseFlipMode;
        public FrameMonitor FrameMonitor { get { return fFrameMonitor; } }
        private bool fEnd;
        private bool fIsFullScreen;
        private bool fIsDebug;
        private bool fIsFixedBackBufferSize;
        Vector2 fFixedBackBufferSize;
        private int fClientHeight;
        private int fClientWidth;
        private Color fBackgroundColor;
        private VSyncMode fVsync;
        private int fAntiAliasing;
        private IGraphicsHost fGraphicsHost;



        private SpriteRenderer fSpriteBatch;
        private int MaxMultiSample = 0;
        private int fWorkingAdapter = 0;
        private List<PenLink.Graphics.Resources.Resource> fResources;
        private RenderTargetView fRendertargetView;
        public Rectangle ScreenBounds;
        public SharpDX.Vector2 ScreenCenter;
        public int DesiredMonitorIndex { get; set; }

        Output fPrimaryOutput;

        public GraphicRenderer(IGraphicsHost aGraphicsHost, bool aIsFullScreen, bool aIsFixedBackBufferSize)
        {
            if (aGraphicsHost == null)
                throw new Exception("Illegal Graphics host has been initialized");
            DesiredMonitorIndex = -1;
            #if DEBUG
            	fIsDebug = true;
			#else
	            fIsDebug = false;
			#endif
            fExclusiveModeBroken = false;
            fVsync = VSyncMode.Normal;
            fBackgroundColor = Color.Gray;
            fGraphicsHost = aGraphicsHost;
            
            fIsFixedBackBufferSize = aIsFixedBackBufferSize;
            fClientHeight = fGraphicsHost.Height;
            fClientWidth = fGraphicsHost.Width;
            fFrameMonitor = new FrameMonitor();
            fIsFullScreen = aIsFullScreen;


            fGraphicsHost.SizeChanged += new EventHandler(aForm_SizeChanged);
            fGraphicsHost.Activated += fForm_Activated;
        }

        public void CreateResources()
        {
            CreateDevice();
            InitActiveMonitor();
            fSpriteBatch = new SpriteRenderer(fgraphicsDevice);
            fResources = new List<Graphics.Resources.Resource>();
        }

        private void InitActiveMonitor()
        {
            UpdateOutput();
            Rectangle bounds = fPrimaryOutput.Description.DesktopBounds;
            if (bounds != Rectangle.Empty)
                BackBufferSize = new Vector2(bounds.Width, bounds.Height);
        }
     
        
        void fForm_Activated(object sender, EventArgs e)
        {
            RestoreExclusiveMode();
        }

        private void RestoreExclusiveMode()
        {
            if (fExclusiveModeBroken && fSwapChain.IsFullScreen == false)
                IsFullScreen = true;
        }



        public bool IsTrueFullScreen { get { return fSwapChain.IsFullScreen; } }


        public Color BackGroundColor
        {
            get
            {
                return fBackgroundColor;
            }
            set
            {
                fBackgroundColor = value;
            }
        }

        

        void aForm_SizeChanged(object sender, EventArgs e)
        {
            if (!fIsFixedBackBufferSize)
            {
                fClientHeight = fGraphicsHost.Height;
                fClientWidth = fGraphicsHost.Width;
                ResizeBuffers();
            }
        }

        public SharpDX.Direct3D11.Device GraphicsDevice
        {
            get { return fgraphicsDevice; }
        }

        public bool AllowFlipMode { get; set; }

        private void CreateDevice()
        {
            // Creating device (we accept dx10 cards or greater)
             FeatureLevel[] levels = {
                            FeatureLevel.Level_11_0,
                            FeatureLevel.Level_10_1,
                            FeatureLevel.Level_10_0
                        };

            fUseFlipMode = AllowFlipMode & SystemHelper.IsWindows8OrGreater();
            fParameters.BufferCount = fUseFlipMode ? 2 : 1;
            fParameters.Usage = Usage.BackBuffer | Usage.RenderTargetOutput;
            fParameters.ModeDescription = new ModeDescription(0, 0, new Rational(0, 0), Format.R8G8B8A8_UNorm);
            fParameters.SampleDescription = new SampleDescription(1, 0);
            fParameters.OutputHandle = fGraphicsHost.Handle;
            fParameters.IsWindowed = true;
            fParameters.SwapEffect = fUseFlipMode ? SharpDX.DXGI.SwapEffect.FlipSequential : SharpDX.DXGI.SwapEffect.Discard;

            SharpDX.Direct3D11.DeviceCreationFlags flags = fIsDebug ? SharpDX.Direct3D11.DeviceCreationFlags.Debug : SharpDX.Direct3D11.DeviceCreationFlags.None;

            FeatureLevel highestFeatureLevel =  SharpDX.Direct3D11.Device.GetSupportedFeatureLevel(new SharpDX.DXGI.Factory().Adapters[0]);

            if (highestFeatureLevel < FeatureLevel.Level_10_0)
                throw new Exception("Only cards with Directx10 and above are supported");
            
            
            SharpDX.Direct3D11.Device.CreateWithSwapChain(DriverType.Hardware, flags, levels, fParameters, out fgraphicsDevice, out fSwapChain);

            fgraphicsDevice.QueryInterface<SharpDX.DXGI.Device>().GetParent<SharpDX.DXGI.Adapter>().GetParent<SharpDX.DXGI.Factory>().MakeWindowAssociation(fGraphicsHost.Handle, WindowAssociationFlags.IgnoreAll);

            SharpDX.DXGI.Device1 dxgi1 = GraphicsDevice.QueryInterface<SharpDX.DXGI.Device1>();
            dxgi1.MaximumFrameLatency = 1;


        }

        private void UpdateOutput()
        {

            fPrimaryOutput = DXGIHelper.FindOutPutByIndex(DesiredMonitorIndex, fWorkingAdapter);
            if (fPrimaryOutput == null)
                fPrimaryOutput = DXGIHelper.ChoostOutput(OutPutChooseMethod.Second, fWorkingAdapter);

            ScreenBounds = fPrimaryOutput.Description.DesktopBounds;
        }
         
        public float BackBufferRatio
        {
            get
            {
                return (float)BackBufferSize.Y / (float)BackBufferSize.X;
            }
        }
        
        public void StopRendering()
        {
            fEnd = true;
        }

        public VSyncMode Vsync
        {
            get { return fVsync; }
            set {

                if (value != fVsync)
                {
                    fVsync = value;
                }
            }
        }


        public int AntiAliasing
        {
            get { return fAntiAliasing; }
            set
            {
                if (value != fAntiAliasing)
                {
                    fAntiAliasing = (int)MathHelper.Clamp(value,0,MaxMultiSample - 1);
                }
            }
        }


        public void ToggleFullScreen()
        {
            IsFullScreen = !IsFullScreen;
        }

        public bool IsFullScreen 
        { 
            get 
            { 
                return fIsFullScreen; 
            } 

            set
            {
                if (value != fIsFullScreen || (fExclusiveModeBroken && fIsFullScreen && !fSwapChain.IsFullScreen))
                {
                    fExclusiveModeBroken = false;
                    fIsFullScreen = value;

                    if (fUseFlipMode)
                    {
                        if (fRendertargetView != null)
                        {
                            fRendertargetView.Dispose();
                            fRendertargetView = null;
                        }
                        
                        fgraphicsDevice.ImmediateContext.OutputMerger.ResetTargets();
                    }

                    fSwapChain.SetFullscreenState(fIsFullScreen, fIsFullScreen ? fPrimaryOutput : null);

                    ResizeBuffers();

                    if (fOnFullScreenToggle != null)
                    {
                        //TODO: restore this functionality 
                        //fGraphicsHost.BeginInvoke(fOnFullScreenToggle, fIsFullScreen);
                    }
                }
            }
        }


        private void ResizeBuffers()
        {
            Vector2 size = BackBufferSize;

            if (fRendertargetView != null)
            {
                fRendertargetView.Dispose();
                fRendertargetView = null;
            }
            
            fSwapChain.ResizeBuffers(fParameters.BufferCount, (int)size.X, (int)size.Y, Format.R8G8B8A8_UNorm, SwapChainFlags.None);

            Texture2D backBuffer = SharpDX.Direct3D11.Resource.FromSwapChain<SharpDX.Direct3D11.Texture2D>(fSwapChain, 0);
            fRendertargetView = new RenderTargetView(fgraphicsDevice, backBuffer);

            fgraphicsDevice.ImmediateContext.Rasterizer.SetViewport(new Viewport(0, 0, (int)size.X, (int)size.Y, 0.0f, 1.0f));
            fgraphicsDevice.ImmediateContext.OutputMerger.SetTargets(fRendertargetView);

            backBuffer.Dispose();
            if (fSpriteBatch != null)
                fSpriteBatch.RefreshViewport();
        }

        public Vector2 BackBufferSize { 
            get 
            {
                if (fIsFixedBackBufferSize)
                    return fFixedBackBufferSize;
                else
                    return new Vector2(fClientWidth, fClientHeight);
            }
            set
            {
                bool sizeChanged = BackBufferSize != value;
                fFixedBackBufferSize = value;
                ScreenCenter = BackBufferSize / 2;
                if (sizeChanged)
                    ResizeBuffers();
            }

        }

        public void Clear()
        {
            Clear(fBackgroundColor);
        }

        public void Clear(Color4 aColor)
        {
            fgraphicsDevice.ImmediateContext.ClearRenderTargetView(fRendertargetView, aColor);
        }
        
        public void Render()
        {
            fEnd = false;
            

            while (!fEnd)
            {
                if (IsFullScreen && !fSwapChain.IsFullScreen && !fExclusiveModeBroken)
                {
                    fExclusiveModeBroken = true;
                    if (fUseFlipMode)
                        ResizeBuffers();
                }


                if (OnRender!= null)
                    OnRender();

                if (fVsync == VSyncMode.Custom)
                    fSwapChain.ContainingOutput.WaitForVerticalBlank();

                if (fUseFlipMode)
                     fgraphicsDevice.ImmediateContext.OutputMerger.ResetTargets();
                
                fSwapChain.Present(Vsync == VSyncMode.Normal ? 1 : 0, PresentFlags.None);
                
                if (fUseFlipMode)
                    fgraphicsDevice.ImmediateContext.OutputMerger.SetTargets(fRendertargetView);

                fFrameMonitor.Tick();
                Application.DoEvents();
            }
        }
        public SpriteTextRenderer.SharpDX.SpriteRenderer SpriteBatch { get { return fSpriteBatch; } }

        internal void Register(Graphics.Resources.Resource resource)
        {
            fResources.Add(resource);
        }

        internal void UnRegisterResource(Graphics.Resources.Resource resource)
        {
            //TODO: implement
        }
    }
}