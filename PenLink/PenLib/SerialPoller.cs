﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;
using PenLink;
using PenLink.Utilities;

namespace Serial
{
    public class SerialPoller
    {
        private const int MaximumBufferSize = 4096;
        private const int Header = 255;

        SerialPort  fPort;
        byte[]      fComBuffer;
        int         fOffset;
        bool        fPortExist;

        public SerialPoller(string PortName, int BaudRate)
        {
            Init(PortName, BaudRate);
        }

        private void Init(string PortName, int BaudRate)
        {
            fPortExist = IOHelper.IsSerialPortExist(PortName);
            fPort = new SerialPort(PortName);
            fPort.BaudRate = BaudRate;
            fPort.Parity = Parity.None;
            fPort.StopBits = StopBits.One;
            fPort.ReadTimeout = 500;
            fComBuffer = new byte[MaximumBufferSize];
        }

        //cyclic buffer read to fComBuffer.
        public byte[] SerialPolling()
        {
            int bytestoread = fPort.BytesToRead;

            if (bytestoread > 0)
            {
                if (bytestoread > MaximumBufferSize)
                {
                    //TODO: read directly to 'fComBuffer' and don't use 'tempbuffer'
                    //Fill all buffer from offset 0 with new data
                    byte[] tempbuffer = new byte[bytestoread];
                    fPort.Read(tempbuffer, 0, bytestoread);
                    fOffset = MaximumBufferSize;
                    Array.Copy(tempbuffer, tempbuffer.Length - MaximumBufferSize, fComBuffer, 0, MaximumBufferSize);
                }
                else if (fOffset + bytestoread < MaximumBufferSize)
                {
                    //fill buffer from offset (fOffset) with new data
                    fPort.Read(fComBuffer, fOffset, bytestoread);
                    fOffset = (fOffset + bytestoread) % MaximumBufferSize;
                }
                else
                {
                    //Fill buffer  to the end, and loop to offset (0)
                    int read1 = MaximumBufferSize - fOffset;
                    int read2 = bytestoread - read1;

                    fPort.Read(fComBuffer, fOffset, read1);
                    fPort.Read(fComBuffer, 0, read2);
                    fOffset = read2;
                }

                byte[] Packet = BuildPacket();

                return Packet;
            }
            return null;
        }

        public bool IsOpen { get { return fPortExist && fPort.IsOpen; } }

        public virtual void Open()
        {
            if (fPortExist)
                fPort.Open();
        }

        public virtual void Close()
        {
            fPort.Close();

        }
        private int FindHeader(int end)
        {
            int headerIDX = end;
            while (fComBuffer[headerIDX % MaximumBufferSize] != Header)
            {
                headerIDX = headerIDX - 1;
                if (headerIDX < 0)
                    headerIDX += MaximumBufferSize;
                if (headerIDX == end)
                    return -1;
            }
            return headerIDX;
        }


        private byte[] GetSubBuffer(int start, int end)
        {
            byte[] subBuffer = new byte[Distance(start, end)];

            if (start < end)
            {
                int size = end - start;

                Array.Copy(fComBuffer, start, subBuffer, 0, size);
            }
            else
            {
                int read1 = MaximumBufferSize - start;
                int read2 = end;
                Array.Copy(fComBuffer, start, subBuffer, 0, read1);
                Array.Copy(fComBuffer, 0, subBuffer, read1, end);

            }
            return subBuffer;

        }


        private int Distance(int start, int end)
        {
            if (start == end)
                return 0;
            else if (start < end)
                return end - start;
            else
                return (MaximumBufferSize - start + end);
        }

        private byte[] BuildPacket()
        {
            int Headeridx = FindHeader(fOffset);
            int LastHeaderIdx = FindHeader((Headeridx + MaximumBufferSize - 1) % MaximumBufferSize);

            int HeaderLength = Distance(LastHeaderIdx, Headeridx);

            int dist = Distance(Headeridx, fOffset);


            if (dist == 0)
                return null;



            byte[] first = GetSubBuffer(Headeridx + 1, fOffset);

            byte[] Second = null;
            int Remaning = HeaderLength - dist;
            if (Remaning > 0)
            {
                Second = GetSubBuffer(((Headeridx - Remaning) + MaximumBufferSize) % MaximumBufferSize, Headeridx);
            }

            byte[] result = new byte[HeaderLength];

            first.CopyTo(result, 0);

            if (Second != null)
            {
                Second.CopyTo(result, first.Length);
            }

            return result;

        }
    }

    class AsyncSerialPoller : SerialPoller
    {
        public  delegate void ParsePacketDelegate(byte[] packet);
        ParsePacketDelegate ParsePacket;
        Thread pollingThread;
        Control Parent;
        int Interval;

        public override void Open()
        {
            base.Open();
            if (pollingThread != null)
                pollingThread.Start();

        }

        public override void Close()
        {
            
            if (pollingThread != null)
                pollingThread.Abort();
            base.Close();
        }

        public AsyncSerialPoller(Control control, string PortName, int BaudRate, int pollingInterVal, ParsePacketDelegate ParsePacketDelegate) :base(PortName,BaudRate)
        {
            Parent = control;
            Interval = pollingInterVal;
            ParsePacket = ParsePacketDelegate;

            pollingThread = new Thread(StartSerialPolling);
            pollingThread.IsBackground = true;

        }

        void StartSerialPolling()
        {
            while (true)
            {
                byte []Packet=  SerialPolling();
                if (Packet != null && !(Parent == null || Parent.IsDisposed))
                    Parent.Invoke(ParsePacket, Packet);
                Thread.Sleep(Interval);
            }
  
        }
    }
}
