﻿using PenLink.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace PenLink.Extensions
{
    public static class GeneralExtensions
    {
         //Deep clone
        public static T DeepClone<T>(this T a)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }
        

        public static string ToStringFixed2(this float num)
        {
            return FormattingHelper.numToStringFixed2(num);
        }

        public static string ToStringFixed2(this double num)
        {
            return FormattingHelper.numToStringFixed2(num);
        }
        public static bool IsActive(this SharpDX.DirectInput.MouseState A)
        {
            bool movement = A.X != 0 || A.Y != 0 || A.Z != 0;
            bool buttons = false;
            if (!movement)
            {
                foreach (bool press in A.Buttons)
                    if (press == true)
                    {
                        buttons = true;
                        break;
                    }
            }

            return movement || buttons;
        }

        public static SharpDX.Color ToSharpDXColor(this System.Drawing.Color aColor)
        {
            return new SharpDX.Color(aColor.R, aColor.G, aColor.B, aColor.A);
        }

        public static System.Drawing.Rectangle ToSystemRect(this SharpDX.Rectangle aRect)
        {
            System.Drawing.Point pnt = new System.Drawing.Point(aRect.Left, aRect.Top);
            System.Drawing.Size size = new System.Drawing.Size(aRect.Width, aRect.Height);
            return new System.Drawing.Rectangle(pnt, size);
        }

        public static double GetBrightness(this SharpDX.Color aColor)
        {
            return Math.Sqrt(.241 * aColor.R * aColor.R + .691 * aColor.G * aColor.G + .068 * aColor.B * aColor.B);
        }

        public static void CenterRelativeTo(this System.Windows.Forms.Control child,  System.Windows.Forms.Control parent)
        {
            child.Location = new System.Drawing.Point(parent.Left + (parent.Width - child.Width) / 2, parent.Top + (parent.Height - child.Height) / 2);
        }
    }

}
