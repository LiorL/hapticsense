﻿using System;
using System.Windows.Forms;
using PenLink.Utilities;
using Message = System.Windows.Forms.Message;


namespace PenLib
{
    class Filter : IMessageFilter
    {
        private Form fForm;
        public Filter(Form aForm)
        {
            fForm = aForm;
        }
        public bool PreFilterMessage(ref Message m)
        {
            if (m.Msg == SingleInstance.WM_SHOME)
            {
                if (fForm.WindowState == FormWindowState.Minimized)
                {
                    fForm.WindowState = FormWindowState.Normal;
                }
                // get our current "TopMost" value (ours will always be false though)
                bool top = fForm.TopMost;
                // make our form jump to the top of everything
                fForm.TopMost = true;
                // set it back to whatever it was
                fForm.TopMost = top;
                return true;
            }
            return false;
        }
    }

    public class SingleInstance
    {
        public static int WM_SHOME = SystemHelper.RegisterWindowMessage("SHOW_ME");
        private string fGuid;

        public string SingleInstanceGuid => fGuid;
        private System.Threading.Mutex mutex;
        public bool IsAlreadyRunning { get { return !mutex.WaitOne(TimeSpan.Zero, true); } }
        public SingleInstance(string guid)
        {
            fGuid = guid;
            mutex = new System.Threading.Mutex(true, fGuid);
        }
    }

    public class SingleInstanceForm : SingleInstance
    {
        private Form fForm;
        public SingleInstanceForm(string aGuid) : base(aGuid)
        {
            
        }

        public void SetForm(Form aForm)
        {
            fForm = aForm;
            Filter filter = new Filter(aForm);
            SystemHelper.SetProp(aForm.Handle, SingleInstanceGuid, new IntPtr(1));
            Application.AddMessageFilter(filter);
        }
     

    bool ShowMainWindowCallback(IntPtr aWindowHandle, object aUserObject)
    {
        IntPtr handle = PenLink.Utilities.SystemHelper.GetProp(aWindowHandle, SingleInstanceGuid);
        if (handle == new IntPtr(1))
        {
            SystemHelper.PostMessage(aWindowHandle, SingleInstance.WM_SHOME, IntPtr.Zero, IntPtr.Zero);
            return false;
        }
        return true;
    }


    public void ShowForm()
        {
            SystemHelper.ForEachSystemWindow(ShowMainWindowCallback, null);
        }
    }
}
