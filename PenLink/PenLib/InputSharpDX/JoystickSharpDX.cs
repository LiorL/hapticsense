﻿using PenLink.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.InputSharpDX
{

    class JoyStickSharpDX : InputDevice
    {
        public enum JoyStickButtons { Button0, Button1, Button2, Button3, Button4, Button5, Button6, Button7, Button8, Button9, Button10, Button11, Button12, Button13, Button14, Button15 };
        private SharpDX.DirectInput.Joystick fJoystick;
        SharpDX.DirectInput.JoystickState fLastState;
        SharpDX.DirectInput.JoystickState fCurrentState;
        public JoyStickSharpDX(SharpDX.DirectInput.Joystick aJoyStick)
        {
            fCurrentState = new SharpDX.DirectInput.JoystickState();
            fJoystick = aJoyStick;
        }

        override protected Type GetKeyMapObject()
        {
            return new JoyStickButtons().GetType();
        }

        protected override void ChangedButtonState(out List<int> aPressed, out List<int> aReleased)
        {
            aPressed = new List<int>();
            aReleased = new List<int>();

            bool[] lastPressedKeys = fLastState.Buttons;
            bool[] pressedKeys = fCurrentState.Buttons;

            //Update released buttons
            for (int i = 0; i < lastPressedKeys.Length; i++)
            {
                if (lastPressedKeys[i] == true && pressedKeys[i] == false)
                    aReleased.Add(i);
                else
                    if (lastPressedKeys[i] == false && pressedKeys[i] == true)
                        aPressed.Add(i);
            }
        }

        protected override void ChangedAxisState(out List<int> aAxes)
        {
            aAxes = new List<int>();
            aAxes.AddRange(new int[] { fCurrentState.X, fCurrentState.Y, fCurrentState.Z });
        }

        protected override void PerformBind()
        {
            fJoystick.Acquire();
        }

        protected override void PerformUnBind()
        {
            fJoystick.Unacquire();
        }

        protected override void UpdateStart()
        {
            fLastState = fCurrentState;
            fCurrentState = fJoystick.GetCurrentState();
        }

        protected override void UpdateEnd()
        {
            
        }
    }

}
