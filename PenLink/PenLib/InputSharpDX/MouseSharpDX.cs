﻿using PenLink.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.InputSharpDX
{
    public class MouseSharpDX : InputDevice
    {
        public enum MouseButtons { Left, Right, Middle, Button3, Button4, Button5, Button6, Button7 };
        private SharpDX.DirectInput.Mouse fMouse;
        SharpDX.DirectInput.MouseState fLastState;
        SharpDX.DirectInput.MouseState fCurrentState;
        public MouseSharpDX(SharpDX.DirectInput.Mouse aMouse)
        {
            fCurrentState = new SharpDX.DirectInput.MouseState();
            fMouse = aMouse;
        }

        override protected Type GetKeyMapObject()
        {
            return new MouseButtons().GetType();
        }

        protected override void ChangedButtonState(out List<int> aPressed, out List<int> aReleased)
        {
            aPressed = new List<int>();
            aReleased = new List<int>();

            bool[] lastPressedKeys = fLastState.Buttons;
            bool[] pressedKeys = fCurrentState.Buttons;

            //Update released buttons
            for (int i = 0; i < lastPressedKeys.Length; i++)
            {
                if (lastPressedKeys[i] == true && pressedKeys[i] == false)
                    aReleased.Add(i);
                else
                    if (lastPressedKeys[i] == false && pressedKeys[i] == true)
                        aPressed.Add(i);
            }
        }

        protected override void ChangedAxisState(out List<int> aAxes)
        {
            aAxes = new List<int>();
            aAxes.AddRange(new int[] { fCurrentState.X, fCurrentState.Y, fCurrentState.Z });
        }

        protected override void PerformBind()
        {
            fMouse.Acquire();
        }

        protected override void PerformUnBind()
        {
            fMouse.Unacquire();
        }

        protected override void UpdateStart()
        {
            fLastState = fCurrentState;
            fCurrentState = fMouse.GetCurrentState();
        }

        protected override void UpdateEnd()
        {
         
        }
    }

}
