﻿using PenLink.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.InputSharpDX
{
    namespace PenLink.Input
    {
        public class KeyBoardSharpDX : InputDevice
        {
            private SharpDX.DirectInput.Keyboard fKeyboard;
            SharpDX.DirectInput.KeyboardState fLastState;
            SharpDX.DirectInput.KeyboardState fCurrentState;
            public KeyBoardSharpDX(SharpDX.DirectInput.Keyboard aKeyboard)
            {
                fCurrentState = new SharpDX.DirectInput.KeyboardState();
                fKeyboard = aKeyboard;
            }

            override protected Type GetKeyMapObject()
            {
                return new SharpDX.DirectInput.Key().GetType();
            }
            protected override void ChangedButtonState(out List<int> aPressed, out List<int> aReleased)
            {
                aPressed = new List<int>();
                aReleased = new List<int>();

                List<SharpDX.DirectInput.Key> lastPressedKeys = fLastState.PressedKeys;
                List<SharpDX.DirectInput.Key> pressedKeys = fCurrentState.PressedKeys;

                //Update released buttons
                for (int i = 0; i < lastPressedKeys.Count(); i++)
                {
                    SharpDX.DirectInput.Key btn = lastPressedKeys[i];
                    if (!pressedKeys.Contains(btn))
                        aReleased.Add((int)btn);
                }

                //Update pressed buttons
                for (int i = 0; i < pressedKeys.Count(); i++)
                {
                    SharpDX.DirectInput.Key btn = pressedKeys[i];
                    if (!lastPressedKeys.Contains(btn))
                        aPressed.Add((int)btn);
                }
            }

            protected override void PerformBind()
            {
                try
                {
                    fKeyboard.Acquire();
                }
                catch { }
            }

            protected override void PerformUnBind()
            {
                fKeyboard.Unacquire();
            }

            protected override void ChangedAxisState(out List<int> aAxes)
            {
                aAxes = EmptyButtonList;
            }
		#if DEBUG
            long fLastTry;
		#endif
            protected override void UpdateStart()
            {
			#if DEBUG
                if (Environment.TickCount - fLastTry < 1000) //try every second
                    return;

			#endif

                fLastState = fCurrentState;
                try
                {
                    fCurrentState = fKeyboard.GetCurrentState();
                    
                }
                catch (Exception e )
                {
			#if DEBUG
					fLastTry = Environment.TickCount;
			#endif
                    try
                    {
                        fKeyboard.Acquire();
                    }
                    catch { }
                }
            }

            internal void AssignToWindow(IntPtr intPtr)
            {
                fKeyboard.SetCooperativeLevel(intPtr, SharpDX.DirectInput.CooperativeLevel.NonExclusive | SharpDX.DirectInput.CooperativeLevel.Foreground);
            }


            protected override void UpdateEnd()
            {
                
            }
        }

     
    }

}
