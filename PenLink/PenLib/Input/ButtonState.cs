﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Input
{
    public class ButtonState
    {
        private long fButtonDown = long.MaxValue;
        private long fButtonUp = long.MaxValue;
        private bool fIsDown = false;
        private static readonly double TicksPerSecond = System.Diagnostics.Stopwatch.Frequency;
        private static readonly double TicksPerMillisecond = TicksPerSecond / 1000.0;


        public long DownDuration { get { return (long)((CurrentTimeStamp - this.fButtonDown) / TicksPerMillisecond); } }
        public long UpDuration { get { return (long)((CurrentTimeStamp - this.fButtonUp) /TicksPerMillisecond); } } 

        public bool IsDown { get { return this.fIsDown; } }
        public bool IsUp { get { return !this.fIsDown; } }

        public bool IsDownFor(int aMilliseconds)
        {
            return this.fIsDown && (DownDuration > aMilliseconds);
        }

        public bool IsUpFor(int aMilliseconds)
        {
            return !this.fIsDown && (UpDuration > aMilliseconds);
        }

        internal void SetButton(bool aDown)
        {
            if (aDown == true)
                this.SetButtonDown();
            else
                this.SetButtonUP();

        }

        private long CurrentTimeStamp { get { return System.Diagnostics.Stopwatch.GetTimestamp(); } }



        private void SetButtonDown()
        {
            if (!this.fIsDown)
            {
                this.fButtonUp = long.MaxValue;
                this.fButtonDown = CurrentTimeStamp;
                this.fIsDown = true;
            }
        }

        private void SetButtonUP()
        {
            if (this.fIsDown)
            {
                this.fButtonDown = long.MaxValue;
                this.fButtonUp = CurrentTimeStamp;
                this.fIsDown = false;
            }
        }
        
    }

}
