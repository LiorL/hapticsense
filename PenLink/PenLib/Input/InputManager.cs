﻿using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Input
{
    public class InputManager : Common.Singleton<InputManager>
    {
        
        protected List<InputDevice> fInputDevices;
        protected InputManager()
        {
            fInputDevices = new List<InputDevice>();
        }

        public void AddDevice(InputDevice aDevice)
        {
            fInputDevices.Add(aDevice);
        }

        public void Update()
        {
            foreach (InputDevice device in fInputDevices)
                device.Update();
         
        }
    }
}
