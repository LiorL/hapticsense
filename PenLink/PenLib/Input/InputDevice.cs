﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Input
{
    public abstract class InputDevice
    {
        private ButtonState[] fButtonState;
        private bool fIsBound;
        private ButtonBehaviour fButtonBehaviour;

        protected static readonly List<int> EmptyButtonList = new List<int>();
        public enum KeyState { None, Pressed, Released };

        public delegate void ButtonStateChangeDelegate(ButtonEventArgs buttonEventArgs);
        public event ButtonStateChangeDelegate OnButtonStateChanged;

        public delegate void AxisChangeDelegate(List<int> aAxes);
        public event AxisChangeDelegate OnAxisStateChanged;

        public ButtonBehaviour DefaultBehaviour { get { return fButtonBehaviour; } }


        IntPtr fTarget = IntPtr.Zero;


        //private List<Trigger> fTriggers;


        public InputDevice()
        {
            fButtonState = null;
            fIsBound = false;
            fButtonBehaviour = new ButtonBehaviour(this);
            CreateButtonsStates();
        }


        public IntPtr TargetWindow
        {
            get
            {
                return fTarget;
            }
            set
            {
                fTarget = value;
            }
        }
        

        private void CreateButtonsStates()
        {
            Type keymap = GetKeyMapObject();
            Array arr = Enum.GetValues(keymap);
            int min = int.MaxValue, max = int.MinValue;
            foreach (var a in arr)
            {
                int num = (int)a;
                if (num > max)
                    max = num;
                if (num < min)
                    min = num;
            }

            this.fButtonState = new ButtonState[max + 1];

            foreach (var btn in arr)
                this.fButtonState[(int)btn] = new ButtonState();
        }

        public void Bind()
        {
            if (!this.fIsBound)
            {
                this.PerformBind();
                this.fIsBound = true;
            }
        }
        public bool IsBound { get { return fIsBound; } }

        private void UnBind()
        {
            if (this.fIsBound)
            {
                PerformUnBind();
                fIsBound = false;
            }
        }

        public ButtonState[] ButtonStates { get { return fButtonState; } }

        private void SetButton(int aButton, bool aDown)
        {
            ButtonState buttonState = this.fButtonState[aButton];
            if (buttonState != null)
                buttonState.SetButton(aDown);

        }

        public void Update()
        {
            if (!IsBound)
                return;

            UpdateStart();
            UpdateButtons();
            UpdateAxes();
            UpdateEnd();

            fButtonBehaviour.Update();
        
        }

        

        private void UpdateAxes()
        {
            List<int> axes;
            ChangedAxisState(out axes);

            if (axes != EmptyButtonList && axes.Count > 0)
            {
                if (OnAxisStateChanged != null)
                    OnAxisStateChanged(axes);

            }
        }

        private void UpdateButtons()
        {
            List<int> pressed;
            List<int> released;
            ChangedButtonState(out pressed, out released);

            foreach (int btn in pressed)
            {
                SetButton(btn, true);
                if (OnButtonStateChanged != null)
                    OnButtonStateChanged(new ButtonEventArgs()
                    {
                        Button = btn,
                        State = InputDevice.KeyState.Pressed,
                    });

                    //OnButtonStateChanged( btn, KeyState.Pressed);
            }

            foreach (int btn in released)
            {
                SetButton(btn, false);
                if (OnButtonStateChanged != null)
                    OnButtonStateChanged(new ButtonEventArgs()
                    {
                        Button = btn,
                        State = InputDevice.KeyState.Released,
                    });
                    
            }
        }

        protected virtual void ChangedButtonState(out List<int> aPressed, out List<int> aReleased)
        {
            aPressed = aReleased = EmptyButtonList;
        }

        protected virtual void ChangedAxisState(out List<int> aAxes)
        {
            aAxes = EmptyButtonList;
        }

        #region abstract methods

        protected abstract void UpdateStart();
        protected abstract void UpdateEnd();
        protected abstract Type GetKeyMapObject();




        protected abstract void PerformBind();
        protected abstract void PerformUnBind();

        #endregion
    }
}
