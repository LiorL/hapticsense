﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Input
{
    using DictionaryButtonState = Dictionary<int, ButtonBehaviourRecord>;
    
    

    public class ButtonBehaviourRecord
    {
        public int time { get; set; }
        public int repeat { get; set; }
    }

    public class ButtonBehaviour
    {
        const int Default_Repeat_Delay = 150;
        const int Default_Repeat_Rate = 20;
        InputDevice fInputDevice;
        public int RepeatRate { get; set; }
        public int RepeatDelay { get; set; }
        DictionaryButtonState fButtonsTimings;
        HashSet<int> fPressedButtons;
        public delegate void ButtonEventDelegate(ButtonEventArgs aButtonEventArgs);
        public event ButtonEventDelegate OnButtonEvent = delegate { };
        
        public ButtonBehaviour(InputDevice aInputDevice)
        {
            RepeatRate = Default_Repeat_Rate;
            RepeatDelay = Default_Repeat_Delay;
            fInputDevice = aInputDevice;
            fButtonsTimings = new DictionaryButtonState();
            fPressedButtons = new HashSet<int>();
            fInputDevice.OnButtonStateChanged += fInputDevice_OnButtonStateChanged;
        }

        void fInputDevice_OnButtonStateChanged(ButtonEventArgs buttonEventArgs)
        {
            int button = buttonEventArgs.Button;
            if (buttonEventArgs.State == InputDevice.KeyState.Pressed)
            {
                //Add button the the presseed button list
                fPressedButtons.Add(button);
                //fire up immediatly first event
                FireButton(button);
            }
            else
            {
                //remove from pressed key list
                fPressedButtons.Remove(button);
                fButtonsTimings.Remove(button);
                //raise key released event
                buttonEventArgs.Repeats = 1;
                OnButtonEvent(buttonEventArgs);
            }
        }

        public ButtonBehaviourRecord GetButtonRecord(int aButton)
        {
            ButtonBehaviourRecord record;
            if (!fButtonsTimings.TryGetValue(aButton, out record))
                fButtonsTimings.Add(aButton,record = new ButtonBehaviourRecord()
                {
                    time = -1,
                    repeat = 0
                });

            return record;
            

        }

        private void FireButton(int button)
        {
               ButtonBehaviourRecord record = GetButtonRecord(button);
                int totalDown = (int)fInputDevice.ButtonStates[button].DownDuration;

                if (record.time  == -1 || // is first key 
                    ((record.repeat == 1 && (totalDown - record.time > RepeatDelay))) // is second 
                    || (record.repeat > 1 &&   (totalDown - record.time > RepeatRate))  ) // third and above
                {
                    record.time = totalDown;
                    record.repeat++;

                    OnButtonEvent(new ButtonEventArgs()
                    {
                        Button = button,
                        State = InputDevice.KeyState.Pressed,
                        Repeats = record.repeat,
                        Duration = totalDown
                    });
                }
        }

        public void Update()
        {
            foreach (int button in fPressedButtons)
                FireButton(button);
            
        }
    }
}
