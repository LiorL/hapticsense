﻿namespace PenLink.Input
{
    public class ButtonEventArgs
    {
        public int Button { get; set; }
        public InputDevice.KeyState State { get; set; }
        public int Repeats { get; set; }
        public int Duration { get; set; }
    }
}
