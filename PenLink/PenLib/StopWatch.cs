using System;
using System.Runtime.InteropServices;

namespace PenLink
{
    public class StopWatch
    {
        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(
            out long lpPerformanceCount);

        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(
            out long lpFrequency);

        private long fStart=0;
        private long fStop=0;

        // static - so this value used in all instances of 
        private static double sFrequency = getFrequency();

        // Note this is static- called once, before any constructor!
        private static double getFrequency()
        {
            long tempfrequency;
            QueryPerformanceFrequency(out tempfrequency);
            return tempfrequency; // implicit casting to double from long
        }

        public void Start()
        {
            Reset();
            QueryPerformanceCounter(out fStart);
        }

        public void Stop()
        {
            QueryPerformanceCounter(out fStop);
        }

        public void Reset()
        {
            fStop = fStart = 0;
        }
        public bool IsRunning { get { return fStart != 0; } }

        public double ElapsedSeconds
        {
            get
            {
                
                if (fStart == 0) //not running
                    return 0;

                long end;
                if (fStop != 0)
                    end = fStop;
                else
                     QueryPerformanceCounter(out end);
                
                return (double)(end - fStart) / sFrequency;
            }
        }

        public double ElapsedMilliseconds{get{return ElapsedSeconds * 1000;}}
        public double ElapsedMicroSeconds { get { return ElapsedSeconds * 1000000; } }


        public string FormatTimeSpan(TimeSpan timespan)
        {
            return string.Format("{0:00}:{1:00}", timespan.Minutes, timespan.Seconds);
        }

        public string FormatTime()
        {
            return FormatTimeSpan(new TimeSpan(0, 0, 0, 0, (int)Math.Round(ElapsedMilliseconds)));
        }

    }
}
