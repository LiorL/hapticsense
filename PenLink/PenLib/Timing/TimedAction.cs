﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Timing
{
    public delegate void OnTimedExecuteDelegate(TimedActionObject aObj);
    public class TimedAction
    {
        private string fName;
        private double fLastTick = 0;
        private double fResidue = 0;
        
        public double Interval { get; set; }
        public bool Enabled { get; set; }

        public OnTimedExecuteDelegate CallBack;
        
        public TimedAction(string aName)
        {
            fName = aName;
            Enabled = true;
        }


        public void Update(double aTime)
        {
           // The delta from last execution time to 'aTime'.
           double deltaFromLast = aTime - fLastTick;

           // delta time from 'aTime' to when to schedule the next execution.
           double deltaToNext = deltaFromLast + fResidue - Interval;
           
           
           // if deltaToNext is zero - execution is now.
           // if deltaToNext is positive - execution is late
           // on both cases commit execution
           if (deltaToNext >= 0)
           {
               // if execution is late, the residue time will be > 0 and scheduling wil be compensated at the next cycle.
               fResidue = deltaToNext % Interval ;
               fLastTick = aTime;
               CallBack(new TimedActionObject() { Loops = (int)(deltaToNext / Interval) + 1 , DeltaTime = deltaFromLast });
           }
        }
    }
}
