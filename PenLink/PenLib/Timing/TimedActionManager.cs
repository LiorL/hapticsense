﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Timing
{
    public class TimedActionManager : Common.Singleton<TimedActionManager>
    {
        private Dictionary<string, TimedActionBatch> fBatches;

        private TimedActionManager()
        {
            fBatches = new Dictionary<string,TimedActionBatch>();
        }

        public TimedAction CreateAction(string aBatchName,string aActionName,double aItervalInSeconds, OnTimedExecuteDelegate aCallback)
        {
            TimedActionBatch batch =   GetBatch(aBatchName);
            TimedAction action = new TimedAction(aActionName);
            action.CallBack = aCallback;
            action.Interval = aItervalInSeconds;
            batch.AddAction(action);
            return action;
        }


        public void UpdateBatch(string aBatchName)
        {
            GetBatch(aBatchName).Update();
        }

        private TimedActionBatch GetBatch(string aBatchName)
        {
            TimedActionBatch batch = null;
            if (!fBatches.TryGetValue(aBatchName, out batch))
            {
                batch = new TimedActionBatch();
                fBatches.Add(aBatchName, batch);
            }
            
            return batch;
        }

    }
}
