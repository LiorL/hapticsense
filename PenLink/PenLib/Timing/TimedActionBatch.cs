﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Timing
{
    public class TimedActionBatch
    {
        List<TimedAction> fLstActions;
        StopWatch fStopWatch;

        public TimedActionBatch()
        {
            fLstActions = new List<TimedAction>();
            fStopWatch = new StopWatch();
            fStopWatch.Start();
            
        }

        internal void AddAction(TimedAction action)
        {
            fLstActions.Add(action);
        }

        internal void Update()
        {
            foreach (TimedAction action in fLstActions)
                if (action.Enabled)
                    action.Update(fStopWatch.ElapsedSeconds);
        }
    }
}
