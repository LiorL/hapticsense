﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Timing
{
    public class TimeUtility
    {
        public static double TimeFromMiliSeconds(int aValue)
        {
            return (aValue / 1000.0);
        }

        public static double TimeFromMicroSeconds(int aValue)
        {
            return aValue / 1000000000.0;
        }

        public static double TimeFromNanoSeconds(int aValue)
        {
            return aValue / 1000000000000.0;
        }
    }
}
