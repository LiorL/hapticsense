﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PenLink.Timing
{
    public class TimedActionObject
    {
        public int Loops {get;set;}
        public double DeltaTime {get;set;}
    }
}
