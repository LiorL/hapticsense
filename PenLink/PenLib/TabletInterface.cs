﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tablet;
using System.IO;
using System.Runtime.InteropServices;
namespace PenLink
{
    public class TabletInterface
    {
        public struct NormalizedPacket
        {
            public float x;
            public float y;
            public float z;
            public float pressure;
            public int azimuth;
            public int altitude;
            public int status;
        }

        TabletApi.LOGCONTEXT context = new TabletApi.LOGCONTEXT();
        private int oldPacket;
        private int newPacket;
        int lastPacketRead;
        bool Initialized = false;
        
        private TabletApi.tagAxis NormalPressureCap;
        
        //private TabletApi.tagAxis[] OrientationCap = new TabletApi.tagAxis[3];
        private string DeviceName;
        private float HalfX;
        private float HalfY;
        IntPtr hContext = IntPtr.Zero;
        //Logger log=new Logger(@"c:\logger.txt");
        public bool CreateContext(IntPtr aHandle,int aRate)
        {
            try
            {
                if (!Initialized)
                {
                    GetCaps();

                    int result = -1;
                    if (DeviceName != null && DeviceName.ToLower().Contains("wacom"))
                    {

                        result = TabletApi.WTInfo(TabletApi.WTI_DEFCONTEXT, 0, ref context);

                        HalfX = (float)context.lcOutExtX / 2.0f;
                        HalfY = (float)context.lcOutExtY / 2.0f;

                        context.lcName = "FirstContext";
                        context.lcOptions = TabletApi.CXO_MESSAGES | TabletApi.CXO_CSRMESSAGES; //'receive packets as messages
                        context.lcMsgBase = TabletApi.WT_DEFBASE;// 'establish message base number
                        context.lcPktData = TabletApi.PK_STATUS | TabletApi.PK_CURSOR | TabletApi.PK_BUTTONS | TabletApi.PK_X | TabletApi.PK_Y | TabletApi.PK_Z | TabletApi.PK_NORMAL_PRESSURE | TabletApi.PK_TANGENT_PRESSURE | TabletApi.PK_ORIENTATION | TabletApi.PK_ROTATION | TabletApi.PK_TIME | TabletApi.PK_SERIAL_NUMBER;
                        context.lcPktRate = aRate;
                        context.lcPktMode = 0;
                        context.lcMoveMask = TabletApi.PK_CURSOR | TabletApi.PK_STATUS | TabletApi.PK_BUTTONS | TabletApi.PK_X | TabletApi.PK_Y | TabletApi.PK_Z | TabletApi.PK_NORMAL_PRESSURE | TabletApi.PK_TANGENT_PRESSURE | TabletApi.PK_ORIENTATION | TabletApi.PK_ROTATION;
                        context.lcBtnDnMask = //SBN_LCLICK Or PK_STATUS Or SBN_RCLICK Or SBN_LDBLCLICK Or SBN_RDBLCLICK
                        context.lcBtnUpMask = //pCtxDesc.lcBtnDnMask
                        context.lcSysMode = 0; //absolute
                        context.lcOptions = 0;
                        context.lcOutOrgZ = 0;
                        context.lcInOrgZ = 0;
                        Initialized = true;
                    }

                }


                if (Initialized)
                {
                    if (hContext != IntPtr.Zero)
                        TabletApi.WTClose(hContext);

                    hContext = TabletApi.WTOpen(aHandle, ref context, true);

                    //TabletApi.WTEnable(hContext, true);
                    return true;
                }
            }

            catch{}
            
                return false;
            
        }

        private void GetCaps()
        {

            String refStr = new String(' ', 1024);
            int result = 0;
            result = TabletApi.WTInfo(TabletApi.WTI_DEVICES, TabletApi.DVC_NAME, ref refStr);
            int idxEnd = refStr.IndexOf('\0');
            if (idxEnd > -1)
                DeviceName = refStr.Substring(0, idxEnd);
            result = TabletApi.WTInfo(TabletApi.WTI_DEVICES, TabletApi.DVC_NPRESSURE, ref NormalPressureCap);
            TabletApi.tagAxis aba = new TabletApi.tagAxis();
            result = TabletApi.WTInfo(TabletApi.WTI_DEVICES, TabletApi.DVC_Z, ref aba);
            
            //int active = -1;
            //result = TabletApi.WTInfo(TabletApi.WTI_CURSORS, TabletApi.CSR_ACTIVE, ref active);
        }

        public bool PacketInQueue()
        {
            TabletApi.WTQueuePacketsEx(hContext, ref oldPacket, ref newPacket);
            //log.Log(this.oldPacket.ToString()+ " "+this.newPacket.ToString()+" "+this.lastPacketRead.ToString());
            if (oldPacket != newPacket && newPacket != lastPacketRead)
                return true;
                    else return false;
        }

        public NormalizedPacket GetPacket()
        {
            int size = Marshal.SizeOf(typeof(TabletApi.tagPacket));
            int totalpackets = newPacket - oldPacket + 1;
            int totalsize = size * totalpackets;
            IntPtr pointer = Marshal.AllocHGlobal(totalsize);
            long packetsread = 0;
            TabletApi.WTDataGet(hContext, oldPacket, newPacket, totalpackets, pointer, ref packetsread);
            
            TabletApi.tagPacket packet0 = (TabletApi.tagPacket)Marshal.PtrToStructure(pointer, typeof(TabletApi.tagPacket));

            TabletApi.tagPacket packet = packet0;
            //if (packetsread == totalpackets)
            //{
            //    pointer += size ;
            //    TabletApi.tagPacket packet1 = (TabletApi.tagPacket)Marshal.PtrToStructure(pointer, typeof(TabletApi.tagPacket));
            //    packet = packet1;
            //}

            //pointer += size;
            //TabletApi.tagPacket packet1 = (TabletApi.tagPacket)Marshal.PtrToStructure(pointer, typeof(TabletApi.tagPacket));
            Marshal.Release(pointer);
            //TabletApi.WTPacketsGet(hContext, 1, ref packet);
            //TabletApi.Flush(hContext);
            //TabletApi.WTPacket(hContext, newPacket, ref packet);
            lastPacketRead = newPacket;

            return NormalizePacket(packet);
        }

        //public NormalizedPacket GetPacket()
        //{
        //    TabletApi.tagPacket packet= new TabletApi.tagPacket();    
        //    TabletApi.WTPacket(hContext, newPacket, ref packet);
        //    lastPacketRead = newPacket;
        //    return NormalizePacket(packet);
        //}

        private NormalizedPacket NormalizePacket(TabletApi.tagPacket packet)
        {
            NormalizedPacket NP = new NormalizedPacket();
            NP.azimuth = packet.pkAzimuth / 10;
            NP.altitude = packet.pkAltitude / 10;
            NP.pressure = (float)packet.pkNormalPressure / NormalPressureCap.Max;
            //TODO : use Utility.NormalizeCoordinates
            NP.x = (packet.pkX - HalfX) / HalfX;
            NP.y = ((packet.pkY - HalfY) / HalfY);
            //TODO: fix this const when wacom fixes their bug
            NP.z = packet.pkZ / 1024.0f;
            NP.status = packet.pkStatus;
            return NP;
        }

        ~TabletInterface()
        {
            if (hContext != IntPtr.Zero)
                TabletApi.WTClose(hContext);
        }
            
            
        }
    }
