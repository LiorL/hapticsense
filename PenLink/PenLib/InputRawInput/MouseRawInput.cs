﻿using PenLink.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.RawInput;
using SharpDX.Multimedia;
using System.Windows.Forms;

namespace PenLink.InputRawInput
{
    public class MouseRawInput : InputDevice
    {
        HashSet<int> fCurrentState;
        HashSet<int> fLastState;
        int dx;
        int dy;
        public MouseRawInput()
        {
            fCurrentState = new HashSet<int>();
            fLastState = new HashSet<int>();
        }
        protected override void UpdateStart()
        {
            // Do nothing. state is being read by windows messages (WM_INPUT)
        }

        protected override Type GetKeyMapObject()
        {
            return new  InputSharpDX.MouseSharpDX.MouseButtons().GetType();
        }

        protected override void PerformBind()
        {
            DeviceFlags flags = DeviceFlags.None;
            if (TargetWindow != IntPtr.Zero)
            {
                flags |= DeviceFlags.InputSink;
                Device.RegisterDevice(UsagePage.Generic, UsageId.GenericMouse, flags,TargetWindow);
            }
            else
            {
                Device.RegisterDevice(UsagePage.Generic, UsageId.GenericMouse, flags);
            }
            
            Device.MouseInput += Device_MouseInput;
        }

     
        protected override void ChangedButtonState(out List<int> aPressed, out List<int> aReleased)
        {
            aPressed = new List<int>();
            aReleased = new List<int>();

            //Update released buttons
            foreach (int key in fLastState)
            {
                if (!fCurrentState.Contains(key))
                    aReleased.Add((int)key);
            }

            //Update pressed buttons
            foreach (int key in fCurrentState)
            {
                if (!fLastState.Contains(key))
                    aPressed.Add((int)key);
            }
        }

        protected override void ChangedAxisState(out List<int> aAxes)
        {
            aAxes = new List<int>();
            aAxes.AddRange(new int[] { dx, dy});
            
        }

        void Device_MouseInput(object sender, MouseInputEventArgs e)
        {
            
            //update button state
            int buttonsState = (int)e.ButtonFlags;
            for (int i = 0 ; i < 8 ; i++)
            {
                int buttonDownMask  = 1 << (i * 2);
                int buttonUpMask =    1 << (i * 2 + 1);
               
                if ((buttonsState & buttonDownMask) == buttonDownMask)
                {
                    fCurrentState.Add(i);
                }
                if ((buttonsState & buttonUpMask) == buttonUpMask)
                {
                    fCurrentState.Remove(i);
                }
            }

            //upddate axis 
            dx += e.X;
            dy += e.Y;
        }

        protected override void PerformUnBind()
        {
            Device.MouseInput -= Device_MouseInput;
        }

        protected override void UpdateEnd()
        {
            //Save last button state
            fLastState.Clear();
            foreach (int key in fCurrentState)
                fLastState.Add(key);

            //reset movement
            dx = dy = 0;
            
        }
    }

}
