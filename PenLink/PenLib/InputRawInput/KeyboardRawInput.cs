﻿using PenLink.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.RawInput;
using SharpDX.Multimedia;
using System.Windows.Forms;

namespace PenLink.InputRawInput
{
    public class KeyboardRawInput : InputDevice
    {
        HashSet<int> fCurrentState;
        HashSet<int> fLastState;
        public KeyboardRawInput()
        {
            fCurrentState = new HashSet<int>();
            fLastState = new HashSet<int>();
        }
        protected override void UpdateStart()
        {
            // Do nothing. state is being read by windows messages (WM_INPUT)
        }

        protected override Type GetKeyMapObject()
        {
            return new SharpDX.DirectInput.Key().GetType();
        }

        protected override void PerformBind()
        {
            DeviceFlags flags = DeviceFlags.None;
            if (TargetWindow != IntPtr.Zero)
            {
                flags |= DeviceFlags.InputSink;
                Device.RegisterDevice(UsagePage.Generic, UsageId.GenericKeyboard, flags, TargetWindow);
            }
            else
            {
                 Device.RegisterDevice(UsagePage.Generic, UsageId.GenericKeyboard, flags);
            }
            
            
            Device.KeyboardInput += new EventHandler<KeyboardInputEventArgs>(Device_KeyboardInput);
        }

        protected override void ChangedButtonState(out List<int> aPressed, out List<int> aReleased)
        {
            aPressed = new List<int>();
            aReleased = new List<int>();
            
            //Update released buttons
            foreach (int key in fLastState)
            {
                if (!fCurrentState.Contains(key))
                    aReleased.Add((int)key);
            }

            //Update pressed buttons
            foreach (int  key in fCurrentState)
            {
                if (!fLastState.Contains(key))
                    aPressed.Add((int)key);
            }
        }

        private void Device_KeyboardInput(object sender, KeyboardInputEventArgs e)
        {
            //Filter out unsupported keys.
            if (ButtonStates[e.MakeCode] == null)
                return;
            
            if (e.State == SharpDX.RawInput.KeyState.KeyDown)
                fCurrentState.Add(e.MakeCode);
            if (e.State == SharpDX.RawInput.KeyState.KeyUp)
                fCurrentState.Remove(e.MakeCode);
        }

        protected override void PerformUnBind()
        {
            Device.KeyboardInput -= Device_KeyboardInput;
        }

        protected override void UpdateEnd()
        {
            fLastState.Clear();
            foreach (int key in fCurrentState)
                fLastState.Add(key);
        }
    }

}
