﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;

namespace PenLink
{
    public class TrialElement
    {
        public Path2D Path;
        internal DateTime TimeStampStart;
        internal DateTime TimeStampEnd;
        internal List<LendoltResult> SecondaryTaskResults;

      



        public TrialElement()
        {
            Path = new Path2D();
            SecondaryTaskResults = new List<LendoltResult>();
        }

        public string GetSecondaryResult()
        {
            int []results = new int[Enum.GetValues( typeof(LendoltResult)).Length];

            foreach (LendoltResult result in SecondaryTaskResults)
                results[(int)result]++;

            return String.Format("Hit: {0} Miss: {1} False Alram: {2}", results[0], results[1], results[2]);
        }
        
        
    }

    class Trial
    {
        public enum State { None, WaitingForNext, Running, Paused, Stopped};
        State fState;

        Section[] fSections;
        TrialElement[] fTrialElements;
        int fCurrentSection;
        private int fSectionTime = 0;
        

        public Trial(Section[] sections)
        {
            InitializeSections(sections);
            fCurrentSection = -1;
            InitializeTestPaths();
        }

        public  int NextSectionIndex { get { return fCurrentSection + 1; } }

        public State CurrentState { get { return fState;} }

        public Section CurrentSection{get{return fSections[fCurrentSection];}}
        public bool IsLastSection { get {return fCurrentSection == fSections.Count() - 1;}}

        public int SectionTime { get { return fSectionTime; } }
        public bool IsEnded { get { return fState == State.Stopped; } }

        public TrialElement[] TrialElements { get { return (TrialElement[])fTrialElements.Clone();}}

        public Path2D WorkingTestPath  {get {return fTrialElements[fCurrentSection].Path;}}
        
        private void InitializeSections(Section[] srcSections)
        {
            fSections =(Section[]) srcSections.Clone();
        }

        private void InitializeTestPaths()
        {
            fTrialElements = new TrialElement[fSections.Length];
            for (int i = 0; i < fSections.Length; i++)
                fTrialElements[i] = new TrialElement();
        }

        private TrialElement CurrentTrialElement { get { return fTrialElements[fCurrentSection]; } }

        public void AddSecondaryTaskResult(LendoltResult result)
        {
            fTrialElements[fCurrentSection].SecondaryTaskResults.Add(result);
        }

        public void Stop()
        {
            if (fState != State.Stopped)
            {
                CurrentTrialElement.TimeStampEnd = DateTime.Now;
                fState = State.Stopped;
            }
        }
        
        public void PrepareNextSection()
        {
            if (fState != State.WaitingForNext)
            {
                if (!IsLastSection)
                {
                    fSectionTime = (int)(double)(fSections[fCurrentSection + 1].Options["Misc.Time"]);
                    fState = State.WaitingForNext;
                }
                else
                    fState = State.Stopped;

                fCurrentSection++;
            }
        }

        public void Start()
        {
            fState = State.Running;
            CurrentTrialElement.TimeStampStart = DateTime.Now;
        }


      
        internal void Pause()
        {
            if (fState != State.Stopped)
                fState = State.Paused;
        }

        internal void MoveActiveSection(int p)
        {
            if (fState == State.Paused)
            {
                int desiredIndex = fCurrentSection + p;
                if (desiredIndex > -2 && desiredIndex < fSections.Length - 1)
                {
                    fCurrentSection = desiredIndex;
                }
            }
        }
    }
}
