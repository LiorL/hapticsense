﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using PenLink.Timing;
using PenLink.Extensions;
using System.Diagnostics;
using PenLink.Utilities;
using System.IO;
using System.Reflection;

namespace PenLink
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            SystemHelper.SetDllDirectory(Globals.DllsPAth + @"\Native_" + Globals.PlatformString);
            SystemHelper.SetManagedDllDirectory(Globals.DllsPAth);

            if (args.Length > 0)
                new HapticCommandLine(args);
            else
                new HapticMain().Run();

        }
    }
}

 