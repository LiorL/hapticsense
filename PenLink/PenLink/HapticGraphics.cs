﻿using PenLink.Graphics;
using SharpDX;
using SpriteTextRenderer.SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PenLink.Extensions;
using PenLink.Utilities;
using System.IO;
using PenLink.Interfaces;
using PenLib.Graphics;

namespace PenLink
{
   

    public class HapticGraphics : IGraphics
    {
        //Grpahics        
        GraphicRenderer fGraphicRenderer;
        LinesRenderer fLinesRendererMaster;
        LinesRenderer fLinesRendererTest;
        bool fEnableDirect2D;
        TextBlockRenderer fFont;
        Sprite fBackgroundImage;
        Sprite fRectFeedBack;
        Sprite fCursor;
        Sprite fLendoltCursor;
        Sprite fNormalCursor;
        PeripheralVisual fPeripheral;
        
        string fTimeText;
        IHaptic fHaptic;
        IGraphicsHost fGraphicsHost;
        bool fFullScreen;
        string fSectionProperties;
        
        public HapticGraphics(IGraphicsHost aGraphicsHost,IHaptic aHaptic)
        {
            fGraphicsHost = aGraphicsHost;
            fHaptic = aHaptic;
            fTimeText = "00:00";
            fEnableDirect2D = !IsWorkingUnderNsight;

            fGraphicRenderer = new GraphicRenderer(aGraphicsHost, false, true);
            Settings.Data.ItemChanged += Data_ItemChanged;
            Settings.Data.SynthesizeAllChanged();
            
            fGraphicRenderer.CreateResources();

            OnRender += Render;
         
            ResourceLoader.Instance.SetResourcesFolder = Path.Combine(Globals.DefaultMediaPath);
            if (fEnableDirect2D)
                fFont = new TextBlockRenderer(fGraphicRenderer.SpriteBatch, "Arial", SharpDX.DirectWrite.FontWeight.Normal, SharpDX.DirectWrite.FontStyle.Normal, SharpDX.DirectWrite.FontStretch.Normal, 36);

            fLinesRendererMaster = new LinesRenderer(fGraphicRenderer.BackBufferSize.X, fGraphicRenderer.BackBufferSize.Y, fGraphicRenderer);
            fLinesRendererMaster.Clear(fGraphicRenderer);

            fLinesRendererTest = new LinesRenderer(fGraphicRenderer.BackBufferSize.X, fGraphicRenderer.BackBufferSize.Y, fGraphicRenderer);
            fLinesRendererTest.Clear(fGraphicRenderer);

            

            fPeripheral = new PeripheralVisual(fGraphicRenderer);
            fPeripheral.SetColor(LocalData.Current.PeripheralColor.ToSharpDXColor());
            fPeripheral.Visible = false;


            PenLink.Graphics.Resources.Texture texture = ResourceLoader.Instance.GetResource(ResourceType.ID_TEXTURES, "Lendolt", fGraphicRenderer) as PenLink.Graphics.Resources.Texture;
            
            float lendoltSize = fGraphicRenderer.BackBufferSize.X / 120.0f;
            Color lendoltColor = Settings.Data.GetValue<System.Drawing.Color>("Experiment.LendoltColor").ToSharpDXColor();
            fLendoltCursor = new Sprite(texture, new Vector2(lendoltSize, lendoltSize), fGraphicRenderer.ScreenCenter);
            fLendoltCursor.BlendColor = lendoltColor;

            fLendoltCursor.TotalFrames = 8;
            fLendoltCursor.CurrentFrame = 2;

            texture = ResourceLoader.Instance.GetResource(ResourceType.ID_TEXTURES, "cursor", fGraphicRenderer) as PenLink.Graphics.Resources.Texture;
            int normalCursorSize = (int)(fGraphicRenderer.ScreenBounds.Width / 60.0);
            fNormalCursor = new Sprite(texture);
            fNormalCursor.Size = new SharpDX.Vector2(normalCursorSize, normalCursorSize);
            fNormalCursor.Placement = Sprite.CursorPlacement.BottomRight;

            fCursor = fNormalCursor;
            fBackgroundImage = new Sprite(null, Vector2.Zero, fGraphicRenderer.ScreenCenter);

            PenLink.Graphics.Resources.Texture touchfeedBack = ResourceLoader.Instance.GetResource(ResourceType.ID_TEXTURES, "rectfeedback", fGraphicRenderer) as PenLink.Graphics.Resources.Texture;
            fRectFeedBack = new Sprite(touchfeedBack, new Vector2(fGraphicRenderer.BackBufferSize.X, fGraphicRenderer.BackBufferSize.Y) * 0.6f, fGraphicRenderer.ScreenCenter);
            fRectFeedBack.BlendColor = new Color4(0.0f, 0.0f, 0.0f, 0.2f);
            fRectFeedBack.IsVisible = false;


        }

        void Data_ItemChanged(NetSettings.ItemChangedArgs changedArgs)
        {

            string container = changedArgs.Key.ContainerName.ToLower();
            if (container != "graphics")
                return;

            string key = changedArgs.Key.KeyName.ToLower();
            
            if (changedArgs.ChangedMode == NetSettings.ItemChangedMode.Synthesized)
            {
                object val = changedArgs.Val;
                switch (key)
                {

                    case "useflipmode":
                            fGraphicRenderer.AllowFlipMode = (bool)val;
                        break;
                    case "forceworkingonprimaryscreen":
                        if (val != null)
                            fGraphicRenderer.DesiredMonitorIndex = (int)(double)val;
                        break;
                    case "enablevsync":
                        fGraphicRenderer.Vsync = (bool)val == true ? GraphicRenderer.VSyncMode.Normal : GraphicRenderer.VSyncMode.Disabled;
                        break;
                        
                }
            }
            
            
        }

        public GraphicRenderer Renderer { get { return fGraphicRenderer; } }

        private bool IsWorkingUnderNsight
        {
            get
            {
                return ParentProcessUtilities.GetParentProcess().ProcessName.ToLower().Contains("nsight");
            }
        }

      

        private void DrawLinesToTexture()
        {
            fLinesRendererMaster.DrawToSurface(fGraphicRenderer);
            fLinesRendererTest.DrawToSurface(fGraphicRenderer);
        }

        private void Render()
        {
            DrawLinesToTexture();

            fGraphicRenderer.Clear();

            SpriteRenderer fSpriteBatch = fGraphicRenderer.SpriteBatch;


            fBackgroundImage.Draw(fSpriteBatch);

            fRectFeedBack.Draw(fSpriteBatch);

            fSpriteBatch.Flush();
            fPeripheral.Draw(fGraphicRenderer);


            fSpriteBatch.AlphaBlendMode = SpriteTextRenderer.AlphaBlendModeType.PreMultiplied;
            fLinesRendererMaster.DrawSurface(fGraphicRenderer.SpriteBatch);

            if (fHaptic.DrawMode == DrawMode.Testee)
                fLinesRendererTest.DrawSurface(fSpriteBatch);

            fSpriteBatch.AlphaBlendMode = SpriteTextRenderer.AlphaBlendModeType.PostMultiplied;


            if (fEnableDirect2D)
            {
                fFont.DrawString(fTimeText, new Vector2(fGraphicRenderer.BackBufferSize.X - 70, 10), 24.0f, new Color4(1.0f, 1.0f, 1.0f, 1.0f), SpriteTextRenderer.CoordinateType.Absolute);
                if (Settings.Data.GetValue<bool>("Graphics.ShowFPS"))
                    fFont.DrawString(fGraphicRenderer.FrameMonitor.FPS.ToStringFixed2() + " FPS", new Vector2(40, 60), 18.0f, new Color4(1.0f, 1.0f, 1.0f, 1.0f), SpriteTextRenderer.CoordinateType.Absolute);

                if (!String.IsNullOrWhiteSpace(fSectionProperties))
                {
                    Color4 ForeColor = BackGroundColor.GetBrightness() < 130 ? System.Drawing.Color.White.ToSharpDXColor() : System.Drawing.Color.Black.ToSharpDXColor();
                    fFont.DrawString(fSectionProperties, fGraphicRenderer.ScreenCenter, 23f, ForeColor, SpriteTextRenderer.CoordinateType.Absolute);
                }
            }

            fCursor.Draw(fSpriteBatch);
            

            fSpriteBatch.Flush();
        }

        public bool FullScreen
        {
            get { return fFullScreen; }
            set
            {
                if (LocalData.Current.EnableToggleFullScreen)
                    if (fFullScreen != value)
                    {

                        fFullScreen = value;
                        if (LocalData.Current.ExclusiveMode && !fFullScreen)
                            fGraphicRenderer.ToggleFullScreen();
                      
                        bool exclusiveMode = Settings.Data.GetValue<bool>("Graphics.ExclusiveMode");
                        if (exclusiveMode && fFullScreen)
                            fGraphicRenderer.ToggleFullScreen();
                    }
            }
        }
      
   
        public void Start()
        {
            fGraphicRenderer.Render();
        }

        public void SetBackGroundImage(string aImageName)
        {
            if (aImageName == null)
                fBackgroundImage.Texture = null;
            else
            {
                PenLink.Graphics.Resources.Texture texture =
                     ResourceLoader.Instance.GetResource(ResourceType.ID_TEXTURES, aImageName, fGraphicRenderer) as PenLink.Graphics.Resources.Texture;
                if (texture != null)
                {
                    fBackgroundImage.Texture = texture;
                    float h = (float)fGraphicRenderer.ScreenBounds.Height / texture.Height;
                    float v = (float)fGraphicRenderer.ScreenBounds.Width / texture.Width;
                    float ratio = Math.Min(h, v);
                    fBackgroundImage.Size = new Vector2(texture.Width * ratio, texture.Height * ratio);
                }
            }
        }

        public Vector2 CursorPosition 
        { 
            get
            {
                return fCursor.Position;
            }
            set
            {
                fCursor.Position = value;
            }
        }

        public Vector2 NormalizeCoordinates(Vector2 fMousePosition)
        {
            return Compute.NormalizeCoordinates(fGraphicRenderer.BackBufferSize, fMousePosition);
        }

        public Vector2 UnNormalizeCoordinates(Vector2 aPoint)
        {
            return Compute.UnNormalizeCoordinates(fGraphicRenderer.BackBufferSize,aPoint);
        }

        CursorType fActiveCursor = CursorType.Normal;

        public CursorType ActiveCursor 
        {

            get { return fActiveCursor; }
            set
            {
                if (fActiveCursor != value)
                {
                    fActiveCursor = value;
                    switch (fActiveCursor)
                    {
                        case CursorType.None:
                            fCursor.IsVisible = false;
                            break;
                        case CursorType.Normal:
                            fCursor = fNormalCursor;
                            fCursor.IsVisible = true;
                            break;
                        case CursorType.Lendolt:
                            fCursor = fLendoltCursor;
                            fCursor.IsVisible = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        

        public void AddLine(DrawMode aDrawMode, Color4 aLineColor, Vector2 aFrom, Vector2 aTo)
        {
            LinesRenderer linesrenderer = GetLinesRenderer(aDrawMode);


            if (linesrenderer != null)
                linesrenderer.Add(new RoundLineCode.RoundLine(aFrom,aTo,aLineColor, fGraphicRenderer.BackBufferRatio));
            
        }

        private LinesRenderer GetLinesRenderer (DrawMode fDrawMode)
        {
            switch (fDrawMode)
            {
                case DrawMode.Master:
                    return fLinesRendererMaster;
                case DrawMode.Testee:
                    return fLinesRendererTest;
                default:
                    return null;
            }
        }

        public void ClearLines(DrawMode fDrawMode)
        {
            GetLinesRenderer(fDrawMode).Clear(fGraphicRenderer);
        }

        public void AddLines(DrawMode aDrawMode, Color aColor, List<Vector2> aSegments)
        {
            for (int i = 0; i < aSegments.Count; i += 2)
                fLinesRendererMaster.Add(new RoundLineCode.RoundLine(aSegments[i], aSegments[i + 1],
                    Settings.Data.GetValue<System.Drawing.Color>("Experiment.PenMasterColor").ToSharpDXColor(),
                    fGraphicRenderer.BackBufferRatio));
            
        }

        public void Stop()
        {
            fGraphicRenderer.StopRendering();
        }

        public bool RectFeedbackVisible { get; set; }

        public void SetPeripheralOuterRadius(double size)
        {
            fPeripheral.SetOuterRadius(size);
        }


        public Vector2 BackBufferSize
        {
            get
            {
                return fGraphicRenderer.BackBufferSize;
            }

        }


        public Color BackGroundColor
        {
            get
            {
                return fGraphicRenderer.BackGroundColor;
                
            }
            set
            {
                fGraphicRenderer.BackGroundColor = value;
            }
        }


        public Rectangle MonitorBounds
        {
            get { return fGraphicRenderer.ScreenBounds; }
        }


        public event System.Windows.Forms.MethodInvoker OnRender 
        { 
            add 
            {
                fGraphicRenderer.OnRender += value;
            } 
            remove
            {
                fGraphicRenderer.OnRender -= value;
            }
        }

        public string Time
        {
            get
            {
                return fTimeText;
            }
            set
            {
                fTimeText = value;
            }
        }


        public bool IsPeripheralVisibile
        {
            get
            {
                return fPeripheral.Visible;
            }
            set
            {
                fPeripheral.Visible = value;
            }
        }


        public void SetLendoltFrameNumber(int fRandomNumber)
        {
            fLendoltCursor.CurrentFrame = fRandomNumber;
        }


        public void ExecuteGraphicCommand(GraphicCommand aCommand)
        {
            switch (aCommand)
            {
                case GraphicCommand.None:
                    break;
                case GraphicCommand.Start:
                    Start();
                    break;
                case GraphicCommand.ClearMasterLines:
                    ClearLines(DrawMode.Master);
                    break;
                case GraphicCommand.ClearTesteeLines:
                    ClearLines(DrawMode.Testee);
                    break;
                case GraphicCommand.ClearAlllLines:
                    ClearLines(DrawMode.Master);
                    ClearLines(DrawMode.Testee);
                    break;
                case GraphicCommand.Stop:
                    Stop();
                    break;
                default:
                    break;
            }
        }

        public void SetVisible(GraphicElement aElement,bool aVisible)
        {
            switch (aElement)
            {
                case GraphicElement.None:
                    break;
                case GraphicElement.Peripheral:
                    fPeripheral.Visible = aVisible;
                    break;
                case GraphicElement.HeightRect:
                    fRectFeedBack.IsVisible = aVisible;
                    break;
                case GraphicElement.SectionProperties:
                    fSectionProperties = null;
                    break;
                default:
                    break;
            }
        }

        public void SetSectionProperties(string aProperties)
        {
            fSectionProperties = aProperties;
        }
    }
}
