﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PenLink.Utilities
{
    public class FileHelper
    {
        public enum Choice { None, Sucess, Cancelled };

        public static string[] OpenFiles(string aFilter)
        {
            return OpenFiles(aFilter, LocalData.Current.WorkingDirectory);
        }
        public static string[] OpenFiles(string aFilter, string aPath)
        {
            string strResult = null;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = aFilter;
            dialog.InitialDirectory = aPath;
            dialog.Multiselect = true;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                strResult = dialog.FileName;
                LocalData.Current.WorkingDirectory = strResult;
                LocalData.Save();
            }
            return dialog.FileNames;
        }

        public static Choice ChooseFile(bool aOpen, string aFilter, string aPath, out string aFileName)
        {

            aFileName = null;
            FileDialog dialog;
            if (aOpen)
                dialog = new OpenFileDialog();
            else
                dialog = new SaveFileDialog();

            dialog.Filter = aFilter;
            dialog.InitialDirectory = aPath;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                aFileName = dialog.FileName;
                string directory = System.IO.Path.GetDirectoryName(aFileName);
                if (!String.IsNullOrEmpty(directory))
                {
                    LocalData.Current.WorkingDirectory = directory;
                    LocalData.Save();
                }
                return Choice.Sucess;
            }

            return Choice.Cancelled;
        }

        public static Choice ChooseFile(bool aOpen, string aFilter, out string aFileName)
        {
            return ChooseFile(aOpen, aFilter, LocalData.Current.WorkingDirectory, out aFileName);
        }

        internal static string ChooseDirectory(string aPath)
        {
            string strResult = null;
            FolderBrowserDialog folderBrowser;
            folderBrowser = new FolderBrowserDialog();
            if (aPath != null)
                folderBrowser.SelectedPath = aPath;
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                strResult = folderBrowser.SelectedPath;
                LocalData.Current.WorkingDirectory = strResult;
                LocalData.Save();
            }
            return strResult;
        }

        public static bool FileSaveCompressed(string filePath, string fileData)
        {
            bool result = true;
            try
            {
                EnsureFileDirectory(filePath);
                File.WriteAllBytes(filePath, CompressionHelper.Compress(System.Text.Encoding.UTF8.GetBytes(fileData)));
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public static string FileLoadCompressed(string filePath)
        {
            string result = null;
            if (File.Exists(filePath))
                result = System.Text.Encoding.UTF8.GetString(CompressionHelper.Decompress(File.ReadAllBytes(filePath)));

            return result;
        }
        public static string ChooseDirectory()
        {
            return ChooseDirectory(LocalData.Current.WorkingDirectory);
        }
        public static void EnsureFileDirectory(string aFilePath)
        {
            string directory = Path.GetDirectoryName(aFilePath);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }

        public static string GetCurrentFileVersion()
        {
            FileVersionInfo v = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("{0}.{1} Revision {2}",v.FileMajorPart,
                v.FileMinorPart, v.FileBuildPart));
            if (v.FilePrivatePart > 0 )
                sb.Append("." + v.FilePrivatePart);
            sb.Append(String.Format(" {0} {1}",v.LegalCopyright, v.LegalTrademarks));
            return sb.ToString();

        }
    }
}
