﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace PenLink
{
    
    class StatusStripNotification : StatusStrip
    {
        public enum DeviceStatus { Disabled, Off, On };
        private Dictionary<DeviceStatus,Image> dicImgStatus = new Dictionary<DeviceStatus,Image>();

        public StatusStripNotification()
        {
            // Detect design time
            if (System.ComponentModel.LicenseManager.UsageMode != System.ComponentModel.LicenseUsageMode.Designtime)
            {
                dicImgStatus[DeviceStatus.Disabled] = Image.FromFile(Path.Combine(Globals.DefaultMediaPath, "Gray.png"));
                dicImgStatus[DeviceStatus.Off] = Image.FromFile(Path.Combine(Globals.DefaultMediaPath, "Red.png"));
                dicImgStatus[DeviceStatus.On] = Image.FromFile(Path.Combine(Globals.DefaultMediaPath, "Green.png"));
            }
        }

        public void AddElement(string name)
        {
            AddElement(name,DeviceStatus.Disabled);
        }

        public void AddElement(string name,DeviceStatus status)
        {
            ToolStripNotificationItem item = new ToolStripNotificationItem(name, dicImgStatus[status]);
            item.BackColor = System.Drawing.SystemColors.Control;
            item.Name = name;
            Items.Add(item);
        }

        public void RemoveElement(string name)
        {
        }

        public void ChangeOrAddElementStatus(string name, DeviceStatus status)
        {
            if (!Items.ContainsKey(name))
                AddElement(name, status);
            else
            {
                ToolStripNotificationItem item = (Items[name] as ToolStripNotificationItem);
                if (item.statusNotification != status)
                    item.Image = dicImgStatus[status];
            }
                    
        }

    }

    class ToolStripNotificationItem : ToolStripLabel
    {
        public StatusStripNotification.DeviceStatus statusNotification;
        public ToolStripNotificationItem(string name, Image image)
            : base(name, image)
        {
        }
    }
}
