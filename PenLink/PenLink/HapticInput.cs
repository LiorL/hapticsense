﻿using PenLink.Input;
using PenLink.InputSharpDX;
using PenLink.Timing;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using PenLink.Extensions;
using PenLink.Utilities;
using PenLink.Interfaces;
using Serial;
using System.IO;
using PenLink.InputRawInput;
using SharpDX.DirectInput;

namespace PenLink
{
    class HapticInput : IInput
    {
        InputDevice fKeyboard;
        InputDevice fMouse;
        InputDevice fJoystick;
        IHaptic fHaptic;
        IGraphics fGraphics;
        Vector2 fMousePosition;
        TabletInterface fTabletInterface;
        StatusStripNotification.DeviceStatus fJoyStickStatus;
        StatusStripNotification.DeviceStatus fSensorsStatus;
        StatusStripNotification.DeviceStatus fWacomTabletStatus;
        AtlasSerialPoller fSerial;
        AtlasSerialPoller.SensorsData fSensorsData;
        TimedAction fMouseTimedAction;
        double fVirtualMousePressure;
        System.Windows.Forms.Timer fProbeDevicesTimer;
        public HapticInput(IHaptic aHaptic,IGraphics aGraphics)
        {
            fHaptic = aHaptic;
            fGraphics = aGraphics;
            fVirtualMousePressure = 0.5;

            //Raw Keyboard
            fKeyboard = new KeyboardRawInput();
            fKeyboard.TargetWindow = fHaptic.WindowHandle;
            fKeyboard.Bind();
            fKeyboard.DefaultBehaviour.OnButtonEvent += fKeyPressTrigger_OnButtonEvent;
       		Input.InputManager.Instance.AddDevice(fKeyboard);
            //Raw mouse
            fMouse = new MouseRawInput();
            fMouse.TargetWindow = fHaptic.WindowHandle;
            fMouse.OnAxisStateChanged += fMouse_OnAxisStateChanged;
            fMouse.Bind();
            Input.InputManager.Instance.AddDevice(fMouse);


            fTabletInterface = new TabletInterface();
            fMousePosition = new Vector2(fGraphics.BackBufferSize.X / 2, fGraphics.BackBufferSize.Y / 2);

            bool enableSensors = Settings.Data.GetValue<bool>("Input.EnableSensors");

            if (enableSensors)
                fSensorsStatus = StatusStripNotification.DeviceStatus.Off;


            if (Settings.Data.GetValue<bool>("Input.EnableJoyStick"))
                fJoyStickStatus = StatusStripNotification.DeviceStatus.Off;

            if (IsTabletEnabled)
                fWacomTabletStatus = StatusStripNotification.DeviceStatus.Off;


            fProbeDevicesTimer = new System.Windows.Forms.Timer();
            fProbeDevicesTimer.Interval = 3000;
            fProbeDevicesTimer.Tick += new EventHandler(ProbeDevices);
            fProbeDevicesTimer.Enabled = true;


            SearchForDevices(false);
            ProbeDevices(this, null);


            if (IsTabletEnabled)
                TimedActionManager.Instance.CreateAction(Globals.InputTimedActionBatch, "DigitizerAction", 1.0 / Settings.Data.GetValue<double>("Input.DigitizerPollingRate"), PollDigitizer);

            if (enableSensors)
                TimedActionManager.Instance.CreateAction(Globals.InputTimedActionBatch, "SensorsAction", 1.0 / Settings.Data.GetValue<double>("Input.SensorsPollingRate"), PollSensors);


            fMouseTimedAction = TimedActionManager.Instance.CreateAction(Globals.InputTimedActionBatch, "MouseAction", 1.0 / Settings.Data.GetValue<double>("Input.MousePollingRate"), PollMouse);
            fMouseTimedAction.Enabled = Settings.Data.GetValue<bool>("Input.EnableMouseControl");

            Settings.Data.ItemChanged += Settings_ItemChanged;

        }

        private void Settings_ItemChanged(NetSettings.ItemChangedArgs changedArgs)
        {
            string container = changedArgs.Key.ContainerName.ToLower();
            if (container != "input")
                return;

            string key = changedArgs.Key.KeyName.ToLower();
            object val = changedArgs.Val;
             switch (key)
             {
                 case "enablemousecontrol":
                     fMouseTimedAction.Enabled = (bool)val;
                     break;
                 case "mousepollingrate":
                     fMouseTimedAction.Interval = 1.0 / (double)val;
                     break;

             }
        }

        private void InitializeInput()
        {
          
        }


        void fKeyPressTrigger_OnButtonEvent(ButtonEventArgs aButtonEventArgs)
        {
            if (!fHaptic.IsActivated)
                return;

            SharpDX.DirectInput.Key keyCode = (SharpDX.DirectInput.Key)aButtonEventArgs.Button;
            double pressure = 0;
            if (aButtonEventArgs.State == InputDevice.KeyState.Pressed)
            {
                switch (keyCode)
                {
                    case Key.Add:
                        pressure = 1;
                        break;
                    case Key.Subtract:
                        pressure = -1;
                        break;
                    case SharpDX.DirectInput.Key.RightBracket:
                        fHaptic.MoveActiveSection(1);
                        break;
                    case SharpDX.DirectInput.Key.LeftBracket:
                        fHaptic.MoveActiveSection(-1);
                        
                        break;
                    default:
                        break;
                }
            }

            if (pressure != 0)
            {
                fVirtualMousePressure = MathHelper.Clamp(fVirtualMousePressure + 0.03 * pressure, 0.0, 1.0);
            }

            if (aButtonEventArgs.State == InputDevice.KeyState.Pressed && aButtonEventArgs.Repeats == 1)
                OnFirstKeyPress(keyCode);
        }


        void fMouse_OnAxisStateChanged(List<int> aAxes)
        {
            UpdateMousePosition(aAxes[0], aAxes[1]);
            if (Settings.Data.GetValue<bool>("Input.EnableMouseControl"))
                fGraphics.CursorPosition = fMousePosition;

        }

        private void UpdateMousePosition(int X, int Y)
        {
            fMousePosition.X += X;
            fMousePosition.Y += Y;
            fMousePosition.X = MathHelper.Clamp(fMousePosition.X, 0, fGraphics. BackBufferSize.X);
            fMousePosition.Y = MathHelper.Clamp(fMousePosition.Y, 0, fGraphics.BackBufferSize.Y);
        }


        private void OnFirstKeyPress(SharpDX.DirectInput.Key KeyCode)
        {
            switch (KeyCode)
            {
                case Key.D1:
                    //fGraphicRenderer.BackBufferSize = new Vector2(400, 400);
                    break;
                case Key.E:
                    //if (keyboard.ButtonStates[(int)SharpDX.DirectInput.Key.LeftControl].IsDown)
                    //QuickExportTrial();
                    break;
                case Key.Tab:
                    fHaptic.ProcessSystemCommand(SystemCommand.ToggleDebug);
                    break;
                case SharpDX.DirectInput.Key.Space:
                    fHaptic.ProcessSystemCommand(SystemCommand.SetLendoltFeedback);
                    break;
                case SharpDX.DirectInput.Key.Return:
                    fHaptic.ProcessTrialCommand(TrialCommand.Start);
                    break;
                case SharpDX.DirectInput.Key.Grave: //Tilde
                    fHaptic.ProcessSystemCommand(SystemCommand.ToggleSidePanel);
                    break;
                case SharpDX.DirectInput.Key.F2:
                    fHaptic.ProcessSystemCommand(SystemCommand.ShowSettingsDialog);
                    
                    break;
                case SharpDX.DirectInput.Key.F3:
                    fHaptic.ProcessSystemCommand(SystemCommand.ShowExportDialog);
                    break;

                case SharpDX.DirectInput.Key.F4:
                    fHaptic.ProcessSystemCommand(SystemCommand.QuickExport);
                    break;

                case SharpDX.DirectInput.Key.F5:
                    fHaptic.ProcessTrialCommand(TrialCommand.Start);
                    

                    break;
                case SharpDX.DirectInput.Key.F6:

                    bool isControlPressed = fKeyboard.ButtonStates[(int)SharpDX.DirectInput.Key.LeftControl].IsDown
                        || fKeyboard.ButtonStates[(int)SharpDX.DirectInput.Key.RightControl].IsDown;

                    if (isControlPressed)
                        fHaptic.ProcessTrialCommand(TrialCommand.Stop);
                    else
                        fHaptic.ProcessTrialCommand(TrialCommand.Pause);

                    break;

                case SharpDX.DirectInput.Key.F9:
                    //StopDrawing(StopCommand.Cancel);
                    break;
                case SharpDX.DirectInput.Key.F7:
                    fHaptic.SetRecordMode(RecordMode.RecordPressure);
                    break;
                case SharpDX.DirectInput.Key.F8:
                    fHaptic.SetRecordMode(RecordMode.RecordHeight);
                    break;
                case SharpDX.DirectInput.Key.F11:
                    fHaptic.DrawMode = DrawMode.Master;
                    break;
                case SharpDX.DirectInput.Key.F12:
                    fHaptic.DrawMode = DrawMode.Testee;
                    break;
                case SharpDX.DirectInput.Key.F10:
                    break;

                default:
                    break;
            }
        }

      


        #region Pollers
        private void PollDigitizer(TimedActionObject aObj)
        {
            if (fWacomTabletStatus == StatusStripNotification.DeviceStatus.On && fTabletInterface.PacketInQueue())
            {
                TabletInterface.NormalizedPacket packet = fTabletInterface.GetPacket();
                fHaptic.UpdatePacket(packet);
                fGraphics.CursorPosition = fGraphics.UnNormalizeCoordinates(new Vector2(packet.x, packet.y));
            }
        }
        private void PollSensors(TimedActionObject aObj)
        {
            if (fSensorsStatus == StatusStripNotification.DeviceStatus.On && fHaptic.DrawMode == DrawMode.Testee && fSerial.IsOpen)
            {
                if (fSerial.Read())
                    fSensorsData = fSerial.GetAllData();
            }
        }
        void PollMouse(TimedActionObject aObj)
        {
            if (Settings.Data.GetValue<bool>("Input.EnableMouseControl"))
            {
                fGraphics.CursorPosition = fMousePosition;

                bool isLeftButtonDown = fMouse.ButtonStates[(int)MouseSharpDX.MouseButtons.Left].IsDown;
                bool isLeftButtonUp = fMouse.ButtonStates[(int)MouseSharpDX.MouseButtons.Left].IsUp;
                TabletInterface.NormalizedPacket packet = new TabletInterface.NormalizedPacket();
                Vector2 normalizedXY = fGraphics.NormalizeCoordinates(fMousePosition);
                packet.x = normalizedXY.X;
                packet.y = normalizedXY.Y;
                if (isLeftButtonDown == true)
                    packet.pressure = (float)fVirtualMousePressure;
                else if (isLeftButtonUp == true)
                    packet.pressure = 0;

                packet.z = 1.0f - packet.pressure;



                fHaptic.UpdatePacket(packet);
            }
        }
        #endregion


        #region Devices Prob
        
        bool ProbTablet()
        {
            if (CreateTabletContext())
            {
                fWacomTabletStatus = StatusStripNotification.DeviceStatus.On;
                return true;
            }
            else
                return false;
        }

        bool ProbSensors()
        {
            bool result = true;
            if (fSerial == null)
                fSerial = new AtlasSerialPoller("COM3", 115200);
            try
            {
                fSerial.Open();
                fSensorsStatus = StatusStripNotification.DeviceStatus.On;
            }
            catch (IOException exception)
            {
                fSensorsStatus = StatusStripNotification.DeviceStatus.Off;
                result = false;
            }
            fSerial.Close();
            return result;
        }


        bool ProbJoyStick()
        {
            return false;
        }


        void ProbeDevices(object obj, EventArgs args)
        {
            bool result = true;

            if (fJoyStickStatus == StatusStripNotification.DeviceStatus.Off)
                result &= ProbJoyStick();

            if (fSensorsStatus == StatusStripNotification.DeviceStatus.Off)
                result &= ProbSensors();

            if (fWacomTabletStatus == StatusStripNotification.DeviceStatus.Off)
                result &= ProbTablet();

            if (result)
                fProbeDevicesTimer.Enabled = false;

            //TODO: update main form status strip
            //statusStripNotification1.ChangeOrAddElementStatus("Joystick", fJoyStickStatus);
            //statusStripNotification1.ChangeOrAddElementStatus("Sensors", fSensorsStatus);
            //statusStripNotification1.ChangeOrAddElementStatus("Wacom Tablet", fWacomTabletStatus);
        }
        #endregion

        private bool CreateTabletContext()
        {
            return fTabletInterface.CreateContext(fHaptic.WindowHandle, (int)Settings.Data.GetValue<double>("Input.DigitizerPollingRate"));
        }

        private bool IsTabletEnabled
        {
            get
            {
                return Settings.Data.GetValue<bool>("Input.EnableTabletControl");
            }
        }

        #region IInput implementation
        public void SearchForDevices(bool Stop)
        {

            if (Stop)
                fProbeDevicesTimer.Enabled = false;
            else
            {

                if (fJoyStickStatus == StatusStripNotification.DeviceStatus.Off ||
                    fSensorsStatus == StatusStripNotification.DeviceStatus.Off)
                    fProbeDevicesTimer.Enabled = true;
            }
        }
        

        public void EnsureTabletConnected()
        {
            if (IsTabletEnabled)
                CreateTabletContext();
        }

        public void OpenSerialPort()
        {
            if (fSensorsStatus == StatusStripNotification.DeviceStatus.On)
                fSerial.Open();
        }
        public void CloseSerialPort()
        {
            if (fSensorsStatus == StatusStripNotification.DeviceStatus.On)
                fSerial.Close();
        }


        public AtlasSerialPoller.SensorsData LastSensorsData
        {
            get
            {
                return fSensorsData;
            }
        }

        #endregion IInput implementation
    }
}
