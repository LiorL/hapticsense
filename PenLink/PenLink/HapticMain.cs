﻿using System;
using PenLib;

namespace PenLink
{
    internal class HapticMain
    {
        private static SingleInstanceForm singleInstance =
            new SingleInstanceForm("{fe0deaf3-09df-471f-872c-c9bbe74959b6}");

        internal void Run()
        {
            if (singleInstance.IsAlreadyRunning)
                singleInstance.ShowForm();
            else
            {
                HapticApp app = new HapticApp();
                singleInstance.SetForm(app.MainForm);
                app.Run();
                //AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            }
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {

            Exception exception = e.ExceptionObject as Exception;
            string msg = exception.ToString() + exception.ToString() + exception.ToString() + exception.ToString();
            //Common.ErrorForm.Show(exception);
            //MessageBox.Show(msg, "Error");
        }
    }
}
