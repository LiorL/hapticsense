﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Export
{
    public enum ExportCommand { None, ExportTest, ExportResults, ExportMaster};
    public enum ExportResult { None, Failed, Success };

    public enum ExportType { None, Manual, Auto };

    public delegate ExportResult ExportCommandDelegate(ExportCommand aCommand, DataExportContainer aData);

    public class DataExporter
    {

        public ExportResult Export(ExportCommand aCommand, DataExportContainer aData)
        {
            ExportResult result = ExportResult.None; 
            switch (aCommand)
            {
                case ExportCommand.None:
                    break;
                case ExportCommand.ExportTest:
                    result = ExportTest(aData);
                    break;
                case ExportCommand.ExportResults:
                    result = ExportResults(aData);
                    break;
                case ExportCommand.ExportMaster:
                    result = ExportMaster(aData);
                    break;
                default:
                    break;
            }
            return result;
        }

        private string TimeStamp
        {
            get
            {
                const string DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss";             
                return DateTime.Now.ToString(DATE_FORMAT);
            }
        }

        private ExportResult ExportMaster(DataExportContainer aData)
        {
            string path = aData.Path;
            string basename = aData.ExperimentName;
                foreach (Section section in aData.MasterSections)
                    WriteToCsv(section.path,Path.Combine(path, basename + "_" + section.Name + ".csv"));
                    
                return ExportResult.Success;
        }
        private ExportResult ExportTest(DataExportContainer aData)
        {
            string path = aData.Path;
            string basename = aData.TesteeID + "_" + aData.ExperimentName;
            for (int i = 0; i < aData.PathsTest.Length; i++)
            {
                string fileName = Path.Combine(path, basename) + "_trial" + i.ToString() + "_" + TimeStamp + ".csv";
                WriteTestToCsv(aData, i , fileName);
                //File.AppendAllText(fileName, aData.MasterSections[i].GetOptionInKeyValue());

            }
            return ExportResult.Success;
        }
        private ExportResult ExportData(DataExportContainer aData)
        {
                //StringBuilder result = new StringBuilder();
                //result.Append("The following Has Been Exported to " + Settings.Current.WorkingDirectory + "\r\n");

                //if (ExportResults(aData) == ExportResult.Success)
                //    result.Append("Master/Test Difference\n");
                //if (ExportTest(aData))
                //    result.Append("Test Trials Raw Data\n");
                return ExportResult.Success;
        }
        private ExportResult ExportResults(DataExportContainer aData)
        {
            string path = aData.Path;
            StringBuilder stringBuilder = new StringBuilder();

            bool anyExportFlag = false;
            for (int i = 0; i < aData.PathsTest.Length; i++)
            {
                string basename = aData.TesteeID + "_" + aData.ExperimentName + "_trial" + i.ToString();
                Path2D Master = aData.MasterSections[i].path.CloneNonEmpty();
                Path2D Test = aData.PathsTest[i].Path.CloneNonEmpty();
                
                if ((Master.Points.Count > 0) && (Test.Points.Count > 0))
                {
                    anyExportFlag = true;
                    stringBuilder.AppendLine(String.Format("Trial no {0} ", i));
                    DistPath distpath = new DistPath(Master, Test, ComputeMethod.cmAll);
                    stringBuilder.AppendLine(String.Format("\tDistance: Master = {0} Test = {1}", Master.Distance, Test.Distance));
                    stringBuilder.AppendLine(String.Format("\tTime: Master = {0} Test = {1}", Master.Time, Test.Time));
                    stringBuilder.AppendLine(String.Format("\tRMS: {0}", distpath.RMS()));
                    stringBuilder.AppendLine(String.Format("\tAverageDistance: {0}", distpath.aveDist()));
                    stringBuilder.AppendLine(String.Format("\t{0}", aData.PathsTest[i].GetSecondaryResult()));

                    File.WriteAllText(Path.Combine(path, basename) + "_" + TimeStamp + ".txt", stringBuilder.ToString());
                }
            }

            return anyExportFlag ? ExportResult.Success : ExportResult.Failed;
        }




        public void WriteTestToCsv(DataExportContainer aData,int index, string FileName)
        {

            Dictionary<string, object> options = aData.MasterSections[index].Options;
            PointEx[] points = aData.PathsTest[index].Path.CloneNonEmpty().Points.ToArray();
            PathRecordTest[] records = new PathRecordTest[points.Length];

            for (int i = 0; i < points.Length; i++)
            {
                PathRecordTest record = records[i] = new PathRecordTest();
                PathRecord baseRecord = record as PathRecord;
                FillBaseData(points[i], ref baseRecord);
                FillTestData(aData, index, ref record);
            }

            FileHelperEngine engine = new FileHelperEngine(typeof(PathRecordTest));
            engine.HeaderText = "Time,X,Y,Pressure,Azimuth,Altitude,GSR,FSR,ASR,EMG,FileName,SectionName,SectionNumber,"
            +"StartDate,StartTime,SectionTime,ReferencePressure,RecordMode,IsLinear,FeedbackColors,FeedbackPeripheral,FeedbackSounds";

            // To Write Use:  
            engine.WriteFile(FileName, records);
        }

        private void FillTestData(DataExportContainer aData, int index, ref PathRecordTest record)
        {
            Section section = aData.MasterSections[index];


            record.SectionName = section.Name;
            record.SectionNumber = index;
            record.FileName = Path.GetFileName(aData.SourceFileName);





            record.StartDate = aData.PathsTest[index].TimeStampStart.ToString("yyyy_MM_dd");
            record.StartTime = aData.PathsTest[index].TimeStampStart.ToString("HH_mm_ss");

            //Fill section Options
            
            Dictionary<string,object>  options =  section.Options;
            NetSettings.DataProvider dataProvider = SectionManager.Instance.CreateProviderInstance();
            dataProvider.DataBinding = new NetSettings.Data.FlatDataBindings(options);
            
            record.SectionTime = (double)dataProvider.GetValueOrDefault("Misc.Time");
            record.ReferencePressure = (double)dataProvider.GetValueOrDefault("Misc.ReferencePressure");
            record.RecordMode = (string)dataProvider.GetValueOrDefault("Misc.RecordMode");
            record.IsLinear = ((bool)dataProvider.GetValueOrDefault("Misc.ForceLinearFormula")) || !Compute.IsLogarithmicfunctionAvailable((float)record.ReferencePressure);
            record.FeedbackColors = (bool)dataProvider.GetValueOrDefault("Feedback.Colors");
            record.FeedbackPeripheral = (bool)dataProvider.GetValueOrDefault("Feedback.Peripheral");
            record.FeedbackSounds = (bool)dataProvider.GetValueOrDefault("Feedback.EnableSounds");
        }

        private static void FillBaseData(PointEx aPoint, ref PathRecord aRecord)
        {
            aRecord.x = aPoint.Point.X;
            aRecord.y = aPoint.Point.Y;
            aRecord.Azimuth = (int)aPoint.Azimuth;
            aRecord.Altitude = (int)aPoint.Altitude;
            aRecord.pressure = aPoint.pressure;
            aRecord.time = aPoint.time;
            aRecord.GSR = aPoint.sensorsData.GSR;
            aRecord.FSR = aPoint.sensorsData.FSR;
            aRecord.ASR = aPoint.sensorsData.ASR;
            aRecord.EMG = aPoint.sensorsData.EMG;
        }

        public void WriteToCsv(Path2D aPath, string FileName)
        {

            PointEx[] points = aPath.CloneNonEmpty().Points.ToArray();
            PathRecord[] records = new PathRecord[points.Length];

            for (int i = 0; i < points.Length; i++)
            {
             
                PathRecord record =  records[i] = new PathRecord();
                FillBaseData(points[i], ref record);
            }
            FileHelperEngine engine = new FileHelperEngine(typeof(PathRecord));
            engine.HeaderText = "Time,X,Y,Pressure,Azimuth,Altitude,GSR,FSR,ASR,EMG";
            
            engine.WriteFile(FileName, records);
        }



    }
}
