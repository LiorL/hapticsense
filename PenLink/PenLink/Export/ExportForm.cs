﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using PenLink.Utilities;

namespace PenLink.Export
{
    partial class ExportForm : Form
    {
        public ExportCommandDelegate OnExportCommand;
        DataExportContainer fData;
        public ExportForm(DataExportContainer aData)
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
            fData = aData;
            fData.ExportMethod = ExportType.Manual;
        }
        
        //Export Master
        private void btnExportMaster_Click(object sender, EventArgs e)
        {
            if (VerifyExperimentName())
            {
                ExportResult result = OnExportCommand(ExportCommand.ExportMaster, fData);
            }
            
        }

     
        private bool VerifyExperimentName()
        {
            if (textBoxExperimentName.Text != "")
                return true;
            else
            {
                MessageBox.Show("Please Insert Experiment Name");
                return false;
            }
        }

        private bool VerifyID()
        {
            if (textBoxTesteeID.Text != "")
                return true;
            else
            {
                MessageBox.Show("Please Insert TesteeID");
                return false;
            }

        }
        
        private void ExportDataButton_Click(object sender, EventArgs e)
        {
            if ((VerifyExperimentName()) && (VerifyID()))
            {
                ExportResult res = OnExportCommand(ExportCommand.ExportTest, fData);
                res = OnExportCommand(ExportCommand.ExportResults, fData);

                //StringBuilder result = new StringBuilder();
                //result.Append("The following Has Been Exported to " + fData.Path + "\r\n");

                //ExportResult res = OnExportCommand(ExportCommand.ExportResults, null);


                //if (ExportResults(Settings.Current.WorkingDirectory))
                //    result.Append("Master/Test Difference\n");
                //if (ExportTest(Settings.Current.WorkingDirectory))
                //    result.Append("Test Trials Raw Data\n");
            
                //MessageBox.Show(result.ToString());
            }    

        }

        private void ExportForm_Load(object sender, EventArgs e)
        {
            if (fData.PathsTest != null)
                button4.Enabled = true;

            if (fData.MasterSections != null)
                btnExportMaster.Enabled = true;

            textBoxPath.Text = fData.Path;

            textBoxExperimentName.Text = fData.ExperimentName; 
            textBoxTesteeID.Text = fData.TesteeID;
            
                    
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Path = FileHelper.ChooseDirectory();
            if (Path != null)
            {
                textBoxPath.Text = fData.Path =  Path + "\\";
              
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            fData.ExperimentName = textBoxExperimentName.Text;
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            fData.TesteeID = textBoxTesteeID.Text;
            
        }

        private void textBoxPath_TextChanged(object sender, EventArgs e)
        {
            fData.Path = textBoxPath.Text;
            
        }

        private void textBoxPath_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                Close();
        }
    }
}
