﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Export
{
    public class DataExportContainer
    {
        public Section[] MasterSections;
        public TrialElement[] PathsTest;
        public string TesteeID;
        public string ExperimentName;
        public string Path;
        public string SourceFileName;
        public ExportType ExportMethod;

        public bool IsValid 
        { 
            get
            {
                bool result = true;
                result &= MasterSections != null || PathsTest != null;
                result &= !String.IsNullOrEmpty(TesteeID) && !String.IsNullOrEmpty(ExperimentName) && !String.IsNullOrEmpty(Path) && ExportMethod != ExportType.None;
                if (result)
                {
                    result &= System.IO.Directory.Exists(Path);
                }
                return result;
            }
        }
    }
}
