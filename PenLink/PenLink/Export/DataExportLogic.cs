﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Export
{
    public class ExportManager
    {
        DataExporter fExporter;
        public ExportManager()
        {
            fExporter = new DataExporter();
        }

        internal void ShowExportForm(DataExportContainer aData)
        {
            FillInDefaultValues(aData);
            ExportForm exportform = new ExportForm(aData);
            exportform.OnExportCommand = OnExportCommand;
            exportform.ShowDialog();
        }

        private void FillIfNoValid(ref string aDestination, string aSource )
        {
            if (String.IsNullOrEmpty(aDestination) && !String.IsNullOrEmpty(aSource))
                aDestination = aSource;
        }

        private void FillInDefaultValues(DataExportContainer aData)
        {
            if (!aData.IsValid)
            {
                FillIfNoValid(ref aData.TesteeID, LocalData.Current.Experimentid);
                FillIfNoValid(ref aData.ExperimentName, LocalData.Current.ExperimentName);
                FillIfNoValid(ref aData.Path, LocalData.Current.WorkingDirectory);
            }
            
        }

        public ExportResult Export(ExportCommand aCommand, DataExportContainer aData)
        {
            ExportResult result = ExportResult.None;
            FillInDefaultValues(aData);

            if (!aData.IsValid)
                ShowExportForm(aData);
            
            if (aData.IsValid)
                result =  fExporter.Export(aCommand, aData);

            if (result == ExportResult.Success)
                SaveSettings(aData);
            

            return result;
        }

        private void SaveSettings(DataExportContainer aData)
        {
            {
                LocalData.Current.ExperimentName = aData.ExperimentName;
                LocalData.Current.WorkingDirectory = aData.Path;
                LocalData.Current.Experimentid = aData.TesteeID;
                LocalData.Save();
            }
        }

        private ExportResult OnExportCommand(ExportCommand aCommand, DataExportContainer aData)
        {
            return Export(aCommand, aData);
            //return fExporter.Export(aCommand, aData);
        }


    }
}
