﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Export
{


   

    [DelimitedRecord(",")]
    class PathRecord
    {
        public int time;
        [FieldConverter(typeof(floatCustomConveter))]
        public float x;
        [FieldConverter(typeof(floatCustomConveter))]
        public float y;
        [FieldConverter(typeof(floatCustomConveter))]
        public float pressure;
        public int Azimuth;
        public int Altitude;
        public int GSR;
        public int FSR;
        public int ASR;
        public int EMG;
    
    }
    class PathRecordTest : PathRecord
    {
        public string FileName;
        public string SectionName;
        public int SectionNumber;
        public string StartDate;
        public string StartTime;
        public double SectionTime;
        public double ReferencePressure;
        public string RecordMode;
        public bool IsLinear;
        public bool FeedbackColors;
        public bool FeedbackPeripheral;
        public bool FeedbackSounds;
    }

    class floatCustomConveter : FileHelpers.ConverterBase
    {
        public override string FieldToString(object from)
        {
            return from.ToString();
        }
        public override object StringToField(string from)
        {
            return float.Parse(from);
        }
    }

}
