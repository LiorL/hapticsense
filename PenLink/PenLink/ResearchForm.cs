﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FileHelpers;
using System.IO;
using PenLink.Utilities;

namespace PenLink
{
    public partial class ResearchForm : Form
    {
        int TotalCategories;
        class TimePressureRecord
        {
            public int time; //in seconds;
            public double Pressure;
        }


        string[][] filesnames;
        TimePressureRecord[][][] results;
        ImageList smallImageList;

         
        public ResearchForm()
        {
           
            InitializeComponent();
            smallImageList = new ImageList();
            smallImageList.Images.Add(Image.FromFile(Path.Combine(Globals.DefaultMediaPath, "Cancel.png")));
            smallImageList.Images.Add(Image.FromFile(Path.Combine(Globals.DefaultMediaPath, "Check.png")));
            listView1.SmallImageList = smallImageList;

            foreach (ListViewItem item in listView1.Items)
                item.ImageIndex = 0;

            TotalCategories = listView1.Items.Count;
            results = new TimePressureRecord[TotalCategories][][];
            filesnames = new string[TotalCategories][];
        }


        private void LoadCategory(int cat)
        {
            string[] Files = FileHelper.OpenFiles("Comma seperated value (*.csv)|*.csv");
            if (Files != null)
            {
                filesnames[cat] = new string[Files.Length];
                Files.CopyTo(filesnames[cat], 0);
                listView1.Items[cat].ImageIndex = 1;
            }
        
        }

     

    private void button2_Click(object sender, EventArgs e)
    {
        //SaveFileDialog savedialog = new SaveFileDialog();
        //savedialog.AddExtension = true;
        //savedialog.DefaultExt = "csv";
        string strResult;
        
        FileHelper.ChooseFile(false, "Comma seperated value (*.csv)|*.csv",out strResult);

        if (strResult != null)
        {
        FileHelperEngine engine = new FileHelperEngine(typeof(PenLink.Export.PathRecord));
        engine.HeaderText = "Time,X,Y,Pressure,Azimuth,Altitude,GSR,FSR,ASR,EMG";
        engine.Options.IgnoreFirstLines = 1;
        List<string> BadFileName = new List<string>();
        bool errorFlag = false;
        for (int i = 0; i < results.Length; i++)
            {
                results[i] = new TimePressureRecord[filesnames[i].Length][];
                int j = 0;
                foreach (string filename in filesnames[i])
                {
                    try
                    {
                        PenLink.Export.PathRecord[] records = (PenLink.Export.PathRecord[])engine.ReadFile(filename);
                        TimePressureRecord[] tpRecords = NormalizeSeconds(records);
                        results[i][j++] = tpRecords;
                    }
                    catch (Exception ez)
                    {
                        errorFlag = true;
                        BadFileName.Add(filename);
                    }
                }
            }

        if (errorFlag)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string badfile in BadFileName)
                sb.Append(Environment.NewLine + badfile);
            MessageBox.Show("There was a problem parsing the following Files: " + sb.ToString());
            return;
        }
        else
            WriteResultsToCsv(strResult,results);
        }
        
    }

    private void WriteResultsToCsv(string p, TimePressureRecord[][][] results)
    {
        int columns = 0;
        int maxTimeStamp = 0;
        for (int i = 0; i < results.Length ; i++)
        {
            columns+=results[i].Length + 1;

            for (int j = 0; j < results[i].Length; j++)
            {
                TimePressureRecord []currentRecordSet= results[i][j];
                maxTimeStamp = Math.Max(maxTimeStamp,currentRecordSet[currentRecordSet.Length -1 ].time);
                
            }
        }
        string[,] csv = new string[columns,maxTimeStamp +1];

        //Fill Time Stamps
        for (int i = 0; i < maxTimeStamp + 1; i++)
            csv[0,i] = i.ToString();

        int currentColumn = 1;
        //Fill csv

        for (int i_cat = 0; i_cat < results.Length; i_cat++)
        {

            for (int i_rep = 0; i_rep < results[i_cat].Length; i_rep++)
            {
                TimePressureRecord[] currentRecordSet = results[i_cat][i_rep];
                foreach (TimePressureRecord record in currentRecordSet)
                    csv[currentColumn, record.time] = record.Pressure.ToString("F10");
                currentColumn++;
            }
            //Category Space
            currentColumn++;
            
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < maxTimeStamp + 1; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                string toApped = csv[j, i];
                if (toApped == null)
                    toApped = "";
                if (j!=columns) toApped += ',';
                sb.Append(toApped);
            }
            sb.AppendLine();
        }

      File.AppendAllText(p,sb.ToString());
    }



    private TimePressureRecord[] NormalizeSeconds(PenLink.Export.PathRecord[] records)
    {
        int seconds = (int)(Math.Ceiling((double)(records[records.Length - 1].time + 1) / 1000));
        TimePressureRecord[] tpRecords = new TimePressureRecord[seconds];

        int currentSecond = 0;
        int currentRecords = 0;
        double PressureSum = 0;
        foreach (PenLink.Export.PathRecord record in records)
        {
            if (currentSecond == record.time  / 1000)
            {
                currentRecords++;
                PressureSum += record.pressure;
            }
            else
            {
                tpRecords[currentSecond] = new TimePressureRecord();
                tpRecords[currentSecond].time = currentSecond;
                if (currentRecords != 0)
                    tpRecords[currentSecond].Pressure = PressureSum / currentRecords;
                PressureSum = 0;
                currentRecords = 0;
                currentSecond++;
            }
        }
        tpRecords[currentSecond] = new TimePressureRecord();
        tpRecords[currentSecond].time = currentSecond;
        if (currentRecords != 0)
            tpRecords[currentSecond].Pressure = PressureSum / currentRecords;
        return tpRecords;
        
    }

    private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
    {

        int Category = listView1.GetItemAt(e.X, e.Y).Index;
        listView1.LabelEdit = false;
        LoadCategory(Category);
        
    }

    


    }
    }

