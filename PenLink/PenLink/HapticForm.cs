using PenLink.Interfaces;
using PenLink.Utilities;
using System;
using System.Windows.Forms;
namespace PenLink
{

    public partial class HapticForm : FormEx, IHapticUI , PenLib.Graphics.IGraphicsHost
    {
        IHaptic fHaptic;
        ToolStripLabel fStatusLabel;
        System.Drawing.Rectangle fLastBounds;
        NetSettings.DataView fMenuSettings;
        bool fFullScreen;
        public event MethodInvoker OnClose = delegate { };

        public HapticForm(IHaptic aHaptic)
        {
            fHaptic = aHaptic;
            InitializeComponent();
            this.Activated += MainForm_Activated;
            this.Deactivate += MainForm_Deactivate;
            this.Text = String.Format("PenLink v{0} - {1}", FileHelper.GetCurrentFileVersion(), Globals.PlatformString);

            panelOptions.AutoScroll = true;
            FormsHelper.SetSpacesToAllMenus(this);

            fStatusLabel = new ToolStripLabel("Ready");
            fStatusLabel.Dock = DockStyle.Right;

            fStatusLabel.BackColor = System.Drawing.SystemColors.Control;
            statusStrip1.Items.Add(fStatusLabel);

            // set 'move up' and 'move down' buttons to display up/down arrows.
            btnMoveUp.Text = char.ConvertFromUtf32(9650);
            btnMoveDown.Text = char.ConvertFromUtf32(9660);

            Settings.Data.ItemChanged += Data_ItemChanged;
            
            fMenuSettings = new NetSettings.DataView();

        }

        private void Data_ItemChanged(NetSettings.ItemChangedArgs changedArgs)
        {
            string key = changedArgs.Key.KeyNameFullyQualified;
            object value = changedArgs.Val;

            switch (key.ToLower())
            {
                case "general.developermode":
                    AllowClipCursor = (bool)value;
                    break;
            }
        }

        void MainForm_Deactivate(object sender, EventArgs e)
        {
            if (fFullScreen == true)
                MouseCursorVisible = true;

        }
        void MainForm_Activated(object sender, EventArgs e)
        {
            if (fFullScreen == true)
                MouseCursorVisible = false;
        }


        public bool FullScreen
        {
            get
            {
                return fFullScreen;
            }
            set
            {
                if (fFullScreen != value)
                {
                    fFullScreen = value;
                    menuStrip1.Visible = !fFullScreen;
                    statusStripNotification1.Visible = !fFullScreen;
                    pnlCreation.Visible = !fFullScreen;
                    this.WindowState = FormWindowState.Normal;
                    this.splitContainer1.Visible = !fFullScreen;

                    switch (fFullScreen)
                    {
                        case true:
                            fLastBounds = this.Bounds;
                            FormBorderStyle = FormBorderStyle.None;

                            this.Bounds = fHaptic.WorkAreaBounds;
                            break;
                        case false:
                            FormBorderStyle = FormBorderStyle.Sizable;
                            this.Bounds = fLastBounds;
                            break;
                    }

                    MouseCursorVisible = !fFullScreen;
                }
            }
        }


        private int fHighlightedSectionIndex = -1;
        private int HighlightedSectionIndex
        {
            get
            {
                return fHighlightedSectionIndex;
            }
            set
            {
                if (fHighlightedSectionIndex != -1 && fHighlightedSectionIndex < lvSections.Items.Count)
                    lvSections.Items[fHighlightedSectionIndex].BackColor = System.Drawing.Color.White;


                if (value != -1 && value < lvSections.Items.Count)
                {
                    fHighlightedSectionIndex = value;
                    lvSections.Items[fHighlightedSectionIndex].BackColor = System.Drawing.Color.Red;

                }

            }
        }


        private void UpdateSectionTitle()
        {
            int sectionIndex = fHaptic.ActiveSectionIndex;

            string sectionName = sectionIndex != -1 ? (lvSections.Items[sectionIndex].Tag as Section).Name : "N/A";
            string indexName = sectionIndex != -1 ? sectionIndex.ToString() : "";
            lblActivatedSection.Text = String.Format("{2}  [{0}/{1}]", sectionIndex + 1, lvSections.Items.Count, sectionName);
        }



        private int GetSelectedsectionIndex()
        {
            int idx = -1;
            if (lvSections.SelectedIndices.Count > 0)
                idx = lvSections.SelectedIndices[0];
            return idx;
        }


        private void showResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fHaptic.ProcessTrialCommand(TrialCommand.Stop);
        }

        private void exitToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void openToolStripMenuItem_Click(object sender, System.EventArgs e)
        {

            string fileName;
            if (FileHelper.ChooseFile(true, "Penlink Files (*.plf, *.plfx) |*.plf;*.plfx", out fileName) == FileHelper.Choice.Sucess)
            {
                if (fHaptic.ProcessFileCommand(FileCommand.Open,fileName) == false)
                    MessageBox.Show("Could not open the file : " + fileName);

            }
        }


        private void saveToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            string openFile = fHaptic.CurrentOpenFile;

            if (openFile != null && CheckOverwrite(openFile))
                SaveFile(openFile);

        }

        private void SaveFile(string aFileName)
        {
            if (aFileName != null)
            {
                if (fHaptic.ProcessFileCommand(FileCommand.Save,aFileName) == false)
                    MessageBox.Show("Could not save the file: " + aFileName);
            }
        }

        private bool CheckOverwrite(string aFileName)
        {
            bool overwrite = false;
            if (System.IO.File.Exists(aFileName))
                overwrite = MessageBox.Show(this, "Overwrite file: " + System.IO.Path.GetFileName(aFileName), "Save file", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes;
            else
                overwrite = true;

            return overwrite;
        }
        private void saveToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            string strResult;
            FileHelper.ChooseFile(false, "PenLink File (*.plfx)|*.plfx", out strResult);
            SaveFile(strResult);
        }

        private void startToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            fHaptic.ProcessTrialCommand(TrialCommand.Start);
        }
        private void stopToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            fHaptic.ProcessTrialCommand(TrialCommand.Pause);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            OnClose();
        }

        private void clearToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            fHaptic.ProcessSystemCommand(SystemCommand.ClearMasterPath);
        }



        private void button2_Click(object sender, EventArgs e)
        {
            fHaptic.AddSection();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lvSections.SelectedIndices.Count > 0)
                fHaptic.RemoveSection(lvSections.SelectedIndices[0]);

        }

        private void masterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fHaptic.DrawMode = DrawMode.Master;
        }

        private void testToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            fHaptic.DrawMode = DrawMode.Testee;

        }

        private void lvSections_ItemActivate(object sender, EventArgs e)
        {

            if (lvSections.SelectedIndices.Count > 0)
                fHaptic.ActivateSection(lvSections.SelectedIndices[0]);
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (lvSections.SelectedIndices.Count > 0)
            {
                fHaptic.CloneSection(lvSections.SelectedIndices[0]);
            }
        }


        private void lvSections_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            if (e.Label != null)
            {
                (lvSections.Items[e.Item].Tag as Section).Name = e.Label;
                UpdateSectionTitle();
            }
        }



        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fHaptic.ProcessSystemCommand(SystemCommand.ShowExportDialog);
        }

        private void autoExportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fHaptic.ProcessSystemCommand(SystemCommand.QuickExport);
        }


        private void optionsF2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fHaptic.ProcessSystemCommand(SystemCommand.ShowSettingsDialog);
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            int idx = GetSelectedsectionIndex();
            if (idx > 0)
                fHaptic.MoveSection(idx, -1);
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            int idx = GetSelectedsectionIndex();
            if (idx > -1 && idx < lvSections.Items.Count - 1)
                fHaptic.MoveSection(idx, 1);
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName;
            if (FileHelper.ChooseFile(true, "Penlink Files (*.plf, *.plfx) |*.plf;*.plfx", out fileName) == FileHelper.Choice.Sucess)
            {
                if (fHaptic.ProcessFileCommand(FileCommand.ImportFrom,fileName) == false)
                    MessageBox.Show("Could not import section from the file : " + fileName);
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fHaptic.ProcessFileCommand(FileCommand.CreateNew, null);
        }

        public void MoveSelectedSection(int aOffset)
        {
            int desiredIndex = GetSelectedsectionIndex() + aOffset;

            if (desiredIndex == -1)
            {
                lvSections.SelectedIndices.Clear();
            }
            else if (desiredIndex > -1 && desiredIndex < lvSections.Items.Count)
            {
                lvSections.SelectedIndices.Clear();
                lvSections.SelectedIndices.Add(desiredIndex);
                lvSections.EnsureVisible(desiredIndex);
            }
        }
        public void MoveHighlightSection(int aSectionIndex)
        {
            HighlightedSectionIndex = aSectionIndex;

            if (HighlightedSectionIndex > -1 && HighlightedSectionIndex < lvSections.Items.Count)
                lvSections.EnsureVisible(HighlightedSectionIndex);
        }

        public void UpdateDebugInfo(TabletInterface.NormalizedPacket aPacket)
        {
            infoWindow1.SetValue("Altitude", aPacket.altitude.ToString());
            infoWindow1.SetValue("Azimuth", aPacket.azimuth.ToString());
            //infoWindow1.SetValue("Device", packet.);
            infoWindow1.SetValue("Location X", aPacket.x.ToString("F4"));
            infoWindow1.SetValue("Location Y", aPacket.y.ToString("F4"));
            infoWindow1.SetValue("Location Z", aPacket.z.ToString("F4"));
            infoWindow1.SetValue("Absolute Pressure", aPacket.pressure.ToString("F4"));
            infoWindow1.SetValue("Shifted Pressure", Compute.TransformPressure(aPacket.pressure).ToString("F4"));
        }


        public void CreateSectionView(NetSettings.DataProvider aSectionDataProvider)
        {
            fMenuSettings.Create(new NetSettings.DataViewParams()
            {
                container = panelOptions,
                descriptionContainer = controlContainer1,
                dataProvider = aSectionDataProvider
            });
        }

        public void SyncSections(SectionsList aSections)
        {
            fHighlightedSectionIndex = -1;
            Section lastSelected =  lvSections.SelectedIndices.Count > 0 ? lvSections.Items [lvSections.SelectedIndices[0]].Tag as Section : null;

            lvSections.SuspendLayout();
            lvSections.Items.Clear();
            
            ListViewItem selectedViewItem = null;
            //Rebuild section list
            foreach (Section section in aSections)
            {
                ListViewItem item = new ListViewItem(section.Name);
                if (section == lastSelected)
                {
                    selectedViewItem = item;
                    selectedViewItem.Selected = true;
                }


                item.Tag = section;
                lvSections.Items.Add(item);
            }
            
            if (selectedViewItem != null)
                selectedViewItem.EnsureVisible();

            lvSections.ResumeLayout();
            //Update hedaer text
            UpdateSectionTitle();

        }

        public void SetOpenFileTitle(string aFileName)
        {
            saveToolStripMenuItem.Enabled = true;
            lblFileName.Text = System.IO.Path.GetFileName(aFileName);
        }


        public void AddInfoMessage(string message)
        {
            if (!String.IsNullOrWhiteSpace(message))
                fStatusLabel.Text = message;
        }


        public void EditSectionName(int aSectionIndex)
        {
            if (aSectionIndex < lvSections.Items.Count)
                lvSections.Items[aSectionIndex].BeginEdit();
        }

        public void ExecuteCommand(UICommand uICommand)
        {
            switch (uICommand)
            {
                case UICommand.None:
                    break;
                case UICommand.UpdateSectionTitle:
                    UpdateSectionTitle();
                    break;
                case UICommand.RefreshSectionOptionData:
                    fMenuSettings.RefreshViewFromData();
                    break;
                case UICommand.DeselectIndices:
                    lvSections.SelectedIndices.Clear();
                    break;
                case UICommand.ToggleSidePanel:
                    pnlCreation.Visible = !pnlCreation.Visible;
                    break;
                case UICommand.CheckStartMenuItem:
                    FormsHelper.UnCheckMenuTreeByItem(startToolStripMenuItem);
                    (startToolStripMenuItem).Checked = true;
                    break;
                case UICommand.CheckStopMenuItem:
                    FormsHelper.UnCheckMenuTreeByItem(stopToolStripMenuItem);
                    (stopToolStripMenuItem).Checked = true;
                    break;
                case UICommand.ToggleDebugWindow:
                    infoWindow1.Visible = !infoWindow1.Visible;
                    break;
                case UICommand.ShowMainWindow:
                    Show();
                    break;
                case UICommand.CheckMasterMenuItem:
                    FormsHelper.CheckSoleBrother(masterToolStripMenuItem);
                    break;
                case UICommand.CheckTesteeMenuItem:
                    FormsHelper.CheckSoleBrother(testToolStripMenuItem);
                    break;
                default:
                    break;
            }
        }
    }
}
