﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink
{
    class SectionManager : Common.Singleton<SectionManager>
    {
        private string fDataDescriptorFile;
        public string DataDescriptorFile 
        { 
            get
            {
                return fDataDescriptorFile;
            }
        }
        NetSettings.DataProvider fDataProvider;

        public NetSettings.DataProvider SectionDataProvider{  get  {return fDataProvider;}}

        public void InitializeSettingFile(string aPath)
        {
            fDataDescriptorFile = aPath;
            fDataProvider = CreateProviderInstance(); 
        }

        public NetSettings.DataProvider CreateProviderInstance()
        {
            return new NetSettings.DataProvider(NetSettings.ItemTree.FromFile(fDataDescriptorFile));
        }
        public Dictionary<string,object> GetDefaultSectionValues()
        {
            NetSettings.Data.FlatDataBindings result =  new NetSettings.Data.FlatDataBindings(new Dictionary<string, object>());
            fDataProvider.FillDefaultOptionsSet(result);
            return result.Data;
        }
    }
}
