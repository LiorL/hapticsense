﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PenLink.Extensions;
using PenLink.Utilities;

namespace PenLink.Files
{
    public class FileManager
    {
        public static PenLinkFile  LoadPenLinkFile(string aFileName)
        {
            PenLinkFile plf = null; 
            try
			{    
                string extension = Path.GetExtension(aFileName);
				switch (extension)
				{
					case ".plf":
						BinaryPenlinkFile.PenLinkFile oldPlf = BinaryPenlinkFile.PenLinkFile.Load(aFileName);
						ConvertOldPlfToNewPlf(oldPlf,out plf);
						Path.ChangeExtension(aFileName, "plfx");;
					break;
					case ".plfx":
					string JsonText = FileHelper.FileLoadCompressed(aFileName);
					plf = Newtonsoft.Json.JsonConvert.DeserializeObject<PenLinkFile>(JsonText);

					break;
                }
            }
            catch {}
			
            return plf;
            
        }

        private static void ConvertOldPlfToNewPlf(BinaryPenlinkFile.PenLinkFile oldPlf, out PenLinkFile plf)
		{
			plf = new PenLinkFile();
			plf.Sections = new List<Section>(oldPlf.Sections.Count);

			for (int i = 0 ; i < oldPlf.Sections.Count(); i++)
			{

				BinaryPenlinkFile.Section currentSectionOld = oldPlf.Sections[i];
				plf.Sections.Add(new Section(currentSectionOld.Name, currentSectionOld.path.CanvasWidth, currentSectionOld.path.CanvasHeight));
				Section currentSectionNew = plf.Sections[i];
				currentSectionNew.path.Points.AddRange(currentSectionOld.path.ToArray());

                Dictionary<string,object> oldSettings;

                Dictionary<string,object> newSettings = currentSectionNew.Options;


                if (currentSectionOld.Options.TryGetValue("BackGround Settings",out oldSettings ))
                {
                    object o;
                    if (oldSettings.TryGetValue("Color",out o))
                        newSettings["Background.Color"] = o;

                    if (oldSettings.TryGetValue("Image",out o))
                        newSettings["Background.Image"] = o;

                    if (oldSettings.TryGetValue("Rotation", out o))
                        newSettings["Background.ImageRotation"] = o.ToString();
                }

                if (currentSectionOld.Options.TryGetValue("Misc Settings", out oldSettings))
                {
                    object o;
                    if (oldSettings.TryGetValue("Time", out o))
                        newSettings["Misc.Time"] = (double)(int)o;

                    if (oldSettings.TryGetValue("Reference pressure", out o))
                        newSettings["Misc.ReferencePressure"] = Math.Round((float)o,2);
                }

			}
		}

        public static PenLinkFile UnifyPlfFiles(string[] aFileNames)
        {

            
            PenLinkFile[] penlinkFiles = new PenLinkFile[aFileNames.Length];

            int i = 0;
            foreach (string fileName in aFileNames)
                penlinkFiles[i++] = LoadPenLinkFile(fileName);

            PenLinkFile combined = new PenLinkFile();
            combined.Sections = new List<Section>();

            foreach (PenLinkFile file in penlinkFiles)
                combined.Sections.AddRange(file.Sections.DeepClone());
                
            return combined;
        }

        internal static bool SavePenLinkFile(string aFileName, PenLinkFile pf)
        {
            string jsonText = Newtonsoft.Json.JsonConvert.SerializeObject(pf, new Newtonsoft.Json.JsonSerializerSettings()
            {
                Formatting = Newtonsoft.Json.Formatting.Indented,
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
            });
            return FileHelper.FileSaveCompressed(aFileName, jsonText);
        }
    }
}
