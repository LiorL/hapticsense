﻿using System.Drawing;
using System.Runtime.Serialization;
using System;
using System.Collections.Generic;
//using System.Linq;
namespace PenLink
{
    
    [Serializable]
    public class PenLinkFile
    {
        public List<Section> Sections;
    }
}
