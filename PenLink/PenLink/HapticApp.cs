﻿using PenLink.Input;
using PenLink.Interfaces;
using PenLink.Sound;
using PenLink.Timing;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PenLink.Extensions;

namespace PenLink
{


    class HapticApp : IHaptic
    {
        #region Main interfaces

        IHapticUI fHapticUI;
        IGraphics fGraphics;
        IInput fInput;
        IHaptic fHaptic;

    #endregion Main Interfaces

        #region private field memebers

        RecordMode fRecordMode;
        DrawState fDrawState;
        DrawMode fDrawMode;

        StopWatch fDisplayTime;
        double fHeightRectActuationPoint;
        double fHeightRectReleasePoint;
        bool fShouldRestartTrial;
        string fCurrentOpenFile;
        NetSettings.SettingsForm fSettingsForm;
        NetSettings.DataProvider fSectionDataProvider;

        bool fIsPenDown;
        int fActiveSectionIdx;
        Section fCurrentSection;
        LendoltLogic fLendoltLogic;
        PeripheralFeedback fPeripheral;
        PressureNotification fPressureNotification;
        Trial fTrial;
        
        StopWatch fDrawTimer;
        Vector2 fLastPoint;
        
        float fReferencePressure;
        bool fForceLinearFormula;
        bool fIsColorFeedbackEnabled;

        bool fDebugEnabled;
        bool fFullScreen;
        float fLastPressre;
        bool fPressureSound;

        SoundWave fSndCount;
        SoundWave fSndFinal;

        bool fIsSoundAvailable;

        Export.ExportManager fExportManager;

        SectionsList fSectionsList;

        #endregion private field memebers

        #region Initialization
        public HapticApp()
        {
            fHaptic = this;
            ResetVariables();

            fSectionsList = new SectionsList();


            //Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            fHapticUI = new HapticForm(fHaptic);
            
            //fGraphicsHost = fHapticUI as PenLib.Graphics.IGraphicsHost;
            fHapticUI.ExecuteCommand(UICommand.ShowMainWindow);
            fHapticUI.OnClose += OnUIClose;

            fExportManager = new Export.ExportManager();
            fDrawTimer = new StopWatch();
            fDisplayTime = new StopWatch();
            fHeightRectActuationPoint = Settings.Data.GetValue<double>("General.HeightRectActuationPoint");
            fHeightRectReleasePoint = Settings.Data.GetValue<double>("General.HeightRectReleasePoint");

            TimedActionManager.Instance.CreateAction(Globals.DefaultTimedActionBatch, "TimeDisplay", 1.0, OnTimeDisplay);

            DrawMode = DrawMode.Testee;


            CreateSectionSettings();

            fGraphics = new HapticGraphics(fHapticUI as PenLib.Graphics.IGraphicsHost, fHaptic);
            
            fInput = new HapticInput(this, fGraphics);
            fPeripheral = new PeripheralFeedback(fGraphics);
            fPeripheral.SetParams(LocalData.Current.PeripheralInnerSize / 100.0, LocalData.Current.PeripheralOuterSize / 100.0);

            fLendoltLogic = new LendoltLogic(750, 1500, new LendoltLogic.LendoltResultDelegate(OnLendoltResult), fGraphics);

            SoundManager.Instance.Handle = fHapticUI.Handle;
            fIsSoundAvailable = SoundManager.Instance.Device != null;
            if (fIsSoundAvailable)
            {
                fSndCount = new SoundWave(SoundManager.Instance.Device, SoundWave.Note.E, 3, 44100, 16, 500, 0, 2, false);
                fSndFinal = new SoundWave(SoundManager.Instance.Device, SoundWave.Note.E, 4, 44100, 16, 500, 0, 2, false);
                fPressureNotification = new PressureNotification();
            }

            //Create default new penlink file upon start up
            CreateNewFile();
            fGraphics.OnRender += OnRender;
        }

        private void ResetVariables()
        {
            fRecordMode = RecordMode.RecordPressure;
            fIsColorFeedbackEnabled = true;
            fForceLinearFormula = false;
            fIsPenDown = false;
            fDrawMode = DrawMode.Master;
            fDrawState = DrawState.None;
            fLastPoint = Vector2.Zero;
            fDebugEnabled = false;
            fActiveSectionIdx = -1;
            fFullScreen = false;
        }
        #endregion Initialization

        #region Helper properties
        private bool IsTesteeMode { get { return fDrawMode == PenLink.DrawMode.Testee; } }
        private bool IsMasterMode { get { return fDrawMode == PenLink.DrawMode.Master; } }

        private bool IsNullMode { get { return fDrawMode == PenLink.DrawMode.None; } }

        private bool IsReadyForClick { get { return fDrawState == PenLink.DrawState.ReadyForClick; } }
        private bool IsDrawingInProgress { get { return fDrawState == PenLink.DrawState.Drawing; } }

        private bool IsDrawingStarted { get { return fDrawState != PenLink.DrawState.None; } }


        private bool IsRecordPressure { get { return fRecordMode == RecordMode.RecordPressure; } }
        private bool IsRecordHeight { get { return fRecordMode == RecordMode.RecordHeight; } }
        #endregion Helper properties

        #region Unsorted methods

        private void OnLendoltResult(LendoltResult result)
        {
            fTrial.AddSecondaryTaskResult(result);
        }

      

        private void OnUIClose()
        {
            Shutdown();
        }

        private void Shutdown()
        {
            fGraphics.ExecuteGraphicCommand(GraphicCommand.Stop);
            fInput.SearchForDevices(true);
            Application.Exit();
        }

        private void OnRender()
        {
            InputManager.Instance.Update();
            TimedActionManager.Instance.UpdateBatch(Globals.InputTimedActionBatch);
            TimedActionManager.Instance.UpdateBatch(Globals.DefaultTimedActionBatch);
        }
     

        internal void Run()
        {
            fGraphics.ExecuteGraphicCommand(GraphicCommand.Start);
        }

        public void SetRecordMode(RecordMode aRecordMode)
        {
            if (fRecordMode != aRecordMode)
            {
                fRecordMode = aRecordMode;
                string message= String.Empty;
                 switch (fRecordMode)
                {
                case RecordMode.NoRecord:
                    break;
                case RecordMode.RecordPressure:
                         message  = "Record Mode: Pressure";
                    break;
                case RecordMode.RecordHeight:
                          message = "Record Mode: Height";
                    break;
                default:
                    break;
            } 
                fHapticUI.AddInfoMessage(message);
            }
        }

        public DrawState DrawState
        {
            get
            {
                return fDrawState;

            }
            set
            {
                if (fDrawState != value)
                {
                    fDrawState = value;
                    UpdateSectionProperies();
                }
            }
        }




        public DrawMode DrawMode
        {
            get
            {
                return fDrawMode;
            }

            set
            {
                if (fDrawMode != value && !IsDrawingStarted)
                {
                    fDrawMode = value;

                    UICommand command = UICommand.None;
                    string message = String.Empty;
                    switch (fDrawMode)
                    {
                        case PenLink.DrawMode.Master:
                            fHapticUI.MoveHighlightSection(-1);
                            command = UICommand.CheckMasterMenuItem;
                            message = "Draw mode: Master";
                            break;
                        case PenLink.DrawMode.Testee:
                            command = UICommand.CheckTesteeMenuItem;
                            message = "Draw mode: Test";
                            break;


                    }
                    fHapticUI.ExecuteCommand(command);
                    fHapticUI.AddInfoMessage(message);
                    StopDrawing();
                }
            }
        }


        public void MoveActiveSection(int aOffset)
        {
            switch (fDrawMode)
            {
                case DrawMode.None:
                    break;
                case DrawMode.Master:
                    fHapticUI.MoveSelectedSection(aOffset);
                    break;
                case DrawMode.Testee:
                    if (fTrial != null)
                    {
                        fTrial.MoveActiveSection(aOffset);
                        UpdateNextTrialSectionIndex();
                    }
                    
                    break;
                default:
                    break;
            }
        }

        private void UpdateNextTrialSectionIndex()
        {
            if (fTrial != null)
                fHapticUI.MoveHighlightSection(fTrial.NextSectionIndex);
        }

        public void ClearMasterPath()
        {
            fCurrentSection.path.Points.Clear();
            fGraphics.ExecuteGraphicCommand(GraphicCommand.ClearMasterLines);
        }

        public IntPtr WindowHandle {get { return fHapticUI.Handle;}}

        private void LoadImage(string aImageName)
        {
            string imageName = null;
            if (System.IO.File.Exists(aImageName))
                imageName = aImageName;
            fGraphics.SetBackGroundImage(imageName);
        }

        private Path2D GetWorkingPath()
        {
            switch (fDrawMode)
            {
                case DrawMode.Master:
                    return fCurrentSection.path;
                case DrawMode.Testee:
                    return fTrial.WorkingTestPath;
                default:
                    return null;
            }
        }

        bool FullScreen
        {
            get
            {
                return fFullScreen;
            }
            set
            {
                if (fFullScreen != value)
                {
                    fFullScreen = value;
                    fGraphics.FullScreen = fFullScreen;
                    fHapticUI.FullScreen = fFullScreen;
                }
            }
        }


        private void TrialEndSection(bool aTrialEnded, Section aNextSection)
        {
            StopDrawing();
            FullScreen = !aTrialEnded;

            if (!aTrialEnded)
            {
                //Section ended
                ActivateSection(aNextSection);
            }
            else
            {   //Trial Ended
                EndTrial();
            }
        }

        private void OnTimeDisplay(TimedActionObject aObj)
        {
            switch (fDrawMode)
            {
                case DrawMode.Testee:
                    TrialTimeLeft();
                    break;
                case DrawMode.Master:
                    fGraphics.Time = fDisplayTime.FormatTime();

                    break;
            }
        }

        public Form MainForm => fHapticUI as Form;

        #endregion Unsorted methods

        #region Section Methods

        private void UpdateSectionProperies()
        {
            if (IsReadyForClick)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                sb.Append((fActiveSectionIdx + 1).ToString() + " / " + fCurrentSection.Name + "\r\n");


                switch (fRecordMode)
                {
                    case RecordMode.NoRecord:
                        break;
                    case RecordMode.RecordPressure:
                        sb.Append("Real pressure");
                        break;
                    case RecordMode.RecordHeight:
                        sb.Append("Virtual");
                        break;
                    default:
                        break;
                }


                //if ((string)fSectionDataProvider.GetValueOrDefault("Misc.RecordMode") == "Pressure")


                //if ((string)fSectionDataProvider.GetValueOrDefault("Misc.RecordMode") == "Height")


                //(bool)fSectionDataProvider.GetValueOrDefault("Feedback.Colors")
                sb.Append("\r\n");


                //bool occur = false;
                if (fIsColorFeedbackEnabled)
                {
                    sb.Append("Color");
                    //occur = true;
                }
                /*

                if ((bool)fSectionDataProvider.GetValueOrDefault("Feedback.Peripheral"))
                {
                    if (occur)
                        sb.Append(" / ");
                    sb.Append("Peripheral");
                    occur = true;
                }
                if (!occur)
                    sb.Append("None");

                sb.Append("\r\n");

                sb.Append("Pressure: " + fSectionDataProvider.GetValueOrDefault("Misc.ReferencePressure").ToString() + "\r\n");
                */
                fGraphics.SetSectionProperties(sb.ToString());

            }
            else
            {
                fGraphics.SetSectionProperties(null);
            }

        }
        private void CreateSectionSettings()
        {

            SectionManager.Instance.InitializeSettingFile(@"Resources\GuiTemplate.json");
            fSectionDataProvider = SectionManager.Instance.SectionDataProvider;
            fSectionDataProvider.ItemChanged += root_ItemChanged;
            fHapticUI.CreateSectionView(fSectionDataProvider);
        }




        void root_ItemChanged(NetSettings.ItemChangedArgs aArgs)
        {
            string key = aArgs.Key.KeyNameFullyQualified;
            object val = aArgs.Val;

            OnOptionChange(key, val);
        }

        private void ActivateSection(Section section)
        {
            fActiveSectionIdx = fSectionsList.IndexOf(section);
            fCurrentSection = section;
            List<Vector2> segments = fCurrentSection.path.GetNormalizedSegments();

            fGraphics.ExecuteGraphicCommand(GraphicCommand.ClearAlllLines);
            fGraphics.SetBackGroundImage(null);

            SharpDX.Color masterColor = Settings.Data.GetValue<System.Drawing.Color>("Experiment.PenMasterColor").ToSharpDXColor();

            fGraphics.AddLines(DrawMode.Master, masterColor, segments);

            fSectionDataProvider.DataBinding = new NetSettings.Data.FlatDataBindings(section.Options);
            fHapticUI.ExecuteCommand(UICommand.RefreshSectionOptionData);
            fHapticUI.ExecuteCommand(UICommand.UpdateSectionTitle);

            fSectionDataProvider.SynthesizeAllChanged();
        }

        public void ActivateSection(int idx)
        {
            ActivateSection(fSectionsList[idx]);
        }
        public void AddSection()
        {
            AddSection(true);
        }
        public void RemoveSection(int aIndex)
        {
            if (IsSectionIndexInRange(aIndex))
                fSectionsList.RemoveAt(aIndex);
            SectionStructuedChanged();
        }

        void OnOptionChange(string name, object value)
        {
            string sectionSetting = name.ToLower();
            switch (sectionSetting)
            {
                case "background.image":
                    LoadImage((string)value);
                    break;
                case "background.color":
                    fGraphics.BackGroundColor = ((System.Drawing.Color)value).ToSharpDXColor();
                    break;
                case "misc.referencepressure":
                    fReferencePressure = (float)(double)(value);
                    break;
                case "misc.recordmode":
                    string lowerCaseVal = (value as string).ToLower();
                    SetRecordMode(lowerCaseVal == "height" ? RecordMode.RecordHeight : RecordMode.RecordPressure);
                    break;
                case "misc.forcelinearformula":
                    fForceLinearFormula = (bool)value;
                    break;
                case "feedback.colors":
                    fIsColorFeedbackEnabled = (bool)value;
                    break;
                case "feedback.peripheral":
                    fPeripheral.Enabled = (bool)value;
                    break;
                case "feedback.enablesounds":
                    fPressureSound = (bool)value;
                    break;

            }
        }

        public void CloneSection(int aIndex)
        {
            if (IsSectionIndexInRange(aIndex))
            {
                Section sectionToClone = fSectionsList[aIndex];
                AddSection((Section)sectionToClone.DeepClone<Section>());
            }
        }

        bool IsSectionIndexInRange(int aIndex)
        {
            return aIndex >= 0 && aIndex < fSectionsList.Count;
        }

        public void MoveSection(int aIndex, int aOffset)
        {
            int oldIndex = aIndex;
            int newIndex = aIndex + aOffset;
            if (IsSectionIndexInRange(oldIndex) && IsSectionIndexInRange(newIndex))
            {
                Section oldSection = fSectionsList[oldIndex];
                fSectionsList[oldIndex] = fSectionsList[newIndex];
                fSectionsList[newIndex] = oldSection;
            }
            SectionStructuedChanged();
        }

        private void SetNewSectionsList(List<Section> sections)
        {
            fActiveSectionIdx = -1;
            fSectionsList.Clear();
            fSectionsList.AddRange(sections);
            ActivateSection(0);
            SectionStructuedChanged();
        }

        public void AddSection(Section section, bool aBeginEdit = false)
        {
            fSectionsList.Add(section);


            //fHapticUI.AddSection(section, aBeginEdit);
            SectionStructuedChanged();
            if (aBeginEdit)
                fHapticUI.EditSectionName(fSectionsList.IndexOf(section));
        }


        private void AddSection(bool aBeginEdit)
        {
            string DefaultName = "Section" + fSectionsList.Count.ToString();
            Section section = new Section(DefaultName, (int)fGraphics.BackBufferSize.X, (int)fGraphics.BackBufferSize.Y);
            AddSection(section, aBeginEdit);
        }


        private void SectionStructuedChanged()
        {
            fShouldRestartTrial = true;
            fHapticUI.SyncSections(fSectionsList);
        }
#endregion Section Methods

        #region Trial Methods
        private void PauseOrEndTrial()
        {
            FullScreen = false;

            if (fTrial != null)
            {
                fTrial.Pause();
                if (fTrial.IsLastSection)
                    EndTrial();
            }

        }

        private void EndTrial()
        {
            FullScreen = false;

            if (fTrial != null)
                fTrial.Stop();

        }
        private void TrialTimeLeft()
        {
            if (IsDrawingInProgress && fTrial != null)
            {
                TimeSpan time = new TimeSpan(0, 0, fTrial.SectionTime - (int)fDisplayTime.ElapsedSeconds);
                fGraphics.Time = fDisplayTime.FormatTimeSpan(time);

                if (time.TotalSeconds <= 3)
                    switch ((int)time.TotalSeconds)
                    {
                        case 0:
                            fSndFinal.Play();
                            PrepareNextSection();
                            break;
                        default:
                            fSndCount.Play();
                            break;
                    }
            }
        }
        private void CreateNewTrial()
        {
            fTrial = new Trial(fSectionsList.ToArray());
            PrepareNextSection();
            fShouldRestartTrial = false;
        }
        private void PrepareNextSection()
        {
            fTrial.PrepareNextSection();
            StopDrawing();
            FullScreen = !fTrial.IsEnded;

            if (!fTrial.IsEnded)
                ActivateSection(fTrial.CurrentSection);
            else
                EndTrial();


        }
        #endregion Trial Methods

        #region General Methods

        void fSettingsForm_OnSave()
        {
            Settings.Save();
        }

        public bool DebugEnabled
        {
            get { return fDebugEnabled; }
            set
            {
                if (fDebugEnabled != value)
                {
                    fDebugEnabled = value;
                    fHapticUI.ExecuteCommand(UICommand.ToggleDebugWindow);
                }

            }
        }

        public bool IsActivated { get { return fHapticUI.IsActivated; } }

        private void SetCurrentOpenFile(string aFileName)
        {
            fCurrentOpenFile = aFileName;
            fHapticUI.SetOpenFileTitle(aFileName);
        }

        public void UpdatePacket(TabletInterface.NormalizedPacket packet)
        {
            if (fDebugEnabled)
                fHapticUI.UpdateDebugInfo(packet);

            bool sendPacket = false;
            if (IsDrawingStarted)
            {
                switch (fRecordMode)
                {
                    case RecordMode.RecordPressure:
                        //Stylus is UP        
                        if (fLastPressre > 0 && packet.pressure == 0)
                            if (fIsPenDown == true)
                            {
                                GetWorkingPath().AddEmptyPoint();
                                fIsPenDown = false;
                            }

                        if (packet.pressure > 0)
                        {
                            sendPacket = true;
                            packet.pressure = Compute.TransformPressure(packet.pressure);
                        }

                        fLastPressre = packet.pressure;

                        break;
                    case RecordMode.RecordHeight:
                        {
                            float pressure = 1 - packet.z;
                            float normalizedPressure = Compute.TransformHeight(pressure);
                            if (normalizedPressure > 0)
                            {
                                sendPacket = true;
                                packet.pressure = normalizedPressure;

                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            if (sendPacket)
                FollowPath(packet);

        }

        

        private void StartDrawing()
        {
            //When starting to draw unselect all indices so 'Enter' won't activate the selected section.
            fHapticUI.ExecuteCommand(UICommand.DeselectIndices);
            

            if (!IsDrawingStarted)
            {
                //fGraphicRenderer.Vsync = false;
                fInput.SearchForDevices(true);
                fInput.EnsureTabletConnected();

                FullScreen = true;
                if (IsTesteeMode)
                {
                    if (fPressureSound)
                        fPressureNotification.Start();

                    fGraphics.ActiveCursor = CursorType.Lendolt;
                    
                    if (fTrial == null || fTrial.IsEnded || fShouldRestartTrial)
                        CreateNewTrial();
                    else
                        PrepareNextSection();

                    fGraphics.SetVisible(GraphicElement.Peripheral, fPeripheral.Enabled);
                    fInput.OpenSerialPort();
                }

                DrawState = DrawState.ReadyForClick;
                fHapticUI.ExecuteCommand(UICommand.CheckStartMenuItem);
                
                ResetTimer();
            }
        }

        private void ResetTimer()
        {
            fDisplayTime.Reset();
            fGraphics.Time = fDisplayTime.FormatTime();
        }
        public void ProcessTrialCommand(TrialCommand aTrialCommand)
        {
            switch (aTrialCommand)
            {
                case TrialCommand.Start:
                    StartDrawing();
                    break;
                case TrialCommand.Pause:
                    StopDrawing();
                    PauseOrEndTrial();
                    break;
                case TrialCommand.Stop:
                    StopDrawing();
                    EndTrial();
                    break;
            }
        }

        public void StopDrawing()
        {
            if (IsDrawingStarted)
            {
                if (fPressureSound)
                    fPressureNotification.Stop();

                if (IsTesteeMode)
                    UpdateNextTrialSectionIndex();

                fLendoltLogic.Stop();

                fGraphics.SetVisible(GraphicElement.Peripheral, false);

                fGraphics.SetVisible(GraphicElement.HeightRect,false);

                fInput.CloseSerialPort();

                fInput.SearchForDevices(false);
                fDisplayTime.Stop();

                fIsPenDown = false;
                DrawState = DrawState.None;

                fHapticUI.ExecuteCommand(UICommand.CheckStopMenuItem);

            }
        }



        private void FollowPath(TabletInterface.NormalizedPacket packet)
        {
            // if it's not the first touch of the pen with the canvas then draw the line
            if (!ProcessFirstTouch())
                DrawLines(packet);

            AddPointToWorkingPath(packet);

            //Change the pressure audible frequency
            if (fPressureSound)
                fPressureNotification.ChangeRate(packet.pressure * 1000);

            //Calculate and set the frequency of the peripheral ellipse.
            float frequency = Compute.FrequencyFromPressure(packet.pressure, fReferencePressure, fForceLinearFormula);
            fPeripheral.SetPulsesPerSecond(frequency);

            if (IsTesteeMode && IsRecordHeight)
            {
                if (packet.pressure >= fHeightRectActuationPoint)
                    fGraphics.SetVisible(GraphicElement.HeightRect, true);


                if (packet.pressure < fHeightRectReleasePoint)
                    fGraphics.SetVisible(GraphicElement.Peripheral, false);
            }

            fLastPoint.X = packet.x;
            fLastPoint.Y = packet.y;

        }

        private void AddPointToWorkingPath(TabletInterface.NormalizedPacket packet)
        {
            GetWorkingPath().Points.Add(new PointEx(
             new System.Drawing.PointF(packet.x, packet.y),
             (int)fDrawTimer.ElapsedMilliseconds,
             packet.pressure,
             packet.azimuth,
             packet.altitude,
             fInput.LastSensorsData));
        }

        private void DrawLines(TabletInterface.NormalizedPacket packet)
        {
            Color4 lineColor = Settings.Data.GetValue<System.Drawing.Color>("Experiment.PenMasterColor").ToSharpDXColor();

            if (IsTesteeMode && fIsColorFeedbackEnabled)
                lineColor = Compute.GetPressurizedColor(packet.pressure, fReferencePressure, fForceLinearFormula);

            fGraphics.AddLine(fDrawMode, lineColor, fLastPoint, new Vector2(packet.x, packet.y));
        }

        private bool ProcessFirstTouch()
        {
            if (!fIsPenDown)
            {
                //First touch of  the pen with the canvas, we cannot draw a line since two points are needed.
                if (IsReadyForClick)
                {
                    fDrawTimer.Start();
                    DrawState = DrawState.Drawing;

                    if (IsTesteeMode)
                    {
                        fLendoltLogic.start();
                        fTrial.Start();
                        DrawState = DrawState.Drawing;
                    }
                    fDisplayTime.Start();
                }
                fIsPenDown = true;
                return true;

            }
            return false;
        }

        public void ProcessSystemCommand(SystemCommand systemCommand)
        {
            switch (systemCommand)
            {
                case SystemCommand.None:
                    break;
                case SystemCommand.ToggleDebug:
                    DebugEnabled = !DebugEnabled;
                    break;
                case SystemCommand.SetLendoltFeedback:
                    fLendoltLogic.SetLendoltFeedback();
                    break;
                case SystemCommand.ToggleSidePanel:
                    fHapticUI.ExecuteCommand(UICommand.ToggleSidePanel);
                    break;
                case SystemCommand.ShowSettingsDialog:

                    ShowSettingsForm();
                    break;
                case SystemCommand.ShowExportDialog:
                    ShowExportForm();
                    break;
                case SystemCommand.QuickExport:
                    QuickExportTrial();
                    break;
                case SystemCommand.ClearMasterPath:
                    ClearMasterPath();
                    break;
                default:
                    break;
            }
        }

        public bool LoadFile(string fileName)
        {
            PenLinkFile plf = Files.FileManager.LoadPenLinkFile(fileName);
            if (plf != null)
            {
                SetNewSectionsList(plf.Sections);
                SetCurrentOpenFile(fileName);
                return true;
            }

            return false;
        }

        public bool SaveFile(string aFileName)
        {
            PenLinkFile pf = new PenLinkFile();
            pf.Sections = fSectionsList;
            bool result = Files.FileManager.SavePenLinkFile(aFileName, pf);
            if (result == true)
                SetCurrentOpenFile(aFileName);
            return result;

        }





        public System.Drawing.Rectangle WorkAreaBounds
        {
            get { return fGraphics.MonitorBounds.ToSystemRect(); }
        }

        public bool ImportSectionsFromFile(string fileName)
        {
            PenLinkFile plf = Files.FileManager.LoadPenLinkFile(fileName);
            if (plf != null)
            {
                fSectionsList.AddRange(plf.Sections);
                SectionStructuedChanged();
                return true;
            }
            return false;

        }

        public void CreateNewFile()
        {
            fActiveSectionIdx = -1;
            fSectionsList.Clear();
            AddSection(false);
            ActivateSection(0);
            SetCurrentOpenFile("");
        }



        public string CurrentOpenFile
        {
            get { return fCurrentOpenFile; }

        }

        private void ShowSettingsForm()
        {
            if (fSettingsForm == null)
            {
                fSettingsForm = new NetSettings.SettingsForm(Settings.Data);
                fSettingsForm.OnSave += fSettingsForm_OnSave;
                fSettingsForm.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            }

            fSettingsForm.CenterRelativeTo(fHapticUI as Control);
            fSettingsForm.Show();


            fSettingsForm.BringToFront();
        }
        #endregion General methods

        #region Export methods

        private void QuickExportTrial()
        {
            if (fTrial != null)
            {
                Export.DataExportContainer data = new Export.DataExportContainer();
                data.PathsTest = fTrial.TrialElements;
                data.MasterSections = fSectionsList.ToArray();
                data.ExportMethod = Export.ExportType.Auto;

                Export.ExportResult testResult = fExportManager.Export(Export.ExportCommand.ExportTest, data);
                Export.ExportResult resultsResult = fExportManager.Export(Export.ExportCommand.ExportResults, data);

                string message;

                if (testResult != Export.ExportResult.Failed || resultsResult != Export.ExportResult.Failed)
                {
                    message = "The following has been exported:\n\n";
                    if (testResult == Export.ExportResult.Success)
                        message += "Test paths\n";
                    if (resultsResult == Export.ExportResult.Success)
                        message += "results";
                }
                else
                {
                    message = "Export failed";
                }

                MessageBox.Show(message);
            }
        }

        private void ShowExportForm()
        {
            Export.DataExportContainer data = GetExportData();
            fExportManager.ShowExportForm(data);
        }

        private Export.DataExportContainer GetExportData()
        {
            Export.DataExportContainer data = new Export.DataExportContainer();
            if (fTrial != null)
                data.PathsTest = fTrial.TrialElements;
            data.MasterSections = fSectionsList.ToArray();
            data.SourceFileName = fCurrentOpenFile;
            return data;
        }

        #endregion Export methods

        #region IHaptic implementation

        public int ActiveSectionIndex
        {
            get { return fActiveSectionIdx; }
        }


        public bool ProcessFileCommand(FileCommand aFileCommand, string aFileName)
        {
            bool result = false;
            switch (aFileCommand)
            {
                case FileCommand.None:
                    break;
                case FileCommand.Open:
                    result = LoadFile(aFileName);
                    break;
                case FileCommand.Save:
                    result = SaveFile(aFileName);
                    break;
                case FileCommand.ImportFrom:
                    result = ImportSectionsFromFile(aFileName);
                    break;
                case FileCommand.CreateNew:
                    CreateNewFile();
                    result = true;
                    break;
                default:
                    result = false;
                    break;
            }
            return result;
        }

        #endregion IHaptic implementation
    }
}