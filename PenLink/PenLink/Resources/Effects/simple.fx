uniform float width;
uniform float outerRadius; 
uniform float4 blendColor = float4(1.0,1.0,1.0,1.0);
uniform float ratio;
uniform float blur;

struct VS_IN
{
	float3 pos : POSITION;
	float4 color: COLOR;
	float2 uv: TEXTCOORD;

};


struct PS_IN
{
	float4 pos : SV_POSITION;
	float4 color : COLOR;
	float2 uv	 : TEXTCOORD;
};


PS_IN MyVS( VS_IN In )
{
	PS_IN Out;
	Out.pos = float4(In.pos,1.0);
	Out.color = In.color;
	Out.uv = In.uv;
	return Out;
}

float4 CalulateCircle(PS_IN input)
{
	float lineWidth = width * 2;
	float2 can = (input.uv - 0.5) * 2;
	float4 result = input.color * blendColor;
	float X = can.x;
	float Y = can.y;
	
	//Fix ratio distorion
	float diff = ratio * lineWidth - lineWidth;
	lineWidth += diff * abs(Y);

	float radius = sqrt((X * X) + (Y * Y));
	float halfWidth = lineWidth / 2.0;

	float blurWidth = blur * halfWidth;

	float innerRadius = outerRadius - lineWidth;
	float centerRadius = innerRadius  + halfWidth;
	float distToCenter = abs(centerRadius - radius);

	if (distToCenter > halfWidth)
		discard;
	else
		if (distToCenter > halfWidth - blurWidth)
			result.a = (halfWidth - distToCenter) / blurWidth;

	return result;
}

float4 MyPSStandard( PS_IN input) : SV_TARGET
{
	return CalulateCircle(input);
}


DepthStencilState dssEnableDepth
{
  DepthEnable = false;
};


RasterizerState rsStandard
{
		CullMode =  None;
};



BlendState bsStandard
{

		BlendEnable[0] = true;
		RenderTargetWriteMask[0] = 0xf;
		SrcBlend[0] = src_Alpha;
		DestBlend[0] = Inv_Src_Alpha;
		BlendOp[0] = Add;
	    BlendOpAlpha[0] = Add;
	    SrcBlendAlpha[0] = src_Alpha;
	    DestBlendAlpha[0] = Inv_Src_Alpha;
};




technique11 Standard
{
	pass P0
	{
		SetBlendState(bsStandard,float4( 1.0f, 1.0f, 1.0f, 1.0f ), 0xFFFFFFFF );
		SetRasterizerState(rsStandard);
		//SetDepthStencilState(dssEnableDepth, 0);	
		SetGeometryShader( 0 );
		SetVertexShader	 (	CompileShader(vs_4_0 ,MyVS()) );
		SetPixelShader	 (	CompileShader(ps_4_0 ,MyPSStandard()) );
		
	}
}



