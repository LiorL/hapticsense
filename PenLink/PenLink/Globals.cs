﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Common;

namespace PenLink
{
    public class SectionsList : List<Section> { }
    public enum SystemCommand { 
        None, 
        ToggleDebug, 
        SetLendoltFeedback, 
        ToggleSidePanel, 
        ShowSettingsDialog, 
        ShowExportDialog,QuickExport,
        ClearMasterPath
    }

    public enum FileCommand
    {
        None,
        Open,
        Save,
        CreateNew,
        ImportFrom
    }

    public enum DrawMode { None, Master, Testee };
    public enum ComputeMethod { cmAll, cmTime, cmAppTime };
    public enum DrawState { None, ReadyForClick, Drawing }
    public enum RecordMode { NoRecord, RecordPressure, RecordHeight }
    public enum CursorType { None, Normal, Lendolt }

    public enum TrialCommand { None, Start,Pause, Stop};

    public class Globals
    {
        public readonly static string PlatformString = Environment.Is64BitProcess ? "x64" : "x86";
        public const string DefaultTimedActionBatch = "Default";
        public const string InputTimedActionBatch = "Input";
        public readonly static string RootPath = Path.GetDirectoryName(Application.ExecutablePath);
        public readonly static string DefaultMediaPath = Path.Combine(RootPath, "Resources");
        public readonly static string DllsPAth = Path.Combine(RootPath, "Dlls");

        public const string DefaultSettingsFile = "localData.json";
    }

    public class SerializeFileNameClass : SerializeFileName
    {
        public override string FileName
        {
            get { return Globals.DefaultSettingsFile; }
        }
    }

  
}