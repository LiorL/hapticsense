﻿using PenLink.Sound;
using PenLink.Timing;

namespace PenLink
{
    class PressureNotification
    {
        SoundWave wave;
        readonly float ZeroPressureRate = SoundWave.GetFrequency(SoundWave.Note.C, 5);
        readonly int WaveLength = 90;
        readonly int SilenceFactor = 8;

        float fCurrentRate;
        float fRequestedRate;
        public PressureNotification()
        {
            TimedActionManager.Instance.CreateAction(Globals.DefaultTimedActionBatch, "sounds",TimeUtility.TimeFromMiliSeconds(WaveLength), OnSoundChange);
            if (SoundManager.Instance.Device != null)
                wave = new SoundWave(SoundManager.Instance.Device, ZeroPressureRate, 22500, 16, WaveLength, WaveLength * SilenceFactor, 1, false);
        }


        private void SetRate(float aRate)
        {
            fCurrentRate = fRequestedRate = aRate;
            wave.SetFrequency(fCurrentRate);
        }

        private void OnSoundChange(TimedActionObject obj)
        {
            if (fRequestedRate != fCurrentRate)
                SetRate(fRequestedRate);
        }

        public void Start()
        {
            SetRate(ZeroPressureRate);
            wave.PlayLooped();
        }

        public void Stop()
        {
            wave.Stop();
        }

        public void ChangeRate(float aRate)
        {
            fRequestedRate = ZeroPressureRate + aRate;
        }
    }
}
