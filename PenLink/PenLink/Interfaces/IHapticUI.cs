﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Interfaces
{
    public enum UICommand 
    {
        None, 
        UpdateSectionTitle, 
        RefreshSectionOptionData, 
        DeselectIndices, 
        ToggleSidePanel, 
        CheckStartMenuItem,
        CheckStopMenuItem,
        CheckMasterMenuItem,
        CheckTesteeMenuItem,
        ToggleDebugWindow,
        ShowMainWindow
    }

    public interface IHapticUI
    {
        void MoveSelectedSection(int aOffset);

        void MoveHighlightSection(int aSectionIndex);

        void UpdateDebugInfo(TabletInterface.NormalizedPacket packet);

        event System.Windows.Forms.MethodInvoker OnClose;

        void CreateSectionView(NetSettings.DataProvider aSectionDataProvider);

        bool IsActivated { get; }

        void SyncSections(SectionsList aSections);

        void SetOpenFileTitle(string aFileName);

        bool FullScreen { get; set; }

        void AddInfoMessage(string aMessage);

        IntPtr Handle { get; }

        void EditSectionName(int p);

        void ExecuteCommand(UICommand uICommand);
    }
}
