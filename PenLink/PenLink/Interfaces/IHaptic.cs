﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink
{

    public interface IHaptic
    {
        int  ActiveSectionIndex { get; }

        DrawMode DrawMode { get; set; }

        bool IsActivated { get;}

        void MoveActiveSection(int aIndex);

        IntPtr WindowHandle { get; }

        void UpdatePacket(TabletInterface.NormalizedPacket packet);

        void ProcessTrialCommand(TrialCommand stopCommand);

        void ProcessSystemCommand(SystemCommand systemCommand);

        bool ProcessFileCommand(FileCommand aFileCommand , string aFileName);

        void SetRecordMode(RecordMode recordMode);

        void MoveSection(int aIndex, int aOffset);

        void RemoveSection(int aIndex);

        void AddSection();

        void CloneSection(int aTemplateSectionIndex);

        System.Drawing.Rectangle WorkAreaBounds { get;  }


        void ActivateSection(int aIndex);

        string CurrentOpenFile { get;}
    }
}
