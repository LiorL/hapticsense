﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Interfaces
{
    public enum GraphicCommand {  None, Start ,Stop,ClearMasterLines,ClearTesteeLines,ClearAlllLines };
    public enum GraphicElement { None, Peripheral, HeightRect, SectionProperties }

    public interface IGraphics
    {
        void SetSectionProperties(string aProperties);

        void SetVisible(GraphicElement aElement,bool aVisible);

        void SetPeripheralOuterRadius(double size);

        CursorType ActiveCursor { get; set; }

        void AddLines(DrawMode drawMode, Color masterColor, List<Vector2> segments);

        void AddLine(DrawMode fDrawMode, Color4 lineColor, Vector2 vector21, Vector2 vector22);

        Vector2 CursorPosition { get; set; }

        bool FullScreen { get; set; }

        Vector2 NormalizeCoordinates(Vector2 fMousePosition);


        Vector2 BackBufferSize { get; }

        Color BackGroundColor { get; set; }

        Rectangle MonitorBounds { get; }

        void SetBackGroundImage(string aImageName);

        Vector2 UnNormalizeCoordinates(Vector2 vector2);

        void ExecuteGraphicCommand(GraphicCommand aCommand);

        event System.Windows.Forms.MethodInvoker OnRender;

        string Time { get; set; }

        void SetLendoltFrameNumber(int fRandomNumber);
    }
}
