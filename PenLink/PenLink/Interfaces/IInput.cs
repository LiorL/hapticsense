﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink.Interfaces
{
    public interface IInput
    {

        void SearchForDevices(bool p);

        void EnsureTabletConnected();

        void OpenSerialPort();

        void CloseSerialPort();

        Serial.AtlasSerialPoller.SensorsData LastSensorsData { get; }
    }
}
