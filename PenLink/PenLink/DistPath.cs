﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace PenLink
{
    /**
 * this class contains several semi-norms between two path2D.
 * To DO:
 * 1. add suport in area!
 * 2. add suport in time
 * 3. distance relation
 * 4. JDialog 
 * @author boaz
 * @Translation by (The Nicker)™
 */

public class DistPath {
	Path2D _master;
	Path2D _test;
	int[] _m2t, _t2m;
	
	public DistPath(Path2D m, Path2D t, ComputeMethod aComputeMethod){
		_master = m; _test = t;
		init(aComputeMethod);
	}
	public String toString() {
		String ans ="Master: "+_master.ToString()+'\n';
		ans += "Test: "+_test.ToString()+'\n';
        ans += "Time: " + (_master.Time - _test.Time) + "   Dist: " + (_master.Distance - _test.Distance) + "   size: " + (_master.Points.Count - _test.Points.Count) + '\n';
		ans += "Ave dist: "+this.aveDist()+"   RMS: "+this.RMS()+'\n';
		return ans;
	}
	private void init(ComputeMethod aComputeMethod) {
        _m2t = dist(_master, _test, aComputeMethod);
        _t2m = dist(_test, _master, aComputeMethod);
	}
	private static int[] dist(Path2D a, Path2D b, ComputeMethod aComputeMethod ) {
        int[] ans = new int[a.Points.Count];
		int ind =0;
        for (int i = 0; i < a.Points.Count; i++)
        {
            PointEx curr = a.Points[i];

            switch (aComputeMethod)
            {
                case ComputeMethod.cmAll:
                    ind = b.findClosest(curr);
                    break;
                case ComputeMethod.cmTime:
                    ind = b.findClosest(curr, ind, Math.Min(ind + 10, b.Points.Count));
                    break;
                case ComputeMethod.cmAppTime:
                    ind = b.findClosest(curr, Math.Max(0, ind - 10), Math.Min(ind + 10, b.Points.Count));
                    break;
                default:
                    break;
            }

			ans[i]= ind;
		}
		return ans;
	}
	
	public double aveDist() {
		double ans = aveDist_m2t()+ aveDist_t2m();
		return ans/2;
	}
	public double RMS() {
		double ans = this.RMS_m2t()+ RMS_t2m();
		return ans/2;
	}
	
	private double aveDist_m2t() {
		double ans = 0;
		for(int i=0;i<_m2t.Length ;i++) {
			ans+= (_master.Points[i].Distance(_test.Points[_m2t[i]]));
		}
		return ans/_m2t.Length;
	}
	private double aveDist_t2m() {
		double ans = 0;
		for(int i=0;i<_t2m.Length;i++) {
            ans += (_test.Points[i]).Distance(_master.Points[_t2m[i]]);
		}
		return ans/_t2m.Length;
	}
	private double RMS_m2t() {
		double ans = 0, ave = aveDist_m2t();
		for(int i=0;i<_m2t.Length;i++) {
            double dist = (_master.Points[i]).Distance(_test.Points[_m2t[i]]);
			ans+=(dist-ave)* (dist-ave);
		}
		return Math.Sqrt(ans /_m2t.Length);
	}
	private double RMS_t2m() {
		double ans = 0, ave = aveDist_t2m();
		for(int i=0;i<_t2m.Length;i++) {
            double dist = (_test.Points[i]).Distance(_master.Points[_t2m[i]]);
			ans+=(dist-ave)* (dist-ave);
		}
		return Math.Sqrt(ans /_t2m.Length);
	}
}

}
