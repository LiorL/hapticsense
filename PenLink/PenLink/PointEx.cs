﻿using System;
using System.Drawing;
using Serial;
[Serializable]
public class PointEx
{
    public bool empty;
    public PointF Point;
    public int time;
    public float pressure;
    public float Azimuth;
    public float Altitude;
    public AtlasSerialPoller.SensorsData sensorsData;
    
    public double DistanceSquared(PointEx point)
    {
        return DistanceSquared(point.Point.X, point.Point.Y);
    }

    public double DistanceSquared(double x, double y)
    {
        double xdiff = x - Point.X;
        double ydiff = y - Point.Y;
        return xdiff * xdiff + ydiff * ydiff;
    }

    public double Distance(double x, double y)
    {
        double xdiff = x - Point.X;
        double ydiff = y - Point.Y;
        return Math.Sqrt(xdiff * xdiff + ydiff * ydiff);
    }

    
    public double Distance(PointEx aPoint)
    {
        return Distance(aPoint.Point.X, aPoint.Point.Y);
    }

    public PointEx(PointF aPoint, int aTime, float aPressure, bool aEmpty)
    {
        empty = aEmpty;
        Point = aPoint;
        time = aTime;
        pressure = aPressure;
    }

    internal PointEx(PointF aPoint, int aTime, float aPressure, int aAzimuth, int aAltitude,AtlasSerialPoller.SensorsData aSensorsData)
    {
        empty = false;
        Point = aPoint;
        time = aTime;
        pressure = aPressure;
        Azimuth = aAzimuth;
        Altitude = aAltitude;
        sensorsData = aSensorsData;
    }

    
}

