﻿using PenLink;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

[Serializable]
public class Path2D
{
    public int CanvasHeight;
    public int CanvasWidth;
    public List<PointEx> Points = new List<PointEx>();

    public int Time { get { return GetTime(); } }
    public double Distance { get { return GetDistance(); } }

    private void RemoveEmptyPoints()
    {
        Points.RemoveAll(
            delegate(PointEx point)
            {
                if (point.empty)
                    return true;
                return false;
            });

    }

    public void AddEmptyPoint()
    {
        Points.Add(new PointEx(new System.Drawing.PointF(0, 0), 0, 0, true));
    }

    public int findClosest(PointEx aPoint)
    {
        return findClosest(aPoint, 0, Points.Count);
    }

    public int findClosest(PointEx aPoint, int start, int end)
    {
        int ans = start;

        double min = aPoint.Distance(Points[start]);
        for (int i = start; i < end; i++)
        {
            PointEx c = Points[i];
            double dist = aPoint.Distance(c);
            if (dist < min)
            {
                ans = i;
                min = dist;
            }
        }
        return ans;
    }

    

    public float FindClosestPressureToPoint(PointEx point)
    {

        if (Points.Count > 1)
        {
            int FirstIndex = findClosest(point);
            return Points[FirstIndex].pressure;
        }
        else return -1;
     
    }

    protected int GetTime()
    {
        if (Points.Count > 0)
            return Points[Points.Count - 1].time - Points[0].time;
        else return 0;
    }

    protected double GetDistance()
    {
        if (Points.Count > 0)
        {
            double xDiff;
            double yDiff;
            double Distance = 0;
            for (int i = 0; i < Points.Count - 1; i++)
            {
                if (Points[i + 1].empty)
                {
                    i++;
                    continue;
                }
                xDiff = Points[i].Point.X - Points[i + 1].Point.X;
                yDiff = Points[i].Point.Y - Points[i + 1].Point.Y;
                Distance += Math.Sqrt(xDiff * xDiff + yDiff * yDiff);

            }

            return Distance;
        }

        else return 0;


    }
  
    public Path2D CloneNonEmpty()
    {
        Path2D basepath = new Path2D();
        basepath.Points.AddRange(Points.GetRange(0, Points.Count));
        basepath.RemoveEmptyPoints();
        return basepath;
    }
      
  
    public List<Vector2> GetNormalizedSegments()
    {
        List<Vector2> result = new List<Vector2>();
        for (int i = 0; i < Points.Count - 1; i++)
        {
            if (Points[i + 1].empty)
            {
                i++;
                continue;
            }
            result.Add(new Vector2(Points[i].Point.X, Points[i].Point.Y));
            result.Add(new Vector2(Points[i + 1].Point.X, Points[i + 1].Point.Y));

        }
        return result;
    }
}

