﻿using System;
using System.Collections.Generic;
using System.Text;
namespace PenLink
{
    [Serializable]
    public class Section
    {
        public string Name;
        public Dictionary<string, object> Options;
        public Path2D path;

        public Section(string aName,int width,int height)
        {
            Name = aName;
            path = new Path2D();
            path.CanvasWidth = width;
            path.CanvasHeight = height;
            Options = SectionManager.Instance.GetDefaultSectionValues();
        }

        public string GetOptionInKeyValue()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, object> pair in Options)
                if (pair.Value != null)
                    sb.Append("\r\n" + pair.Key + "=" + pair.Value.ToString());

            return sb.ToString();
        }
    }
}
