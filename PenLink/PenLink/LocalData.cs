﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink
{
    public class LocalData : Common.Settings<LocalData, SerializeFileNameClass>
    {

        public bool EnableToggleFullScreen = true;
        public bool ExclusiveMode = false;
        public Color PeripheralColor = Color.Black;
        public float PeripheralOuterSize = 100.0f; //Percent of screen
        public float PeripheralInnerSize = 85.0f;//Percent of screen
        public float PeripheralMinFrequency = 0.5f;
        public float PeripheralMaxFrequency = 4.0f;
        public float PeripheralSensitivityGrowth = 1.0f;
        public float PenSensitivityGrowth = 3.0f;
        

        //Local Data
        public string Experimentid = "000";
        public string ExperimentName = "Exp1";
        public string WorkingDirectory = @"C:\";
    }
}
