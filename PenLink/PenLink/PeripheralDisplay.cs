﻿using PenLink.Graphics;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink
{
    class PeripheralVisual
    {
        Quad fQuad;
        public PeripheralVisual( GraphicRenderer renderer)
        {
            Visible = true;
            (fQuad = new Quad(renderer)).Create();
        }
        public void SetColor(Color4 aColor)
        {
            fQuad.SetBlendColor(aColor);
        }
        public void Draw(GraphicRenderer aRenderer)
        {
            if (Visible)
                fQuad.Draw(aRenderer.GraphicsDevice.ImmediateContext);
        }
        public bool Visible { get; set; }

        internal void SetOuterRadius(double size)
        {
            fQuad.SetOuterRadius((float)size);
        }
    }
     
}
