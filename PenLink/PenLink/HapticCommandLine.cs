﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MessageBox = System.Windows.Forms.MessageBox;

namespace PenLink
{
    class HapticCommandLine
    {
        public HapticCommandLine(string []args)
        {
            RunCommandLineTool(args);
        }

        private void RunCommandLineTool(string[] args)
        {
            if (args.Length != 2 || (args.Length == 2 && args[0].ToLower() != "unify"))
            {
                MessageBox.Show("Usage: penlink unify [folder]");
            }
            else
            {
                string folderToUnify = args[1];
                if (!System.IO.Directory.Exists(folderToUnify))
                {
                    MessageBox.Show("Folder does not exist");
                }
                else
                {
                    string combinedFileName = folderToUnify.Remove(0, folderToUnify.LastIndexOf('\\') + 1) + ".plfx";
                    combinedFileName = System.IO.Path.Combine(folderToUnify, combinedFileName);
                    string[] filesToUnify = System.IO.Directory.GetFiles(folderToUnify, "*.plf*");
                    PenLinkFile combinedPenLinkFile = Files.FileManager.UnifyPlfFiles(filesToUnify);
                    Files.FileManager.SavePenLinkFile(combinedFileName, combinedPenLinkFile);
                }
            }
        }
    }
}
