﻿using PenLink.Graphics;
using PenLink.Interfaces;
using PenLink.Timing;
using SharpDX;
//using SharpDX.Direct3D11;

namespace PenLink
{
    class PeripheralFeedback
    {
        const double fInterVal = 1.0 / (60 * 2);
        Timing.TimedAction  fTimedAction;
        double              fPulsesPerSecond = 1.0;
        private double      fCurrentPosition = 1.0;
        
        private bool    fEnabled;
        private double  fMinSize;
        private double  fMaxSize;
        IGraphics fGraphics;
        
        
        public bool Enabled
        {
            get 
            { 
                return fEnabled;
            }
            set
            {
                if (value != fEnabled)
                {

                    fTimedAction.Enabled = fEnabled = value;

                }
            }
        }

        public PeripheralFeedback(IGraphics aGraphics)
        {
            fTimedAction = TimedActionManager.Instance.CreateAction(Globals.DefaultTimedActionBatch, "Peripheral", fInterVal, NextStep);
            fGraphics = aGraphics;
            
        }

        public void SetParams(double aMinSize,double aMaxSize)
        {
            fMinSize = aMinSize;
            fMaxSize = aMaxSize;
            
        }

     

        private void AdvancePosition(double aDeltaTime)
        {
            fCurrentPosition += aDeltaTime * fPulsesPerSecond;
            fCurrentPosition = fCurrentPosition % 1;

            double pos = fCurrentPosition;
            if (fCurrentPosition > 0.5)
                pos = 1.0 - pos;

            double size = (fMaxSize - fMinSize) * ( pos * 2.0)  + fMinSize;

            fGraphics.SetPeripheralOuterRadius(size);
            
        }
        
        private void NextStep(TimedActionObject aObj)
        {
            if (fEnabled == true)
                AdvancePosition(aObj.DeltaTime);
        }

        public void SetPulsesPerSecond(double aSpeed)
        {
            fPulsesPerSecond = aSpeed;
        }
    }
}
