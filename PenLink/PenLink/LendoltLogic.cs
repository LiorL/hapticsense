﻿using System.Windows.Forms;
using SharpDX;
using System;
using PenLink.Timing;
using PenLink.Interfaces;

namespace PenLink
{
    public enum LendoltResult { Hit, Miss, FalseAlarm };
    public class LendoltLogic
    {
        const int cFrames = 8;

        public delegate void LendoltResultDelegate(LendoltResult result);
        int fCurrentLendoltCState;
        LendoltResultDelegate OnResult;
        Random fRandom;
        int fMinInterval;
        int fMaxIntreval;
        int fDeltaInterval;
        int fDisplayCount;
        Timing.TimedAction fTimedAction;
        IGraphics fGraphics;
        bool fIsActuated;


        public LendoltLogic(int aMinInterval, int aMaxInterval, LendoltResultDelegate aCallBack,IGraphics aGraphics)
        {
            double lendoltProbability = Settings.Data.GetValue<double>("General.LendoltProbability");
            if (lendoltProbability < 0.0 || lendoltProbability > 0.5)
                throw new Exception("LendoltPorbability settings value is out of range [0 - 0.5])");

            fGraphics = aGraphics;

            OnResult = aCallBack;
            fDisplayCount = 0;
            fRandom = new Random();
            fMinInterval = aMinInterval;
            fMaxIntreval = aMaxInterval;
            fDeltaInterval = aMaxInterval - aMinInterval;
            fTimedAction = TimedActionManager.Instance.CreateAction(Globals.DefaultTimedActionBatch, "LendoltC", 1, OnLendoltChange);
            fTimedAction.Enabled = false;
            OnResult = aCallBack;
        }

        public void start()
        {
            fDisplayCount = 0;
            fIsActuated = false;
            fCurrentLendoltCState = -1;
            fTimedAction.Enabled = true;

        }

        public void Stop()
        {
            fTimedAction.Enabled = false;
            fGraphics.ActiveCursor = CursorType.Normal;
        }

        private double probability
        {
            get
            {
                return Settings.Data.GetValue<double>("General.LendoltProbability");
            }
        }
        void OnLendoltChange(TimedActionObject aObj)
        {

            int lendoltCState;
            // We accumulate the number of times where lendolt c should point up, so probability would be valid for the long term.
            if (fRandom.NextDouble() < probability)
                fDisplayCount++;

            
            if (fDisplayCount > 0 && fCurrentLendoltCState != 0)
            {
                //current lendolt c state not pointing up and there are lendolt c up waiting to be shown - so show lendolt c up.
                lendoltCState = 0;
                fDisplayCount--;
            }
            else
            {
                //There is no lendolt c up waiting to be shown, choose random state that is not lendolt c up
                //TODO; improve this mechanism, preallocate and randomize provided numbers or allow the same lendolt c state to be shown two timees or more in a row.
                lendoltCState = fRandom.Next(cFrames - 1) + 1;
                if (lendoltCState == fCurrentLendoltCState)
                    lendoltCState = ((lendoltCState + 1) % (cFrames - 1)) + 1;
            }

            if (fCurrentLendoltCState == 0)
                RaiseResult(LendoltResult.Miss);
            
            fCurrentLendoltCState = lendoltCState;
            fGraphics.SetLendoltFrameNumber(fCurrentLendoltCState);
            fTimedAction.Interval = (fRandom.Next(fDeltaInterval) + fMinInterval) / 1000.0;
            fIsActuated = false;

        }

        private void RaiseResult(LendoltResult aResult)
        {
            if (fIsActuated == false)
            {
                fIsActuated = true;
                OnResult(aResult);
            }
        }

        public void SetLendoltFeedback()
        {
            LendoltResult result = (fCurrentLendoltCState == 0) ? LendoltResult.Hit : LendoltResult.FalseAlarm;
            RaiseResult(result);
        }
    }
}