﻿using NetSettings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PenLink
{
    public class Settings
    {
        static ItemTree fRootTree;
        static DataProvider fData;
        static NetSettings.Data.FlatDataBindings fUserSettings;
        const string TemplatePath = @".\Resources\SettingsTemplate.json";
        const string UserPAth = @".\Penlink.settings";

        static Settings()
        {
            fRootTree = ItemTree.FromFile(TemplatePath);
            fData = new DataProvider(fRootTree);
            Load();
            fData.DataBinding = fUserSettings;
        }

        private static JsonSerializerSettings JsonSettings
            {
            get
            {
                return new JsonSerializerSettings()
                {
                    Formatting = Formatting.Indented
                    //TypeNameHandling = TypeNameHandling.All

                };
            }
            }

        public static void Save()
        {
            string text = JsonConvert.SerializeObject(fUserSettings.Data, JsonSettings);

            File.WriteAllText(UserPAth,text);
        }

        static void Load()
        {
            if (!File.Exists(UserPAth))
            {
                fUserSettings = new NetSettings.Data.FlatDataBindings(new Dictionary<string, object>());
                fData.FillDefaultOptionsSet(fUserSettings);
                Save();
                
            }
            else
            {
                string text = File.ReadAllText(UserPAth);
                fUserSettings = new NetSettings.Data.FlatDataBindings(
                    JsonConvert.DeserializeObject<Dictionary<string, object>>(text, JsonSettings));
            }

        }

        public static DataProvider Data { get { return fData; } }
    }
}
