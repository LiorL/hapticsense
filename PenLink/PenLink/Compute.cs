﻿using System;
using SharpDX;

namespace PenLink
{
    public class Compute
    {
        public static bool IsLogarithmicfunctionAvailable(float aReferencePressure)
        {
            return aReferencePressure == 0.4 || aReferencePressure == 0.5 || aReferencePressure == 0.6;
        }

        public static Color4 PressureToColorFormulaDot4Ref(float aPressure, float aReferencePressure)
        {
            if (aPressure > aReferencePressure)
            {
                //red
                float X = 400.0f - (aPressure - aReferencePressure) * 1000.0f;
                float Y = (int)(208.51 * Math.Log(1 + (400.0 - X) / 40, Math.E));
                int R = Y <= 255 ? 255 : 255 - ((int)Y - 255);
                int G = Y <= 255 ? 255 - (int)Y : 0;
                int B = Y <= 255 ? 255 - (int)Y : 0 ;
                return new Color4(R / 255.0f,G / 255.0f,B / 255.0f,1.0f);
            }
            else
            {
                //blue
                float X = 400.0f - (aReferencePressure - aPressure) * 1000.0f;
                float Y = (int)(180.336 * Math.Log(1 + (400.0 - X) / 40, Math.E)) + 500;
                int R = Y <= 755 ? 755 - (int)Y : 0;
                int G = Y <= 755 ? 755 - (int)Y : 0; 
                int B = Y <= 755 ? 255 : 255 - ((int)Y - 755);
                return new Color4(R / 255.0f, G / 255.0f, B / 255.0f, 1.0f);
            }
        }

        public static Color4 PressureToColorFormulaDot5Ref(float aPressure, float aReferencePressure)
        {
            if (aPressure > aReferencePressure)
            {
                //red
                float X = 500.0f - (aPressure - aReferencePressure) * 1000.0f;
                float Y = (int)(192.0 * Math.Log(1 + (500.0 - X) / 40, Math.E));
                int R = Y <= 255 ? 255 : 255 - ((int)Y - 255);
                int G = Y <= 255 ? 255 - (int)Y : 0;
                int B = Y <= 255 ? 255 - (int)Y : 0;
                return new Color4(R / 255.0f, G / 255.0f, B / 255.0f,1.0f);
            }
            else
            {
                //blue
                float X = 500.0f - (aReferencePressure - aPressure) * 1000.0f;
                float Y = (int)(192.0 * Math.Log(1 + (500.0 - X) / 40, Math.E)) + 500;
                int R = Y <= 755 ? 755 - (int)Y : 0;
                int G = Y <= 755 ? 755 - (int)Y : 0;
                int B = Y <= 755 ? 255 : 255 - ((int)Y - 755);
                return new Color4(R / 255.0f, G / 255.0f, B / 255.0f, 1.0f);
            }
        }

        public static Color4 PressureToColorFormulaDot6Ref(float aPressure, float aReferencePressure)
        {
            if (aPressure > aReferencePressure)
            {
                //red
                float X = 600.0f - (aPressure - aReferencePressure) * 1000.0f;
                float Y = (int)(180.336 * Math.Log(1 + (600.0 - X) / 40, Math.E));
                int R = Y <= 255 ? 255 : 255 - ((int)Y - 255);
                int G = Y <= 255 ? 255 - (int)Y : 0;
                int B = Y <= 255 ? 255 - (int)Y : 0;
                return new Color4(R / 255.0f, G / 255.0f, B / 255.0f, 1.0f);
            }
            else
            {
                //blue
                float X = 600.0f - (aReferencePressure - aPressure) * 1000.0f;
                float Y = (int)(208.51 * Math.Log(1 + (600.0 - X) / 40, Math.E)) + 500;
                int R = Y <= 755 ? 755 - (int)Y : 0;
                int G = Y <= 755 ? 755 - (int)Y : 0;
                int B = Y <= 755 ? 255 : 255 - ((int)Y - 755);
                return new Color4(R / 255.0f, G / 255.0f, B / 255.0f, 1.0f);
            }
        }

        public static float PressureToHzFormulaDot4Ref(float aPressure, float aReferencePressure)
        {
            if (aPressure > aReferencePressure)
            {
                float X = 400.0f - (aPressure - aReferencePressure) * 1000.0f;
                float Y = (int)(208.51 * Math.Log(1 + (400.0 - X) / 40, Math.E));
                return 2.25f + Y * 0.004f;
            }
            else
            {
                float X = 400.0f - (aReferencePressure - aPressure) * 1000.0f;
                float Y = (int)(180.336 * Math.Log(1 + (400.0 - X) / 40, Math.E)) + 500;
                return 2.25f - (Y - 500) * 0.004f;
            }
        }
        
        public static float PressureToHzFormulaDot5Ref(float aPressure, float aReferencePressure)
        {
            if (aPressure > aReferencePressure)
            {
                float X = 500.0f - (aPressure - aReferencePressure) * 1000.0f;
                float Y = (int)(192.0 * Math.Log(1 + (500.0 - X) / 40, Math.E));
                return 2.25f + Y * 0.004f;
            }
            else
            {
                float X = 500.0f - (aReferencePressure - aPressure) * 1000.0f;
                float Y = (int)(192.0 * Math.Log(1 + (500.0 - X) / 40, Math.E)) + 500;
                return 2.25f - (Y - 500) * 0.004f;
            }
        }

        public static float PressureToHzFormulaDot6Ref(float aPressure, float aReferencePressure)
        {
            if (aPressure > aReferencePressure)
            {
                float X = 600.0f - (aPressure - aReferencePressure) * 1000.0f;
                float Y = (int)(180.336 * Math.Log(1 + (600.0 - X) / 40, Math.E));
                return 2.25f + Y * 0.004f;
            }
            else
            {
                float X = 600.0f - (aReferencePressure - aPressure) * 1000.0f;
                float Y = (int)(208.51 * Math.Log(1 + (600.0 - X) / 40, Math.E)) + 500;
                return 2.25f - (Y - 500) * 0.004f;
            }
        }

        //main function to retreive color from pressure
        public static Color4 GetPressurizedColor(float aPressure, float aReferencePressure,bool aForceLinear)
        {
            int referencePressure10Times = (int)(aReferencePressure * 10);
            if (!aForceLinear)
            switch (referencePressure10Times)
            {
                case 4:
                    return PressureToColorFormulaDot4Ref(aPressure, aReferencePressure);
                case 5:
                    return PressureToColorFormulaDot5Ref(aPressure, aReferencePressure);
                case 6:
                    return PressureToColorFormulaDot6Ref(aPressure, aReferencePressure);
            }

            float ReferenceComplementary = 1.0f - aReferencePressure;
            float diff = aPressure - aReferencePressure;
            float whiteIntensity = diff < 0 ? (aReferencePressure + diff) / aReferencePressure : (ReferenceComplementary - diff) / ReferenceComplementary;
                        return diff < 0 ? new Color4(whiteIntensity, whiteIntensity, 1.0f, 1.0f) : new Color4(1.0f, whiteIntensity, whiteIntensity, 1.0f);
        }

        public static float FrequencyFromPressure(float aPressure, float aReferencePressure, bool aForceLinear)
        {
            int referencePressure10Times = (int)(aReferencePressure * 10);
            
            if (!aForceLinear)
            switch (referencePressure10Times)
            {
                case 4:
                    return PressureToHzFormulaDot4Ref(aPressure, aReferencePressure);
                case 5:
                    return PressureToHzFormulaDot5Ref(aPressure, aReferencePressure);
                case 6:
                    return PressureToHzFormulaDot6Ref(aPressure, aReferencePressure);
            }

            float min = LocalData.Current.PeripheralMinFrequency;
            float max = LocalData.Current.PeripheralMaxFrequency;
            float growth = LocalData.Current.PeripheralSensitivityGrowth;
            float pressure = (float)Math.Pow(aPressure, growth);
            return min + (max - min) * pressure;
        }

        public static float GetSoundRateFromPressure(float aPressure)
        {
            float speed = (float)Math.Pow(aPressure, 1);
            int octave = 0;
            if (speed < 0.3)
            {
                octave = 2;
            }

            else if (speed < 0.5)
            {
                octave = 3;
            }
            else if (speed < 0.7)
            {
                octave = 4;
            }
            else
            {
                octave = 5;
            }

            return aPressure * 300;
        }
        public static float TransformPressure(float aRawPressure)
        {
            float transformed = (1.0f / (float)Settings.Data.GetValue<double>("Input.MaxInputPressure") * aRawPressure);
            if (transformed > 1.0f)
                transformed = 1.0f;
            return transformed;
        }

        public static float TransformHeight(float aRawPressure)
        {
            float transformed = 0;
            float margin = ((1.0f - (float)Settings.Data.GetValue<double>("Input.MaxInputHeight")) / 2.0f);
            float p0 = margin;
            float p1 = 1.0f - margin;

            if (aRawPressure >= p0 && aRawPressure <= p1)
                transformed = aRawPressure / (p1 - p0);

            return transformed;
        }
        public static System.Drawing.PointF UnNormalizeCoordinates(float x, float y, System.Drawing.Rectangle aScreenBounds)
        {
            return new System.Drawing.PointF(
                        (int)Math.Round((aScreenBounds.Width * (x + 1.0)) / 2.0),
                        (int)Math.Round((aScreenBounds.Height * (-y + 1.0)) / 2.0));
        }


        public static System.Drawing.PointF NormalizeCoordinates(int aWidth, int aHeight, int aX, int aY)
        {
            float halfWidth = aWidth / 2;
            float halfHeight = aHeight / 2;
            return new System.Drawing.PointF((aX - halfWidth) / halfWidth, -(aY - halfHeight) / halfHeight);
        }

        public static Vector2 NormalizeCoordinates(Vector2 aSize, Vector2 aUnNormalizedCoords)
        {
            Vector2 halfSize = aSize / 2;
            Vector2 norm = (aUnNormalizedCoords - halfSize) / halfSize;
            norm.Y *= -1;
            return norm;
            //return new System.Drawing.PointF((aX - halfWidth) / halfWidth, -(aY - halfHeight) / halfHeight);
        }

        static readonly Vector2 UnitXY = new Vector2(1, 1);
        internal static Vector2 UnNormalizeCoordinates(Vector2 aSize, Vector2 aPoint)
        {
            aPoint.Y *= -1;
            return Vector2.Multiply(Vector2.Modulate(aSize, Vector2.Add(aPoint, UnitXY)),0.5f);
        }
    }
}
