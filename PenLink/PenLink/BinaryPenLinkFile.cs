﻿using FileHelpers;
using SharpDX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;


namespace PenLink.BinaryPenlinkFile
{

    sealed class Binder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            //string type = typeName;
            
            string type = typeName.Replace("PenLink.", "PenLink.BinaryPenlinkFile.");
            if (typeName == "Path2D" || typeName == "Path2DGFX")
                type = "PenLink.BinaryPenlinkFile." + typeName;

            Type t = Type.GetType(type);

            return t;
        }
    }

    [Serializable]
    public class PenLinkFile
    {
        internal List<Section> Sections;

        public static PenLinkFile Load(string aFileName)
        {
            if (aFileName != null)
            {
                Stream fStream = File.OpenRead(aFileName);
                BinaryFormatter binFormat = new BinaryFormatter();
                binFormat.Binder = new Binder();
                PenLinkFile plf = (PenLinkFile)binFormat.Deserialize(fStream);
                fStream.Close();
                return plf;
            }
            return null;
        }
        //public string BackgroundImageName = "";
        //public uint BackGroundColor = 0xFFFFFF;
        //public Path2DGFX path2D;
        //[OptionalField(VersionAdded = 0.1)]
        //public float dummy;

    }

    [Serializable]
    public class OptionsSet : Dictionary<string, Dictionary<string, object>>, ICloneable, ISerializable
    {
        public OptionsSet()
        {
        }

        #region ICloneable Members

        public object Clone()
        {
            OptionsSet ClonedObject = new OptionsSet();
            foreach (KeyValuePair<string, Dictionary<string, object>> pair in this)
                ClonedObject[pair.Key] = new Dictionary<string, object>(pair.Value);
            return (object)(ClonedObject);
        }

        #endregion

        protected OptionsSet(SerializationInfo info, StreamingContext context) : base(info, context) { }

    }


    [Serializable]
    public class Section : ICloneable
    {
        public Path2DGFX path;
        //public Dictionary<string, Dictionary<string, object>> Options;
        public OptionsSet Options;
        public string Name;

        public Section(string aName, int width, int height)
        {
            Name = aName;
            path = new Path2DGFX(
                System.Drawing.Color.Blue,
                4.0f,
                3,
                10.0f);
            path.CanvasWidth = width;
            path.CanvasHeight = height;
            //Options = new Dictionary<string, Dictionary<string, object>>();
            Options = new OptionsSet();

            Options.Add("Pen Settings", GetDefault("Pen Settings"));
            Options.Add("Peripheral Settings", GetDefault("Peripheral Settings"));
            Options.Add("BackGround Settings", GetDefault("BackGround Settings"));
            Options.Add("Canvas Function", GetDefault("Canvas Function"));
            Options.Add("Misc Settings", GetDefault("Misc Settings"));

        }

        public static Dictionary<string, object> GetDefault(string OptionSet)
        {
            Dictionary<string, object> res = new Dictionary<string, object>();

            switch (OptionSet)
            {

                case "Pen Settings":
                    res.Add("Min Width", 4.0);
                    res.Add("Max Width", 10.0);
                    res.Add("Sensitivity", LocalData.Current.PenSensitivityGrowth);
                    res.Add("Master Color", Color.Blue);
                    res.Add("Test Color", Color.Red);
                    break;
                case "Peripheral Settings":
                    res.Add("Pen Width", 4.0f);
                    res.Add("Color", Color.YellowGreen);
                    res.Add("Outer Size", 100);
                    res.Add("Inner Size", 85);
                    res.Add("Steps", 10);
                    res.Add("Min Frequency", 10);
                    res.Add("Max Frequency", 120);
                    res.Add("Sensitivity", 0.5f);
                    break;
                case "BackGround Settings":
                    res.Add("Color", Color.Gray);
                    res.Add("Image", "");
                    res.Add("Rotation", 0.0f);
                    break;
                case "Canvas Function":
                    res.Add("Rotation", 0.0f);
                    break;
                case "Misc Settings":
                    res.Add("Time", 10);
                    res.Add("Repetitions", 1);
                    res.Add("Reference pressure", 0.5f);
                    break;

                default:
                    return null;
            }
            return res;
        }


        #region ICloneable Members

        private Dictionary<string, Dictionary<string, object>> CloneOptions()
        {
            Dictionary<string, Dictionary<string, object>> ClonedOptions = new Dictionary<string, Dictionary<string, object>>(Options);
            foreach (KeyValuePair<string, Dictionary<string, object>> pair in Options)
                ClonedOptions[pair.Key] = new Dictionary<string, object>(pair.Value);
            return ClonedOptions;
        }


        public object Clone()
        {
            Section ClonedSection = new Section(Name + "_c", path.CanvasWidth, path.CanvasHeight);
            ClonedSection.path = (Path2DGFX)path.Clone();
            ClonedSection.Options = (OptionsSet)Options.Clone();
            return (object)ClonedSection;
        }

        #endregion
    }

    [Serializable]
    public class Path2D : List<PointEx>
    {

        public int Time { get { return GetTime(); } }
        public double Distance { get { return GetDistance(); } }

        private void RemoveEmptyPoints()
        {
            this.RemoveAll(
                delegate(PointEx point)
                {
                    if (point.empty)
                        return true;
                    return false;
                });

        }

        public void AddEmptyPoint()
        {
            Add(new PointEx(new System.Drawing.PointF(0, 0), 0, 0, true));
        }

        public int findClosest(PointEx aPoint)
        {
            return findClosest(aPoint, 0, Count);
        }

        public int findClosest(PointEx aPoint, int start, int end)
        {
            int ans = start;

            double min = aPoint.Distance(this[start]);
            for (int i = start; i < end; i++)
            {
                PointEx c = this[i];
                double dist = aPoint.Distance(c);
                if (dist < min)
                {
                    ans = i;
                    min = dist;
                }
            }
            return ans;
        }



        public float FindClosestPressureToPoint(PointEx point)
        {

            if (Count > 1)
            {
                int FirstIndex = findClosest(point);
                return this[FirstIndex].pressure;
            }
            else return -1;

            //because the Tablet Polling rate is high our error will be small
            /*
        //Precise Calculation
        int SecondIndex = -1;
        if (Count > 1)
        {
            //Last Point
            if (FirstIndex == this.Count - 1)
                 SecondIndex = FirstIndex - 1;
            //First Point
            else if (FirstIndex == 0)
                SecondIndex = FirstIndex + 1;
            else
            {
                if (this[FirstIndex - 1].DistanceSquared(point) < this[FirstIndex + 1].DistanceSquared(point))
                    SecondIndex = FirstIndex - 1;
                else SecondIndex = FirstIndex + 1;
            }
                  
       
            
        }
         */
            //throw new Exception("Can not Find Closest Point while Path is empty");
        }

        protected int GetTime()
        {
            if (this.Count > 0)
                return this[this.Count - 1].time - this[0].time;
            else return 0;
        }

        protected double GetDistance()
        {
            if (this.Count > 0)
            {
                double xDiff;
                double yDiff;
                double Distance = 0;
                for (int i = 0; i < this.Count - 1; i++)
                {
                    if (this[i + 1].empty)
                    {
                        i++;
                        continue;
                    }
                    xDiff = this[i].Point.X - this[i + 1].Point.X;
                    yDiff = this[i].Point.Y - this[i + 1].Point.Y;
                    Distance += Math.Sqrt(xDiff * xDiff + yDiff * yDiff);

                }

                return Distance;
            }

            else return 0;


        }
        public void WriteToCsv(string FileName)
        {

            PointEx[] points = CloneNonEmpty().ToArray();
            PenLink.Export.PathRecord[] records = new PenLink.Export.PathRecord[points.Length];

            for (int i = 0; i < points.Length; i++)
            {
                records[i] = new PenLink.Export.PathRecord();
                records[i].x = points[i].Point.X;
                records[i].y = points[i].Point.Y;
                records[i].Azimuth = (int)points[i].Azimuth;
                records[i].Altitude = (int)points[i].Altitude;
                records[i].pressure = points[i].pressure;
                records[i].time = points[i].time;
                records[i].GSR = points[i].sensorsData.GSR;
                records[i].FSR = points[i].sensorsData.FSR;
                records[i].ASR = points[i].sensorsData.ASR;
                records[i].EMG = points[i].sensorsData.EMG;
            }

            FileHelperEngine engine = new FileHelperEngine(typeof(PenLink.Export.PathRecord));
            engine.HeaderText = "Time,X,Y,Pressure,Azimuth,Altitude,GSR,FSR,ASR,EMG";
            //Path2D[] res = engine.ReadFile(FileName) as Path2D[];

            // To Write Use:  
            engine.WriteFile(FileName + ".csv", records);
        }
        public Path2D CloneNonEmpty()
        {
            Path2D basepath = new Path2D();
            basepath.AddRange(this.GetRange(0, this.Count));
            basepath.RemoveEmptyPoints();
            return basepath;
        }
    }



    [Serializable]
    public class Path2DGFX : Path2D, ICloneable
    {
        //Serialized Members
        public int CanvasHeight;
        public int CanvasWidth;
        private int PenColor;
        private float MinWidth;
        private float maxWidth;
        private float Growth;

        //Non Serialized Members 
        [NonSerialized]
        private System.Drawing.Pen _pen;

        public System.Drawing.Pen pen { get { return _pen; } }
        public float MaxWidth { get { return maxWidth; } }


        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context)
        {
            CreatePen(System.Drawing.Color.FromArgb(PenColor), MinWidth, Growth, maxWidth);
        }





        //public Path2DGFX()
        //{
        //    CreatePen(Color.Blue, 1f, 0.5f, 10f);
        //}
        public Path2DGFX(System.Drawing.Color aColor, float aMinWidth, float aGrowth, float aMaxWidth)
        {
            CreatePen(aColor, aMinWidth, aGrowth, aMaxWidth);
        }


        private double TransFormWidth(float val)
        {
            return Math.Pow(val, Growth);
        }


        private void CreatePen(System.Drawing.Color aColor, float aMinWidth, float aGrowth, float aMaxWidth)
        {
            _pen = new System.Drawing.Pen(aColor, aMinWidth);
            _pen.StartCap = _pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            //Update For Serialization
            PenColor = _pen.Color.ToArgb();
            MinWidth = aMinWidth;
            maxWidth = aMaxWidth;
            Growth = aGrowth;

        }



        public List<Vector2> GetNormalizedSegments()
        {
            List<Vector2> result = new List<Vector2>();
            for (int i = 0; i < this.Count - 1; i++)
            {
                if (this[i + 1].empty)
                {
                    i++;
                    continue;
                }
                result.Add(new Vector2(this[i].Point.X, this[i].Point.Y));
                result.Add(new Vector2(this[i + 1].Point.X, this[i + 1].Point.Y));

            }
            return result;
        }


        #region ICloneable Members

        public object Clone()
        {
            Path2DGFX clonedObject = new Path2DGFX(System.Drawing.Color.FromArgb(PenColor), MinWidth, Growth, MaxWidth);
            clonedObject.AddRange(this.GetRange(0, this.Count));
            return (object)clonedObject;
        }

        #endregion


    }
 
}
