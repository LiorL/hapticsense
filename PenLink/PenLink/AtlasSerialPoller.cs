﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Serial
{
    public class AtlasSerialPoller : SerialPoller
    {
        public enum SensorType { FSR = 4, ASR = 16, GSR = 28, EMG = 34 };
        
        [Serializable]
        public struct SensorsData
        {
            public int FSR;
            public int GSR;
            public int ASR;
            public int EMG;
        }

        byte[] Packet;
        public AtlasSerialPoller(string PortName, int BaudRate)
            : base(PortName, BaudRate)
       {
            //packet in atlas sensors never exceeds 56
        }
        public bool Read()
        {
            Packet = this.SerialPolling();
            if (Packet != null)
                return true;
            else return false;
        }

        public SensorsData GetAllData()
        {
            SensorsData sdata;
            sdata.ASR = GetData(SensorType.ASR);
            sdata.GSR = GetData(SensorType.GSR);
            sdata.FSR = GetData(SensorType.FSR);
            sdata.EMG = GetData(SensorType.EMG);
            return sdata;
        }
        
        public int GetData(SensorType aType)
        {
            return (((Packet[(int)aType]) << 1 | (Packet[((int)aType + 1)] << 8)));
        }




    }
}
