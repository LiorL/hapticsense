﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tablet;
using System.Threading;

namespace vbtablettest
{
    public partial class Form1 : Form
    {
        IntPtr hContext = IntPtr.Zero;
        TabletApi.LOGCONTEXT context = new TabletApi.LOGCONTEXT();
        public Form1()
        {
            TabletApi.LOGCONTEXT contenxtdescription = new TabletApi.LOGCONTEXT(); ;
            TabletApi.WTInfo(TabletApi.WTI_DEFCONTEXT,0, ref contenxtdescription);
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int result = -1;
                    
            result = TabletApi.WTInfo(TabletApi.WTI_DEFCONTEXT, 0, ref context);
            float HalfX;
            float HalfY;
                    HalfX = (float)context.lcOutExtX / 2.0f;
                    HalfY = (float)context.lcOutExtY / 2.0f;

                    context.lcName = "FirstContext";
                    context.lcOptions = TabletApi.CXO_MESSAGES | TabletApi.CXO_CSRMESSAGES; //'receive packets as messages
                    context.lcMsgBase = TabletApi.WT_DEFBASE;// 'establish message base number
                    context.lcPktData = TabletApi.PK_STATUS | TabletApi.PK_CURSOR | TabletApi.PK_BUTTONS | TabletApi.PK_X | TabletApi.PK_Y | TabletApi.PK_Z | TabletApi.PK_NORMAL_PRESSURE | TabletApi.PK_TANGENT_PRESSURE | TabletApi.PK_ORIENTATION | TabletApi.PK_ROTATION | TabletApi.PK_TIME | TabletApi.PK_SERIAL_NUMBER;
                    context.lcPktRate = 100;
                    context.lcPktMode = 0;
                    context.lcMoveMask = TabletApi.PK_CURSOR | TabletApi.PK_STATUS | TabletApi.PK_BUTTONS | TabletApi.PK_X | TabletApi.PK_Y | TabletApi.PK_Z | TabletApi.PK_NORMAL_PRESSURE | TabletApi.PK_TANGENT_PRESSURE | TabletApi.PK_ORIENTATION | TabletApi.PK_ROTATION;
                    context.lcBtnDnMask = //SBN_LCLICK Or PK_STATUS Or SBN_RCLICK Or SBN_LDBLCLICK Or SBN_RDBLCLICK
                    context.lcBtnUpMask = //pCtxDesc.lcBtnDnMask
                    context.lcSysMode = 0; //absolute
                    context.lcOptions = 0;
                    context.lcOutOrgZ = 0;
                    context.lcInOrgZ = 0;
            hContext = TabletApi.WTOpen(this.Handle, ref context, true);
            this.BeginInvoke(new MethodInvoker(MainLoop));
        }

        private int oldPacket;
        private int newPacket;
        int lastPacketRead;
        public bool PacketInQueue()
        {
            TabletApi.WTQueuePacketsEx(hContext, ref oldPacket, ref newPacket);
            //log.Log(this.oldPacket.ToString()+ " "+this.newPacket.ToString()+" "+this.lastPacketRead.ToString());
            if (oldPacket != newPacket && newPacket != lastPacketRead)
                return true;
            else return false;
        }

        private void MainLoop()
        {
            while (true)
            {
                if (PacketInQueue())
                {
                    
                    TabletApi.tagPacket packet = new TabletApi.tagPacket();
                    int k =  TabletApi.WTPacket(hContext, newPacket, ref packet);
                    lastPacketRead = newPacket;
                }
                Thread.Sleep(50);
                Application.DoEvents();
            }
        }
    }
}
